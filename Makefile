all: server gfx client

server:
	cargo build --release --bin server

gfx:
	cargo build --release --bin gfx

client:
	cargo build --release --bin client

run: all
	sh test/scripts/test_launch.sh

kill:
	sh test/scripts/test_stop_exec.sh

clean:
	cargo clean

fclean: clean

re: fclean all

.PHONY: all clean fclean re client server gfx
