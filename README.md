# Zappy

## Introduction

Zappy is a real time automatic video game where AIs play with each others. It's a project from 42 school and was done with bebosson and juepee-m in rust.


## Game overview

The goal of this game is to gather resources and other players on the same square to start a casting process that will elevate our players to the next level.

# Table of Contents
1. [Geography](#geography)
2. [Resources](#resources)
3. [Individuals](#individuals)
4. [Elevation ritual](#Elevation-ritual)
5. [Field of view](#fov)
6. [Sound Transmission](#sound-transmission)

______________________________________________


### Geography

The game is about managing an entire world and its population. That world, Trantor is geographically made of plains that have no height: no crater, no valley, no mountains.\
The game board represents the entire surface of that world, like a map. If a player exits on the right of the board, he will re-enter on the left.\
The game is played by teams. The winning team is the one that will have its 6 players reach the maximum level.


### Resources

The location where we are is quite rich in resources: mining or food. Thus, all you need is to walk around to discover this fabulous food or many stones of various nature. food is called `nourriture`.\
These stones have 6 distinct kinds: `linemate`, `deraumere`, `sibur`, `mendiane`, `phiras` and `thystame`.\
The server generates the resources. Generation must be random but must follow some rules.


### Individuals

The population of Trantor has two type of occupation:\
    - Get ’nourriture’ to eat and not die of hunger.
    - Look for stones, pick them up to create totems, have an elevation ritual and get to the next level.

The ’nourriture’ that the Trantorian picks up is the only resource he needs to live.\
One ’nourriture’ unit allows him to survive 126 time units, therefore 126/t seconds.\
Elevation is an important activity for Trantorians.


### Elevation ritual

Everyone’s goal is to reach the top of the Trantorian hierarchy. The ritual that allows to increase one’s physical and mental capacities must be accomplished according to a particular rite.\
One needs to bring together on the same field unit:
    - A combination of stones
    - A certain amount of players of the same level

The player will start the incantation and the elevation will start. It isn’t necessary that all the players be on the same team. Only their collective levels matter. All players within the incantation group will reach the higher level.\
Passed on from generation to generation, the secret to the elevation goes as such:

![elevation ritual](./assets/doc/resolution.png)


### Field of view

The field of view for players is limited due to various factors. With each increase in elevation, their field of view extends one unit ahead and one unit on each side in a new row.\
For example, for the first three levels and the first two elevations, the following field of views are obtained (assuming our player is at position 0):

![fov](./assets/doc/field_of_view.png)

For the player to know his surroundings the client sends the command ’VOIR’ and the server responds the following chain of characters (for level 1):\
`{content-square-0, content-square-1, content-square-2, content-square-3}`\

Our player doesn’t even see himself, also if on a box there is more than 1 object, there are all indicated and separated by a space:

![example fov](./assets/doc/example_field_of_view.png)

`{nourriture, player sibur, phiras phiras, }`


### Sound transmission

Sound is a wave that moves in a linear way.\
All the players hear the broadcasts without knowing who emits them. They perceive only the direction the sound comes from and the message.\
The number of the square crossed by the sound before it arrives to the player indicates the direction. This numbering is done through the attribution of `1` to the square in front of the player, then a count down of the squares surrounding the player in the trigonometric direction (counter-clock wise).\
The world is round therefore we will choose the shortest trajectory for the sound between the transmitter to the player for which we calculate.\

The following example indicates the sound trajectory that we must choose, as well as the numbers of the squares around the player. The player receives the broadcast through square `3`.

![broadcast message](./assets/doc/broadcast.png)

In case the broadcast is emitted from the same box as the receiving player, he will get the message from square `0`.


## Mandatory part

1. [Server](#server)
2. [Clients](#clients)
3. [Commands](#commands)
4. [Game spec](#game-spec)
5. [Graphic interface](#gfx)

______________________________________________


### Server

The server needs to be built using any programming language that supports compilation (as long as it works on the dumps).\
The server must be able to manage the world and its population. The server runs under a single process and a single thread 

```
Usage: ./server -p <port> -x <width> -y <height> -n <team> [<team>] [<team>] ... -c <nb> -t <t>
-p port number
-x world width
-y world height
-n team\_name\_1 team\_name\_2 ...
-c number of clients authorized at the beginning of the game
-t time unit divider (the greater t is, the faster the game will go)
```


### Clients

Client can be in any langage, it will control one player by giving orders to the server.

```
Usage: ./client -n <team> -p <port> [-h <hostname>]
-n team\_name
-p port
-h name of the host, by default it'll be localhost
```

The client is autonomous, after its launch the user won’t influence its operation. He pilot a drone.\
Each player is controled by a client. The clients cannot communicate or exchange amongst each other data outside of the game, in any way.\
At the beginning the client has 10 life units, he can therefore survive 1260 time units (ie 1260/t seconds).


### Commands

Each player responds to the following actions and only with the following syntax:

![player commands](./assets/doc/zappy_cmds.png)

The communication between client and server will happen via sockets and tcp. The port used will be an argument of the programs.\
The client will send its requests without waiting for their execution, the server sends back a message confirming successful execution of the requests.\
The connection client to server will happen as such:\
The client open a socket on the port of the server; then the server and the client

![handshake](./assets/doc/handshake.png)

The nb-client indicates the number of clients that can still be accepted by the server for the team team-name.


### Game spec

- The Trantorians have adopted an international time unit. Time unit is a second. If `t=1` `avance` takes 7 seconds.\
We choose by default, `t=10`. t is a integer. The time reference is absolute time.

- `fork` command allows a player to reproduce itself. This execution of this command results in the production of an egg. Once the egg is laid, the player that laid the egg can go around until it hatches.\
When the egg hatches a new player pops. He is oriented randomly.\
This operation authorizes the connection of a new client. The connect_nbr command returns the number of authorized and unauthorized connections for this team.
    - Time to lay an egg: `42/t`
    - Time for the egg to hatch `600/t`

- `expulse` command can expulse all the players sharing the same square. It pushes them in the direction he is looking at. When a client send to the server the kick command, all the clients in this square receive the following line:\
`deplacement <K>\n`\
With K indicating the direction of the square where the player comes from

- `broadcast` command allows to send a message the client must send to the server the following command:\
`broadcast <texte>`\
The server will send to all its client this line:\
`message <K>,<texte>`\
With K indicating the square where the sound comes from.

- `inventory` allows to see what objects the player has and how long it has to live. The server will send back for example the following line :\
`{nourriture 345, sibur 3, phiras 5, ..., deraumere 0}`


### Graphic interface

The project will have to have a graphic visualization client. That client will propose a real-time representation of the world as it is on the server.
The interface will integrate at minima a 2D visualization through icons allowing a representation of the world.\
A 3D interface or any other type of representation will be an appreciated bonus for this project. You also need to include the visualization of the
sounds.\
It must be developped in C, in Rust, in Go, in Python, etc. and will communicate within the network with the server to retrieve the content of the map, teams, inventories, etc. ie everything needed to see what’s going on in the game.\

The server can communicate with graphic server by using this norm :

![rfc4242](./assets/doc/RFC4242.png)

___________________________________________


## How to run

open multiple terminals and run each cargo command in them.

### run 1 team

```
cargo run --release --bin server -- -n lala -c 1 -x 15 -y 15 -p 1312 -t 10
cargo run --release --bin client -- lala 1312
cargo run --release --bin server_gfx
```


### run 4 teams

```
cargo run --release --bin server -- -n lala bibi toto gugu -c 1 -x 15 -y 15 -p 1312 -t 10
cargo run --release --bin client -- lala 1312
cargo run --release --bin client -- bibi 1312
cargo run --release --bin client -- toto 1312
cargo run --release --bin client -- gugu 1312
cargo run --release --bin server_gfx
```

### run by using Makefile

`make run` --> run a game with 4 teams\
`make kill` --> stop the current game
