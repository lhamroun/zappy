use std::process::Command;

pub mod algo
{
    use core::num;

    use rand::{thread_rng, Rng};

    use crate::command::command::{Command, CommandTemplate, AVANCE, BROADCAST, DROITE, EXPULSE, FORK, GAUCHE, INCANTATION, INVENTAIRE, POSE, PREND, VOIR};
    use crate:: player::player::{Orientation, Player, FOOD_CRITICAL, FOOD_LOW, NB_FORK};


    #[derive(Debug, Clone, PartialEq)]
    pub enum Strategy {
        NoStrategy,
        InitialForks,
        GetPath,
        GetFood,
        GetResources,
        BroadcastJoinMaster,
        BroadcastMaster,
    }

    fn random_strategy() -> Command
    {
        println!("random strategy");
        let obj_arg: Vec<String> = ["food".to_string(),
        "linemate".to_string(),
        "deraumere".to_string(),
        "phiras".to_string(),
        "sibur".to_string(),
        "thystame".to_string(),
        "mendiane".to_string()].to_vec();
        let random_cmd: Vec<CommandTemplate> = [POSE, PREND, BROADCAST, AVANCE, DROITE, GAUCHE, INVENTAIRE, EXPULSE, INCANTATION, FORK].to_vec();
        let mut rng = thread_rng().gen_range(0..=9);
        if rng < 3 {
            return Command::new(random_cmd[rng].clone(), Some(obj_arg[thread_rng().gen_range(0..=6)].clone()));
        }
        else {
            return Command::new(random_cmd[rng].clone(), None);
        }
        Command::new(VOIR, None)
    }


    pub fn strat_client(player: &mut Player, random: bool, child_opt: bool) -> Option<Vec<Command>>
    {   
        let mut cmds = Vec::new();

        //println!("strat client");
        match random
        {
            true => {
                cmds.push(random_strategy());
            },
            false => {
                cmds = simple_algo(player, child_opt);
            }
        }
        if cmds.is_empty() {
            return None;
        }
        Some(cmds)
    }

    
    /*
    **  simple strategy is :
    **      fork * 6 --> si pas le joueur master
    **      prend 60 food
    **      prend toutes ressources
    **      broadcast 'go to master'
    **      incantation
    */
    fn simple_algo(player: &mut Player, child_opt: bool) -> Vec<Command>
    {
        let mut cmds = Vec::new();
        let mut strategy_done: bool = false;

        // 6 initial forks by the master player
        if child_opt == false
        {
            println!("strat {:?}\ncmds {:?}\nwait answer {}\nbroadcast square {}\nnb player join {}", player.strategy, player.commands, player.wait_answer, player.tmp, player.nb_player_join);
            if player.strategy == Strategy::NoStrategy {
                player.strategy = Strategy::InitialForks;
            }
            if player.strategy == Strategy::InitialForks {
                for i in 0..NB_FORK {
                    cmds.push(Command::new(FORK, None));
                }
                cmds.push(Command::new(VOIR, None));
                player.strategy = Strategy::GetFood;
                strategy_done = true;
            }
        }
        else {
            if player.strategy == Strategy::BroadcastJoinMaster &&
                (player.inventaire.food < FOOD_CRITICAL
                || player.inventaire.linemate < 9
                || player.inventaire.deraumere < 8
                || player.inventaire.sibur < 10
                || player.inventaire.phiras < 6
                || player.inventaire.mendiane < 5
                || player.inventaire.thystame < 1) {
                player.strategy = Strategy::GetPath;
            }
            else if player.strategy == Strategy::BroadcastJoinMaster
                && player.inventaire.food >= FOOD_CRITICAL
                && player.inventaire.linemate >= 9
                && player.inventaire.deraumere >= 8
                && player.inventaire.sibur >= 10
                && player.inventaire.phiras >= 6
                && player.inventaire.mendiane >= 5
                && player.inventaire.thystame >= 1 {
                strategy_done = true;
                if player.wait_answer == true {
                    if player.commands.len() > 0 {
                        player.commands = vec![player.commands[0].clone()];
                    }
                    match player.commands[0].name {
                        "gauche" => {
                            cmds.push(Command::new(DROITE, None));
                        },
                        "droite" => {
                            cmds.push(Command::new(GAUCHE, None));
                        },
                        "avance" => {
                            cmds.push(Command::new(GAUCHE, None));
                            cmds.push(Command::new(GAUCHE, None));
                            cmds.push(Command::new(AVANCE, None));
                            cmds.push(Command::new(GAUCHE, None));
                            cmds.push(Command::new(GAUCHE, None));
                        },
                        _ => ()
                    }
                }
                match player.tmp {
                    -1 => {
                        println!("already on master cell");
                    },
                    0 => {
                        cmds.push(Command::new(BROADCAST, Some("imhere".to_string())));
                        player.tmp = -1; // init to 0
                    },
                    1 => {
                        cmds.push(Command::new(AVANCE, None));
                    },
                    2 => {
                        cmds.push(Command::new(AVANCE, None));
                        cmds.push(Command::new(GAUCHE, None));
                        cmds.push(Command::new(AVANCE, None));
                    },
                    3 => {
                        cmds.push(Command::new(GAUCHE, None));
                        cmds.push(Command::new(AVANCE, None));
                    },
                    4 => {
                        cmds.push(Command::new(GAUCHE, None));
                        cmds.push(Command::new(AVANCE, None));
                        cmds.push(Command::new(GAUCHE, None));
                        cmds.push(Command::new(AVANCE, None));
                    },
                    5 => {
                        cmds.push(Command::new(GAUCHE, None));
                        cmds.push(Command::new(GAUCHE, None));
                        cmds.push(Command::new(AVANCE, None));
                    },
                    6 => {
                        cmds.push(Command::new(DROITE, None));
                        cmds.push(Command::new(AVANCE, None));
                        cmds.push(Command::new(DROITE, None));
                        cmds.push(Command::new(AVANCE, None));
                    },
                    7 => {
                        cmds.push(Command::new(DROITE, None));
                        cmds.push(Command::new(AVANCE, None));

                    },
                    8 => {
                        cmds.push(Command::new(AVANCE, None));
                        cmds.push(Command::new(DROITE, None));
                        cmds.push(Command::new(AVANCE, None));
                    },
                    _ => { println!("fail to get broadcast square from master"); }
                }
                player.strategy = Strategy::NoStrategy;
            }
        }
        if (strategy_done == false
                && (player.strategy == Strategy::GetPath
                    || (player.strategy == Strategy::NoStrategy && child_opt == true))) {
            if player.inventaire.food >= FOOD_CRITICAL
                && player.inventaire.linemate >= 9
                && player.inventaire.deraumere >= 8
                && player.inventaire.sibur >= 10
                && player.inventaire.phiras >= 6
                && player.inventaire.mendiane >= 5
                && player.inventaire.thystame >= 1 {
                }
            else {
                player.strategy = Strategy::GetFood;
                if player.inventaire.food >= FOOD_LOW {
                    player.strategy = Strategy::GetResources;
                }
                strategy_done = true;
            }
        }
        else if strategy_done == false && player.strategy == Strategy::GetFood && player.inventaire.food <= FOOD_LOW {
            // path finding to get food
            strategy_done = true;
            let resource_path = get_path_for_resource(player, String::from("food"));
            cmds.extend(resource_path);
            cmds.push(Command::new(INVENTAIRE, None));
            player.strategy = Strategy::GetPath;
        }
        if strategy_done == false && player.strategy == Strategy::GetPath && player.inventaire.food > FOOD_CRITICAL {
            player.strategy = Strategy::GetResources;
        }
        else if strategy_done == false && player.strategy == Strategy::GetResources && player.inventaire.linemate < 9 {
            strategy_done = true;
            let resource_path = get_path_for_resource(player, String::from("linemate"));
            cmds.extend(resource_path);
            player.strategy = Strategy::GetPath;
        }
        else if strategy_done == false && player.strategy == Strategy::GetResources && player.inventaire.deraumere < 8 {
            strategy_done = true;
            let resource_path = get_path_for_resource(player, String::from("deraumere"));
            cmds.extend(resource_path);
            player.strategy = Strategy::GetPath;
        }
        else if strategy_done == false && player.strategy == Strategy::GetResources && player.inventaire.sibur < 10 {
            strategy_done = true;
            let resource_path = get_path_for_resource(player, String::from("sibur"));
            cmds.extend(resource_path);
            player.strategy = Strategy::GetPath;
        }
        else if strategy_done == false && player.strategy == Strategy::GetResources && player.inventaire.phiras < 6 {
            strategy_done = true;
            let resource_path = get_path_for_resource(player, String::from("phiras"));
            cmds.extend(resource_path);
            player.strategy = Strategy::GetPath;
        }
        else if strategy_done == false && player.strategy == Strategy::GetResources && player.inventaire.mendiane < 5 {
            strategy_done = true;
            let resource_path = get_path_for_resource(player, String::from("mendiane"));
            cmds.extend(resource_path);
            player.strategy = Strategy::GetPath;
        }
        else if strategy_done == false && player.strategy == Strategy::GetResources && player.inventaire.thystame < 1 {
            strategy_done = true;
            let resource_path = get_path_for_resource(player, String::from("thystame"));
            cmds.extend(resource_path);
            player.strategy = Strategy::GetPath;
        }
        else if strategy_done == false && child_opt == false && player.strategy == Strategy::BroadcastMaster {
            println!("I'm master and I run broadcast --> {} players join me", player.nb_player_join);
            strategy_done = true;
            cmds.push(Command::new(BROADCAST, Some("join-me".to_string())));
            cmds.push(Command::new(INVENTAIRE, None));
            
            if player.nb_player_join >= NB_FORK {
                cmds = vec!(Command::new(INCANTATION, None));
            }
            else {
                println!("master wait for other players");
            }
        }
        else if strategy_done == false
            && player.inventaire.linemate == 9
            && player.inventaire.deraumere == 8
            && player.inventaire.sibur == 10
            && player.inventaire.mendiane == 5
            && player.inventaire.phiras == 6
            && player.inventaire.thystame == 1 {
            // waiting for other team members and run incantation
            strategy_done = true;
            if child_opt == false {
                player.strategy = Strategy::BroadcastMaster;
            }
            else {
                player.strategy = Strategy::NoStrategy;
            }
        }
        cmds
    }


    fn get_path_for_resource(player: &mut Player, resource: String) -> Vec<Command> {
        let mut cmds: Vec<Command> = Vec::new();
        let mut x: i32 = -1;
        let mut y: i32 = -1;
        let mut found = false;

        for tmp_y in 0..player.map.len() {
            //if  found == true {
            //    break ;
            //}
            for  tmp_x in 0..player.map[0].len() {
                //if  found == true {
                //    break ;
                //}
                match resource.as_str() {
                    "food"      => {
                        //println!("trying to check food");
                        if let Some(food) = player.map[tmp_y][tmp_x].food {
                            if food.count > 0 {
                                println!("find food on cell ({}, {}) and player is on ({}, {}) {:?}",
                                    tmp_x,
                                    tmp_y,
                                    player.coord.x,
                                    player.coord.y,
                                    player.orientation);
                                if x != -1 && y != -1 {
                                    if i32::abs(player.coord.x as i32 - x) + i32::abs(player.coord.y as i32 - y) > i32::abs(player.coord.x as i32 - tmp_x as i32) + i32::abs(player.coord.y as i32 - tmp_y as i32) {
                                        x = tmp_x as i32;
                                        y = tmp_y as i32;
                                    }
                                }
                                else if x == -1 && y == -1 {
                                    x = tmp_x as i32;
                                    y = tmp_y as i32;
                                }
                                found = true;
                            }
                        }
                    },
                    "linemate"  => {
                        //println!("trying to check linemate");
                        if let Some(linemate) = player.map[tmp_y][tmp_x].linemate {
                            if linemate.count > 0 {
                                println!("find linemate on cell ({}, {}) and player is on ({}, {}) {:?}",
                                    tmp_x,
                                    tmp_y,
                                     player.coord.x,
                                    player.coord.y,
                                    player.orientation);
                                if x != -1 && y != -1 {
                                    if i32::abs(player.coord.x as i32 - x) + i32::abs(player.coord.y as i32 - y) > i32::abs(player.coord.x as i32 - tmp_x as i32) + i32::abs(player.coord.y as i32 - tmp_y as i32) {
                                        x = tmp_x as i32;
                                        y = tmp_y as i32;
                                    }
                                }
                                else if x == -1 && y == -1 {
                                    x = tmp_x as i32;
                                    y = tmp_y as i32;
                                }
                                found = true;
                            }
                        }
                    },
                    "deraumere" => {
                        //println!("trying to check deraumere");
                        if let Some(deraumere) = player.map[tmp_y][tmp_x].deraumere {
                            if deraumere.count > 0 {
                                println!("find deraumere on cell ({}, {}) and player is on ({}, {}) {:?}",
                                    tmp_x,
                                    tmp_y,
                                    player.coord.x,
                                    player.coord.y,
                                    player.orientation);
                                if x != -1 && y != -1 {
                                    if i32::abs(player.coord.x as i32 - x) + i32::abs(player.coord.y as i32 - y) > i32::abs(player.coord.x as i32 - tmp_x as i32) + i32::abs(player.coord.y as i32 - tmp_y as i32) {
                                        x = tmp_x as i32;
                                        y = tmp_y as i32;
                                    }
                                }
                                else if x == -1 && y == -1 {
                                    x = tmp_x as i32;
                                    y = tmp_y as i32;
                                }
                                found = true;
                            }
                        }
                    },
                    "phiras"    => {
                        //println!("trying to check phiras");
                        if let Some(phiras) = player.map[tmp_y][tmp_x].phiras {
                            if phiras.count > 0 {
                                println!("find phiras on cell ({}, {}) and player is on ({}, {}) {:?}",
                                    tmp_x,
                                    tmp_y,
                                    player.coord.x,
                                    player.coord.y,
                                    player.orientation);
                                if x != -1 && y != -1 {
                                    if i32::abs(player.coord.x as i32 - x) + i32::abs(player.coord.y as i32 - y) > i32::abs(player.coord.x as i32 - tmp_x as i32) + i32::abs(player.coord.y as i32 - tmp_y as i32) {
                                        x = tmp_x as i32;
                                        y = tmp_y as i32;
                                    }
                                }
                                else if x == -1 && y == -1 {
                                    x = tmp_x as i32;
                                    y = tmp_y as i32;
                                }
                                found = true;
                            }
                        }
                     },
                    "sibur"     => {
                        //println!("trying to check sibur");
                        if let Some(sibur) = player.map[tmp_y][tmp_x].sibur {
                            if sibur.count > 0 {
                                println!("find sibur on cell ({}, {}) and player is on ({}, {}) {:?}",
                                    tmp_x,
                                    tmp_y,
                                    player.coord.x,
                                    player.coord.y,
                                    player.orientation);
                                if x != -1 && y != -1 {
                                    if i32::abs(player.coord.x as i32 - x) + i32::abs(player.coord.y as i32 - y) > i32::abs(player.coord.x as i32 - tmp_x as i32) + i32::abs(player.coord.y as i32 - tmp_y as i32) {
                                        x = tmp_x as i32;
                                        y = tmp_y as i32;
                                    }
                                }
                                else if x == -1 && y == -1 {
                                    x = tmp_x as i32;
                                    y = tmp_y as i32;
                                }
                                found = true;
                            }
                        }
                    },
                    "thystame"  => {
                        //println!("trying to check thystame");
                        if let Some(thystame) = player.map[tmp_y][tmp_x].thystame {
                            if thystame.count > 0 {
                                println!("find thystame on cell ({}, {}) and player is on ({}, {}) {:?}",
                                    tmp_x,
                                    tmp_y,
                                    player.coord.x,
                                    player.coord.y,
                                    player.orientation);
                                if x != -1 && y != -1 {
                                    if i32::abs(player.coord.x as i32 - x) + i32::abs(player.coord.y as i32 - y) > i32::abs(player.coord.x as i32 - tmp_x as i32) + i32::abs(player.coord.y as i32 - tmp_y as i32) {
                                        x = tmp_x as i32;
                                        y = tmp_y as i32;
                                    }
                                }
                                else if x == -1 && y == -1 {
                                    x = tmp_x as i32;
                                    y = tmp_y as i32;
                                }
                                found = true;
                            }
                        }
                    },
                    "mendiane"  => {
                        //println!("trying to check mendiane");
                        if let Some(mendiane) = player.map[tmp_y][tmp_x].mendiane {
                            if mendiane.count > 0 {
                                println!("find mendiane on cell ({}, {}) and player is on ({}, {}) {:?}",
                                    tmp_x,
                                    tmp_y,
                                    player.coord.x,
                                    player.coord.y,
                                    player.orientation);
                                if x != -1 && y != -1 {
                                    if i32::abs(player.coord.x as i32 - x) + i32::abs(player.coord.y as i32 - y) > i32::abs(player.coord.x as i32 - tmp_x as i32) + i32::abs(player.coord.y as i32 - tmp_y as i32) {
                                        x = tmp_x as i32;
                                        y = tmp_y as i32;
                                    }
                                }
                                else if x == -1 && y == -1 {
                                    x = tmp_x as i32;
                                    y = tmp_y as i32;
                                }
                                found = true;
                            }
                        }
                    },
                    _ => { println!("error: resource {} does not exist", resource); }
                }
            }
        }
        if x == -1 && y == -1 {
            if player.map[player.coord.y as usize][player.coord.x as usize].food == None
                && player.map[player.coord.y as usize][player.coord.x as usize].player == None
                && player.map[player.coord.y as usize][player.coord.x as usize].linemate == None
                && player.map[player.coord.y as usize][player.coord.x as usize].deraumere == None
                && player.map[player.coord.y as usize][player.coord.x as usize].phiras == None
                && player.map[player.coord.y as usize][player.coord.x as usize].sibur == None
                && player.map[player.coord.y as usize][player.coord.x as usize].thystame == None
                && player.map[player.coord.y as usize][player.coord.x as usize].mendiane == None {
                    cmds.push(Command::new(VOIR, None));
                }
            else {
                let random_cmd: Vec<CommandTemplate> = vec![AVANCE, GAUCHE, DROITE];
                let tmp_rng = thread_rng().gen_range(0..=4);
                for _ in 0..tmp_rng {
                    cmds.push(Command::new(random_cmd[thread_rng().gen_range(0..=2)].clone(), None));
                }
                cmds.push(Command::new(VOIR, None));
                cmds.push(Command::new(INVENTAIRE, None));
            }
            return cmds;
        }
        else {
            let delta_x = player.coord.x as i32 - x;
            let delta_y = player.coord.y as i32 - y;
            let mut tmp_orientation = player.orientation.clone();

            /* find the X path */
            if delta_x < 0 {
                match player.orientation
                {
                    Orientation::N => {
                        cmds.push(Command::new(DROITE, None));
                        tmp_orientation = Orientation::E;
                        for i in 0..(delta_x * -1) {
                            cmds.push(Command::new(AVANCE, None));
                        }
                    },
                    Orientation::S => {
                        cmds.push(Command::new(GAUCHE, None));
                        tmp_orientation = Orientation::E;
                        for i in 0..(delta_x * -1) {
                            cmds.push(Command::new(AVANCE, None)); 
                        }
                    },
                    Orientation::E => {
                        for i in 0..(delta_x * -1) {
                        cmds.push(Command::new(AVANCE, None));
                    }},
                    Orientation::O => {
                        cmds.push(Command::new(GAUCHE, None));
                        cmds.push(Command::new(GAUCHE, None));
                        tmp_orientation = Orientation::E;
                        for i in 0..(delta_x * -1) {
                            cmds.push(Command::new(AVANCE, None));
                        }
                    },
                }
            }
            else if delta_x > 0 {
                match player.orientation
                {
                    Orientation::N => {
                        cmds.push(Command::new(GAUCHE, None));
                        tmp_orientation = Orientation::O;
                        for i in 0..delta_x {
                            cmds.push(Command::new(AVANCE, None));
                        }
                    },
                    Orientation::S => {
                        cmds.push(Command::new(DROITE, None));
                        tmp_orientation = Orientation::O;
                        for i in 0..delta_x {
                            cmds.push(Command::new(AVANCE, None));
                        }
                    },
                    Orientation::E => {
                        cmds.push(Command::new(GAUCHE, None));
                        cmds.push(Command::new(GAUCHE, None));
                        tmp_orientation = Orientation::O;
                        for i in 0..delta_x {
                        cmds.push(Command::new(AVANCE, None));
                    }},
                    Orientation::O => {
                        for i in 0..delta_x {
                            cmds.push(Command::new(AVANCE, None));
                        }
                    },
                }
            }
            else {
                //println!("on the same line X");
            }

            /* find the Y path */
            if delta_y < 0 {
                match tmp_orientation
                {
                    Orientation::N => {
                        cmds.push(Command::new(GAUCHE, None));
                        cmds.push(Command::new(GAUCHE, None));
                        //tmp_orientation = Orientation::S;
                        for i in 0..(delta_y * -1) {
                            cmds.push(Command::new(AVANCE, None));
                        }
                    },
                    Orientation::S => {
                        for i in 0..(delta_y * -1) {
                            cmds.push(Command::new(AVANCE, None));
                        }
                    },
                    Orientation::E => {
                        cmds.push(Command::new(DROITE, None));
                        //tmp_orientation = Orientation::S;
                        for i in 0..(delta_y * -1) {
                        cmds.push(Command::new(AVANCE, None));
                    }},
                    Orientation::O => {
                        cmds.push(Command::new(GAUCHE, None));
                        //tmp_orientation = Orientation::S;
                        for i in 0..(delta_y * -1) {
                            cmds.push(Command::new(AVANCE, None));
                        }
                    },
                }
            }
            else if delta_y > 0 {
                match tmp_orientation
                {
                    Orientation::N => {
                        for i in 0..delta_y {
                            cmds.push(Command::new(AVANCE, None));
                        }
                    },
                    Orientation::S => {
                        cmds.push(Command::new(GAUCHE, None));
                        cmds.push(Command::new(GAUCHE, None));
                        for i in 0..delta_y {
                            cmds.push(Command::new(AVANCE, None));
                        }
                    },
                    Orientation::E => {
                        cmds.push(Command::new(GAUCHE, None));
                        for i in 0..delta_y {
                        cmds.push(Command::new(AVANCE, None));
                    }},
                    Orientation::O => {
                        cmds.push(Command::new(DROITE, None));
                        for i in 0..delta_y {
                            cmds.push(Command::new(AVANCE, None));
                        }
                    },
                }
            }
            else {
                //println!("on the same line Y");
            }
            cmds.push(Command::new(PREND, Some(resource.clone())));
        }        
        cmds
    }
}