pub mod command
{
    use std::io::{LineWriter, Write};
    use crate::{map::map::Dimensions, player::player::Player, utils::utils::{add_ressource_into_inventaire, first_word, get_ressource_from_cmd, move_players_from_expulse, remove_ressource_into_inventaire}};

    #[derive(Debug, Clone)]
    pub struct CommandTemplate
    {
        pub name    : &'static str,
        pub arg     : Option<String>,
        pub count   : u16,
    }

    pub const NO_ACTION: CommandTemplate     = CommandTemplate{ name: "",            arg: None,                  count: 0};
    pub const AVANCE: CommandTemplate        = CommandTemplate{ name: "avance",      arg: None,                  count: 7};
    pub const DROITE: CommandTemplate        = CommandTemplate{ name: "droite",      arg: None,                  count: 7};
    pub const GAUCHE: CommandTemplate        = CommandTemplate{ name: "gauche",      arg: None,                  count: 7};
    pub const VOIR: CommandTemplate          = CommandTemplate{ name: "voir",        arg: None,                  count: 7};
    pub const INVENTAIRE: CommandTemplate    = CommandTemplate{ name: "inventaire",  arg: None,                  count: 1};
    pub const PREND: CommandTemplate         = CommandTemplate{ name: "prend",       arg: Some(String::new()),   count: 7};
    pub const POSE: CommandTemplate          = CommandTemplate{ name: "pose",        arg: Some(String::new()),   count: 7};
    pub const EXPULSE: CommandTemplate       = CommandTemplate{ name: "expulse",     arg: None,                  count: 7};
    pub const BROADCAST: CommandTemplate     = CommandTemplate{ name: "broadcast",   arg: Some(String::new()),   count: 7};
    pub const INCANTATION: CommandTemplate   = CommandTemplate{ name: "incantation", arg: None,                  count: 300};
    pub const FORK: CommandTemplate          = CommandTemplate{ name: "fork",        arg: None,                  count: 42};
    pub const CONNECT_NBR: CommandTemplate   = CommandTemplate{ name: "connect_nbr", arg: None,                  count: 0};

    pub const COMMANDS: [CommandTemplate; 13] = [AVANCE, DROITE, GAUCHE, VOIR, INVENTAIRE, PREND, POSE, EXPULSE, BROADCAST, INCANTATION, FORK, CONNECT_NBR, NO_ACTION];

    #[derive(Debug, Clone)]
    pub struct Command
    {
        pub name    : &'static str,
        pub arg     : Option<String>,
        pub count   : u16,
    }

    impl Command
    {
        pub fn new(cmd_template: CommandTemplate, obj_arg: Option<String>) -> Self
        {
            let command = Command {
                name: cmd_template.name,
                arg: obj_arg,
                count: cmd_template.count,
            };
            command
        }
    }
    
    pub fn send_cmd<W: Write>(writer: &mut LineWriter<W>, command: &Command) -> Result<(), String>
    {
        let mut obj_arg = String::new();
        if let Some(obj) = &command.arg {
            obj_arg.push(' ');
            obj_arg.push_str(obj);
        }
        let cmd_to_send = format!("{}{}\n", command.name.to_string(), obj_arg);
        println!("send command ---> {}", cmd_to_send);
        if let Ok(_) = writer.write(cmd_to_send.as_bytes())
        {
            let _ = writer.flush();
            return Ok(());
        }
        Err(String::from("Error during send_cmd"))
    }

    pub fn exec_cmd(player: &mut Player, dimensions: &Dimensions, teamname: String, port: u16)
    {
        //println!("fn exec_cmd");
        if let Some(cmd) = &player.current_cmd
        {
            match first_word(cmd.name) {
                "avance" => {
                    player.avance(dimensions);
                },
                "droite" => {
                    player.droite();
                },
                "gauche" => {
                    player.gauche();
                },
                "prend" => {
                    let resource = get_ressource_from_cmd(cmd.clone());
                    add_ressource_into_inventaire(player, resource.clone());
                    match resource.as_str()
                    {
                        "food" =>       {
                            if let Some(tmp_food) = player.map[player.coord.y as usize][player.coord.x as usize].food.as_mut() {
                                tmp_food.count -= 1;
                            }
                        },
                        "linemate" =>   {
                            if let Some(tmp_linemate) = player.map[player.coord.y as usize][player.coord.x as usize].linemate.as_mut() {
                                tmp_linemate.count -= 1;
                            }
                        },
                        "deraumere" =>  {
                            if let Some(tmp_deraumere) = player.map[player.coord.y as usize][player.coord.x as usize].deraumere.as_mut() {
                                tmp_deraumere.count -= 1;
                            }
                        },
                        "phiras" =>     {
                            if let Some(tmp_phiras) = player.map[player.coord.y as usize][player.coord.x as usize].phiras.as_mut() {
                                tmp_phiras.count -= 1;
                            }
                        },
                        "thystame" =>   {
                            if let Some(tmp_thystame) = player.map[player.coord.y as usize][player.coord.x as usize].thystame.as_mut() {
                                tmp_thystame.count -= 1;
                            }
                        },
                        "sibur" =>      {
                            if let Some(tmp_sibur) = player.map[player.coord.y as usize][player.coord.x as usize].sibur.as_mut() {
                                tmp_sibur.count -= 1;
                            }
                        },
                        "mendiane" =>   {
                            if let Some(tmp_mendiane) = player.map[player.coord.y as usize][player.coord.x as usize].mendiane.as_mut() {
                                tmp_mendiane.count -= 1;
                            }
                        },
                        _ =>            ()
                    }
                },
                "pose" => {
                    let resource = get_ressource_from_cmd(cmd.clone());
                    remove_ressource_into_inventaire(player, resource.clone());
                    match resource.as_str()
                    {
                        "food" =>       { player.map[player.coord.y as usize][player.coord.x as usize].food.unwrap().count      += 1; },
                        "linemate" =>   { player.map[player.coord.y as usize][player.coord.x as usize].linemate.unwrap().count  += 1; },
                        "deraumere" =>  { player.map[player.coord.y as usize][player.coord.x as usize].deraumere.unwrap().count += 1; },
                        "phiras" =>     { player.map[player.coord.y as usize][player.coord.x as usize].phiras.unwrap().count    += 1; },
                        "thystame" =>   { player.map[player.coord.y as usize][player.coord.x as usize].thystame.unwrap().count  += 1; },
                        "sibur" =>      { player.map[player.coord.y as usize][player.coord.x as usize].sibur.unwrap().count     += 1; },
                        "mendiane" =>   { player.map[player.coord.y as usize][player.coord.x as usize].mendiane.unwrap().count  += 1; },
                        _ =>            ()
                    }
                },
                "expulse" => move_players_from_expulse(player, dimensions),
                "broadcast" => (),
                "fork" => player.fork(teamname, port),
                err => {
                    println!("PROBLEM IN COMMAND NAME --> you have {}", err);
                }
            }
        }
    }

}