
pub mod connexion
{
    use std::io::{BufRead, Write};

    use crate::{map::map::Dimensions, stream::stream::BufIo};

    pub fn connect_to_server(buf_io: &mut BufIo, teamname: &str) -> Dimensions
    {
        let mut buffer = String::new();

        // BIENVENUE
        //println!("waiting for 'BIENVENUE' packet");
        while let Err(err) = buf_io.input.read_line(&mut buffer) {
            println!("read on stream error: {}", err);
        }
        if buffer == "BIENVENUE\n" {
            //println!("Receive 'BIENVENUE' from server. Send back teamname = {:?}", teamname);
            while let Err(err) = buf_io.output.write_fmt(format_args!("{}\n", teamname)) {
                println!("Could not write on stream: {}", err);
            }
        }
        buffer.clear();

        // <nb-client>
        //println!("waiting for <nb-client> and <x> <y> packets");
        while let Err(err) = buf_io.input.read_line(&mut buffer) {
            println!("read on stream error: {}", err);
        }
        buffer.pop();
        //println!("receive msg from server : {}", buffer);
        buffer.parse::<u8>().expect("Failed to parse nb_client as u8");
        buffer.clear();

        // <x> <y>
        while let Err(err) = buf_io.input.read_line(&mut buffer) {
            println!("read on stream error: {}", err);
        }
        buffer.pop();
        //println!("receive msg from server : {}", buffer);
        let mut size_map = buffer.split(' ');
        let width = size_map.next().expect("Expected a value for map width")
                            .parse::<u8>().expect("Failed to parse map width as u8");
        let height = size_map.next().expect("Expected a value for map height")
                            .parse::<u8>().expect("Failed to parse map height as u8");
        
        Dimensions::new(width, height)
    }
}