pub mod debug_file_mode
{
    use std::{fs::File, io::{BufRead, BufReader}};

    use crate::{command::command::Command, NB_STACKED_CMDS};
    use crate::utils::utils::string_to_cmd;

    pub fn dbg_parse_cmd_from_file(cmd_sent: &u8, reader: &mut BufReader<File>) -> Option<Command>
    {
        if *cmd_sent < NB_STACKED_CMDS {
            let mut file_buf = String::new();
            if let Ok(read_result) = reader.read_line(&mut file_buf) {
                if read_result > 0 {
                    //println!("into DBG file, read line : '{}'", file_buf);
                    return string_to_cmd(&file_buf);
                }
            }
            else {
                println!("no line to read");
            }
        }
        None
    }
    
}