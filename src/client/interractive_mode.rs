pub mod interractive_mode
{
    use crate::command::command::{Command, AVANCE, DROITE, EXPULSE, FORK, GAUCHE, INCANTATION, POSE, PREND};
    use device_query::{DeviceState, Keycode, DeviceQuery};

    pub fn input_query_to_command() -> Option<Command>
    {

        let device_state = DeviceState::new();
    
        loop {
            let keys: Vec<Keycode> = device_state.get_keys();
            //println!("input key {:?}", device_state.get_keys());
            if keys.contains(&Keycode::Up) { return Some(Command::new(AVANCE, None)); }
            if keys.contains(&Keycode::Left) { return Some(Command::new(GAUCHE, None)); }
            if keys.contains(&Keycode::Right) { return Some(Command::new(DROITE, None)); }
            if keys.contains(&Keycode::Key1) {
                if keys.contains(&Keycode::LShift) { return Some(Command::new(POSE, Some("food".to_string()))); }
                else { return Some(Command::new(PREND, Some("food".to_string()))); }
            }
            if keys.contains(&Keycode::Key2) {
                if keys.contains(&Keycode::LShift) { return Some(Command::new(POSE, Some("linemate".to_string()))); }
                else { return Some(Command::new(PREND, Some("linemate".to_string()))); }
            }
            if keys.contains(&Keycode::Key3) {
                if keys.contains(&Keycode::LShift) { return Some(Command::new(POSE, Some("deraumere".to_string()))); }
                else { return Some(Command::new(PREND, Some("deraumere".to_string()))); }
            }
            if keys.contains(&Keycode::Key4) {
                if keys.contains(&Keycode::LShift) { return Some(Command::new(POSE, Some("sibur".to_string()))); }
                else { return Some(Command::new(PREND, Some("sibur".to_string()))); }
            }
            if keys.contains(&Keycode::Key5) {
                if keys.contains(&Keycode::LShift) { return Some(Command::new(POSE, Some("mendiane".to_string()))); }
                else { return Some(Command::new(PREND, Some("mendiane".to_string()))); }
            }
            if keys.contains(&Keycode::Key6) {
                if keys.contains(&Keycode::LShift) { return Some(Command::new(POSE, Some("phiras".to_string()))); }
                else { return Some(Command::new(PREND, Some("phiras".to_string()))); }
            }
            if keys.contains(&Keycode::Key7) {
                if keys.contains(&Keycode::LShift) { return Some(Command::new(POSE, Some("thystame".to_string()))); }
                else { return Some(Command::new(PREND, Some("thystame".to_string()))); }
            }
            if keys.contains(&Keycode::F) { return Some(Command::new(FORK, None)); }
            if keys.contains(&Keycode::E) { return Some(Command::new(EXPULSE, None)); }
            if keys.contains(&Keycode::I) { return Some(Command::new(INCANTATION, None)); }
        }
    }
}