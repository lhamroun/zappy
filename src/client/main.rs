pub mod map;
pub mod command;
pub mod player;
pub mod stream;
pub mod connexion;
pub mod debug_file_mode;
pub mod utils;
pub mod interractive_mode;
pub mod algo;
pub mod process;

use std::fs::File;
use std::time::Duration;
use std::{env, io};
use std::net::TcpStream;
use std::io::{BufReader, Read, Write};

use bevy_inspector_egui::egui::TextBuffer;
use command::command::send_cmd;

use connexion::connexion::connect_to_server;
use debug_file_mode::debug_file_mode::dbg_parse_cmd_from_file;
use stream::stream::{ServerAnswer, AnswerType};
use utils::utils::{copy_until_char, first_word};
use process::process::{process_spontaneous_paquet, process_command_packet};

use crate::map::map::Dimensions;
use crate::player::player::Player;
use crate::stream::stream::BufIo;
use crate::algo::algo::strat_client;
use crate::interractive_mode::interractive_mode::input_query_to_command;

const NB_STACKED_CMDS: u8 = 10;
const RANDOM_CMDS: bool = false;

fn parsing(args: &Vec<String>) -> bool {
    if !args.contains(&"--child".to_string()) {
        #[cfg(feature = "dbg")] {
            if args.len() != 4 {
                println!("usage: client --features dbg <teamname> <port> <command file path> [--auth]");
                return false;
            }
        }
        #[cfg(not(feature = "dbg"))] {
            if args.len() == 3 || (args.len() == 4 && args[3] == "--auth") {
                // 
            }
            else {
                println!("usage: client <teamname> <port> [--auth]");
                return false;
            }
        }
    }
    let parsed: Result<i32, _> = args[2].trim().parse();
    match parsed {
        Err(_) => {
            println!("port argument should be integer");
            return false;
         },
        _ => ()
    }
    true
}

fn main()
{
    #[allow(unused_mut)]
    let mut reader: BufReader<File>;
    let args: Vec<String> = env::args().collect();
    let mut cmd_sent: u8 = 0;
    let mut buffer = String::new();
    let mut incomming = [0u8; 4096];
    
    if !parsing(&args) {
        return ;
    }
    let teamname = args[1].clone();
    let stream = TcpStream::connect(format!("localhost:{}", args[2].clone()));
    if let Err(_) = stream {
        println!("Please retry later, server is OFF");
        return ;
    }
    let stream = stream.expect("Error during connexion");

    // stream timeout = 10 ms
    stream.set_read_timeout(Some(Duration::new(0, 10000000)))
                .expect("set_read_timeout activation failed");

    // Create a BufReader and LineWriter wrapping the TcpStream
    let mut buf_io = BufIo::new(&stream).expect("BufTcpStream creation failed");
    let mut dimensions = Dimensions::new(0, 0);
    if args.contains(&"--child".to_string()) == false {
        dimensions = connect_to_server(&mut buf_io, &*teamname);
        if args.contains(&"--auth".to_string()) == true {
            let mut input = String::new();
            println!("Please, enter password:");
            match io::stdin().read_line(&mut input) {
                Ok(_) => {
                    println!("You entered: {}", input.trim());
                    buf_io.output.write(input.as_bytes());
                    buf_io.output.flush();
                }
                Err(error) => {
                    eprintln!("Error reading input: {}", error);
                }
            }   
        }
    }
    else {
        let mut width = 0;
        let mut height = 0;

        // child process is created with option --width=X and --child to identify it and not run again the connexion init protocol
        for arg in &args {
            if arg.starts_with("--width=") {
                // Extraire la valeur après "=" et la convertir en u8
                if let Some(val) = arg.split('=').nth(1) {
                    width = val.parse().expect("Invalid width value");
                }
            } else if arg.starts_with("--height=") {
                // Extraire la valeur après "=" et la convertir en u8
                if let Some(val) = arg.split('=').nth(1) {
                    height = val.parse().expect("Invalid height value");
                }
            }
        }

        dimensions = Dimensions::new(width, height);
    }
    let mut player = Player::new(&dimensions);
    println!("Hello from client {}!", args[1]);

    #[cfg(feature = "dbg")] {
        let file = File::open(&*args.get(3).expect("You need to specify an existing a file with dbg features"))
            .expect("File doesn't exist");
        reader = BufReader::new(file);
    }


    loop
    {
        // define command list if player doesn't have staregy yet
        if player.commands.is_empty()
        {
            #[cfg(feature = "dbg")] {
                // a voir si on parse pas 10 lignes par 10 lignes
                let file_cmd = dbg_parse_cmd_from_file(&cmd_sent, &mut reader);
                if let Some(cmd) = file_cmd
                {
                    player.commands.push(cmd);
                    println!("commands from DBG file : {:?}", player.commands);
                }
            }
    
            #[cfg(feature = "it")] {
                let interractive_cmd = input_query_to_command();
                if let Some(cmd) = interractive_cmd
                {
                    player.commands.push(cmd);
                    println!("commands from Interractive mode : {:?}", player.commands);
                }
            }
    
            #[cfg(not(any(feature = "it", feature = "dbg")))] {
                let child_opt = args.contains(&"--child".to_string());
                let strategy_cmds = strat_client(&mut player, RANDOM_CMDS, child_opt);
                if let Some(cmds) = strategy_cmds
                {
                    player.commands = cmds;
                    // util for print client commands
                    println!("- - - - - - - - - - -");
                    println!("commands from player strategy :");
                    for cmd in player.commands.clone() {
                        let mut cmd_arg = String::from("");
                        if cmd.name.as_str() == "prend" || cmd.name.as_str() == "pose" || cmd.name.as_str() == "broadcast" {
                            cmd_arg = cmd.arg.unwrap();
                        } 
                        println!("{:?} {}", cmd.name, cmd_arg);
                    }
                    println!("- - - - - - - - - - -");
                }
            }
        }

        // send cmds to server
        if !player.commands.is_empty()
        {
            player.current_cmd = Some(player.commands[0].clone());

            if let Some(cmd) = &player.current_cmd
            {
                if player.wait_answer == false
                {
                    if let Ok(_) = send_cmd(&mut buf_io.output, cmd)
                    {
                        player.wait_answer = true;
                    }
                }
            }
        }

        // read input tcp packet from server
        if let Ok(n) = buf_io.input.read(&mut incomming)
        {
            // server close client TCP connexion
            if n == 0
            {
                println!("----------------------");
                println!("--- end connection ---");
                println!("----------------------");
                break ;
            }
            buffer = copy_until_char(&incomming, b'\n');
            println!("receive buffer from server : {}", buffer);
            let answer_type = get_answer_type(&buffer);
            match answer_type
            {
                AnswerType::Spontaneous => {
                    process_spontaneous_paquet(&buffer, &mut player, &dimensions);
                },
                AnswerType::FromCommand => {
                    let ret = process_command_packet( &mut player,
                                                                    &buffer,
                                                                    &dimensions,
                                                                    args[1].clone(),
                                                                    args[2].clone().parse::<u16>().expect("fail to parse port")
                    );
                    // for elevation, command is not 'finish', so we need to wait until 'niveau actuel : X' packet
                    match ret 
                    {
                        ServerAnswer::Elevation => (),
                        ServerAnswer::Ko => {
                            println!("Command result is KO, reinitialize player cmds and memory");
                            player.commands.clear();
                            player.wait_answer = false;
                        },
                        _ => {
                            player.current_cmd = None;
                            player.wait_answer = false;
                            if player.commands.len() > 0
                            {
                                player.commands.remove(0);
                            }
                        }
                    }
                }
            }
            buffer.clear();
        }
    }
    println!("Terminated.");
}


fn get_answer_type(buffer: &String) -> AnswerType
{
    // we consider a paquet can't be corrupted
    let first_word = first_word(buffer);
    //println!("I'm the first word of the buffer : {}", first_word);
    match first_word {
        "deplacement" | "message" | "mort" | "elevation" | "BIENVENUE" => { AnswerType::Spontaneous },
        _ => { AnswerType::FromCommand }
    }
}

