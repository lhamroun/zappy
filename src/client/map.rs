pub mod map {

/**********************************************************************
 * Struct Point
***********************************************************************/
    #[derive(Debug, Clone)]
    pub struct Point
    {
        pub x: u8,
        pub y: u8,
    }

    impl Point
    {
        pub fn new(x: u8, y: u8) -> Self
        {
            Point {x, y}
        }
    }

// redite de Point
/**********************************************************************
 * Struct Dimensions
***********************************************************************/
    pub struct Dimensions
    {
        pub width: u8,
        pub height: u8,
    }

    impl Dimensions
    {
        pub fn new(width: u8, height: u8) -> Self
        {
            Dimensions {width, height}
        }
}

/**********************************************************************
 * Struct Entity
***********************************************************************/
    #[derive(Debug, Clone, Copy, PartialEq)]
    pub struct Entity
    {
        name: &'static str,
        pub count: u8,
        timestamp: u64,
    }

    impl Entity
    {
        pub fn new(name: &'static str, count: u8, timestamp: u64) -> Self
        {
            Entity
            {
                name,
                count,
                timestamp,
            }
        }

        pub fn update(&mut self, timestamp: u64)
        {
            self.count += 1;
            self.timestamp = timestamp;
        }
    }

/**********************************************************************
 * Struct Cell
***********************************************************************/
    #[derive(Debug, Clone)]
    pub struct Cell
    {
        pub player: Option<Entity>,
        pub food: Option<Entity>,
        pub sibur: Option<Entity>,
        pub mendiane: Option<Entity>,
        pub linemate: Option<Entity>,
        pub deraumere: Option<Entity>,
        pub phiras: Option<Entity>,
        pub thystame: Option<Entity>, 
    }

    pub fn init_map(x: u8, y: u8) -> Vec<Vec<Cell>> {
        let map: Vec<Vec<Cell>> = (0..x).map(|_| {
            (0..y).map(|_| {
                Cell {
                    player: None,
                    food: None,
                    sibur: None,
                    mendiane: None,
                    linemate: None,
                    deraumere: None,
                    phiras: None,
                    thystame: None,
                }
            }).collect()
        }).collect();
        map
    }


}