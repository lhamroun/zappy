pub mod player
{
    use crate::algo::algo::Strategy;
    use crate::map::map::{init_map, Cell, Dimensions, Entity, Point};
    use crate::command::command::Command;


    pub const FOOD_CRITICAL: u8 = 10;
    pub const FOOD_LOW: u8      = 40;
    pub const NB_FORK: u8       = 5;

    #[derive(Debug, Clone)]
    pub enum Orientation
    {
        N,
        E,
        S,
        O
    }

    #[derive(Debug, Clone)]
    pub struct Resources
    {
        pub food: u8,
        pub sibur: u8,
        pub mendiane: u8,
        pub linemate: u8,
        pub deraumere: u8,
        pub phiras: u8,
        pub thystame: u8,
    }

    impl Resources
    {
        pub fn new() -> Self
        {
            Resources
            {
                food: 0,
                sibur: 0,
                mendiane: 0,
                linemate: 0,
                deraumere: 0,
                phiras: 0,
                thystame: 0
            }
        }
    }

    #[derive(Debug, Clone)]
    pub struct Player
    {
        pub level           : u8,
        pub map             : Vec<Vec<Cell>>,
        pub coord           : Point,
        pub orientation     : Orientation,
        pub commands        : Vec<Command>,
        pub current_cmd     : Option<Command>,
        pub inventaire      : Resources,
        pub dead            : bool,
        pub wait_answer     : bool,
        pub strategy        : Strategy,
        timestamp           : u64,
        pub tmp             : i8,
        pub nb_player_join  : u8,
    }

    impl Player
    {
        pub fn new(dimensions: &Dimensions) -> Self 
        {
            Player
            {
                level: 1,
                map: init_map(dimensions.width, dimensions.height),
                coord: Point::new(dimensions.width / 2,dimensions.height / 2),
                orientation: Orientation::N,
                commands: Vec::new(),
                current_cmd: None,
                timestamp: 0,
                inventaire: Resources::new(),
                dead: false,
                wait_answer : false,
                strategy : Strategy::NoStrategy,
                tmp : 0,
                nb_player_join: 0,
            }
        }

        pub fn avance(&mut self, dimensions: &Dimensions)
        {
            match self.orientation {
                Orientation::N => {
                    if self.coord.y == 0 { self.coord.y = dimensions.height - 1; }
                    else { self.coord.y -= 1; }
                },
                Orientation::E => {
                    if self.coord.x == dimensions.width - 1 { self.coord.x = 0; }
                    else { self.coord.x += 1; }
                },
                Orientation::S => {
                    if self.coord.y == dimensions.height - 1 { self.coord.y = 0; }
                    else { self.coord.y += 1; }
                },
                Orientation::O => {
                    if self.coord.x == 0 { self.coord.x = dimensions.width - 1; }
                    else { self.coord.x -= 1; }
                }
            }
        }

        pub fn droite(&mut self) -> bool
        {
            match self.orientation
            {
                Orientation::N => self.orientation = Orientation::E,
                Orientation::E => self.orientation = Orientation::S,
                Orientation::S => self.orientation = Orientation::O,
                Orientation::O => self.orientation = Orientation::N,
            }
            true
        }

        pub fn gauche(&mut self) -> bool
        {
            match self.orientation
            {
                Orientation::N => self.orientation = Orientation::O,
                Orientation::E => self.orientation = Orientation::N,
                Orientation::S => self.orientation = Orientation::E,
                Orientation::O => self.orientation = Orientation::S,
            }
            true
        }

        pub fn voir(&mut self, dimensions: &Dimensions, cells_content: Vec<Vec<&str>>)
        {
            let cells_coord = get_cells_coord_from_player_pov(self, dimensions.width, dimensions.height);
            fill_map(self, cells_content, &cells_coord);
        }

        pub fn inventaire(&mut self, resource: Resources) 
        {
            self.inventaire = resource;
        }

        pub fn fork(&mut self, teamname: String, port: u16)
        {
            match std::env::current_exe()
            {
                Ok(path) => {
                    println!("run child process");
                    let child = std::process::Command::new(path)
                        .arg(teamname)
                        .arg(port.to_string())
                        .arg("--child") // use to identify child process
                        .arg(format!("--width={}", self.map[0].len()))
                        .arg(format!("--height={}", self.map.len()))
                        //.arg("test/commands/move.txt") // use with dbg features
                        .spawn();
                    if let Ok(_) = child {
                        println!("fail to create child process");
                    }

                }
                Err(e) => {
                    println!("fail to get binary file : {}", e);
                }
            }
        }

    }


    fn get_cells_coord_from_player_pov(player: &Player, width: u8, height: u8) -> Vec<Point>
    {
        let mut cells_coord : Vec<Point> = Vec::new();

        cells_coord.push(Point{x: player.coord.x, y: player.coord.y});
        for i in 1..=player.level as i8
        {
            for j in 0..(i * 2) + 1 as i8
            {
                let mut x = match player.orientation
                {
                    Orientation::N => player.coord.x as i8 + (-1) * (i * 2 + 1) / 2 + j,
                    Orientation::S => player.coord.x as i8 +        (i * 2 + 1) / 2 - j,
                    Orientation::O => player.coord.x as i8 - 1 * i,
                    Orientation::E => player.coord.x as i8 + i,
                };
                // println!("x -----> {}", x);
                if x < 0 { x = width as i8 + x; }
                else if x > width as i8 - 1 { x = x % width as i8; }
                let mut y = match player.orientation
                {
                    Orientation::E => player.coord.y as i8 + (-1) * (i * 2 + 1) / 2 + j,
                    Orientation::O => player.coord.y as i8 +        (i * 2 + 1) / 2 - j,
                    Orientation::N => player.coord.y as i8 - 1 * i,
                    Orientation::S => player.coord.y as i8 + i,
                };
                // println!("y -----> {}", y);
                if y < 0 { y = height as i8 + y; }
                else if y > height as i8 - 1 { y = y % height as i8; }
                cells_coord.push(Point{x: x as u8, y: y as u8});
            }
        }
        ////println!("coord des cases a voir ---> {:?}", cells_coord);
        cells_coord
    }

    fn reset_cell(player: &mut Player, coord: Point) {
        player.map[coord.y as usize][coord.x as usize].food = None;
        player.map[coord.y as usize][coord.x as usize].player = None;
        player.map[coord.y as usize][coord.x as usize].linemate = None;
        player.map[coord.y as usize][coord.x as usize].phiras = None;
        player.map[coord.y as usize][coord.x as usize].mendiane = None;
        player.map[coord.y as usize][coord.x as usize].sibur = None;
        player.map[coord.y as usize][coord.x as usize].thystame = None;
        player.map[coord.y as usize][coord.x as usize].deraumere = None;
    }

    fn fill_map(player: &mut Player, cells_content: Vec<Vec<&str>>, cells_coord: &Vec<Point>)
    {
        for inner_vec in &cells_content {
            for element in inner_vec {
            }
        }
        for (index, cell_content) in cells_content.iter().enumerate() {
            let coord = cells_coord.get(index).expect("fill_map trying to go too far. Probably a level concordance problem");

            reset_cell(player, coord.clone());

            for entity in cell_content {
                match *entity {
                    "player" => {
                        //println!("@player[{}][{}]", coord.x, coord.y);
                        match player.map[coord.y as usize][coord.x as usize].player.as_mut() {
                            Some(entity) => entity.update(player.timestamp),
                            None => player.map[coord.y as usize][coord.x as usize].player = Some(Entity::new("player", 1, player.timestamp)),
                        }
                    },
                    "food" => {
                        //println!("@food[{}][{}]", coord.x, coord.y);
                        match player.map[coord.y as usize][coord.x as usize].food.as_mut() {
                            Some(entity) => entity.update(player.timestamp),
                            None => player.map[coord.y as usize][coord.x as usize].food = Some(Entity::new("food", 1, player.timestamp)),
                        }
                    },
                    "sibur" => {
                        //println!("@sibur[{}][{}]", coord.x, coord.y);
                        match player.map[coord.y as usize][coord.x as usize].sibur.as_mut() {
                            Some(entity) => entity.update(player.timestamp),
                            None => player.map[coord.y as usize][coord.x as usize].sibur = Some(Entity::new("sibur", 1, player.timestamp)),
                        }
                    },
                    "mendiane" => {
                        //println!("@mendiane[{}][{}]", coord.x, coord.y);
                        match player.map[coord.y as usize][coord.x as usize].mendiane.as_mut() {
                            Some(entity) => entity.update(player.timestamp),
                            None => player.map[coord.y as usize][coord.x as usize].mendiane = Some(Entity::new("mendiane", 1, player.timestamp)),
                        }
                    },
                    "linemate" => {
                        //println!("@linemate[{}][{}]", coord.x, coord.y);
                        match player.map[coord.y as usize][coord.x as usize].linemate.as_mut() {
                            Some(entity) => entity.update(player.timestamp),
                            None => player.map[coord.y as usize][coord.x as usize].linemate = Some(Entity::new("linemate", 1, player.timestamp)),
                        }
                    },
                    "deraumere" => {
                        //println!("@deraumere[{}][{}]", coord.x, coord.y);
                        match player.map[coord.y as usize][coord.x as usize].deraumere.as_mut() {
                            Some(entity) => entity.update(player.timestamp),
                            None => player.map[coord.y as usize][coord.x as usize].deraumere = Some(Entity::new("deraumere", 1, player.timestamp)),
                        }
                    },
                    "phiras" => {
                        //println!("@phiras[{}][{}]", coord.x, coord.y);
                        match player.map[coord.y as usize][coord.x as usize].phiras.as_mut() {
                            Some(entity) => entity.update(player.timestamp),
                            None => player.map[coord.y as usize][coord.x as usize].phiras = Some(Entity::new("phiras", 1, player.timestamp)),
                        }
                    },
                    "thystame" => {
                        //println!("@thystame[{}][{}]", coord.x, coord.y);
                        match player.map[coord.y as usize][coord.x as usize].thystame.as_mut() {
                            Some(entity) => entity.update(player.timestamp),
                            None => player.map[coord.y as usize][coord.x as usize].thystame = Some(Entity::new("thystame", 1, player.timestamp)),
                        }
                    },
                    _ => println!("\ncase vide\n"), // CHANGER: maintenant c'est un probleme
                }
            }
        }
    }

}