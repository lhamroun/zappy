pub mod process
{
    use crate::algo::algo::Strategy;
    use crate::map::map::Dimensions;
    use crate::command::command::exec_cmd;
    use crate::player::player::Player;
    use crate::stream::stream::ServerAnswer;
    use crate::utils::utils::{first_word, get_word_by_index, move_after_someone_expulse_me, check_voir_answer, check_inventaire_answer};


    pub fn process_spontaneous_paquet(buffer: &String, player: &mut Player, dimensions: &Dimensions)
    {    
        let cmd_type = first_word(buffer);
        println!("stratosphere {}", cmd_type);
        match cmd_type
        {
            "mort" => {
                println!("Ooohhw fvck, I'm dead");
                player.dead = true;
                std::process::exit(0);
            },
            "elevation" => {
            },
            "deplacement" => {
                let expulse_direction = get_word_by_index(buffer, 1);
                if let Some(mut direction) = expulse_direction
                {
                    let val = direction.parse::<u8>().unwrap_or(0);
                    let tmp_coord = player.coord.clone();
                    move_after_someone_expulse_me(player, dimensions, val);
                    println!(   "expulse direction = {}\nfrom ({}, {}) to ({}, {})",
                                val,
                                tmp_coord.x,
                                tmp_coord.y,
                                player.coord.x,
                                player.coord.y
                    );
                }
            },
            "message" => {
                if buffer.contains("join-me") && player.tmp != -1 && player.wait_answer == false {
                    if player.strategy != Strategy::BroadcastJoinMaster {
                        player.strategy = Strategy::BroadcastJoinMaster;

                        let parts: Vec<&str> = buffer.split_whitespace().collect();
                        let num_str = parts[1].trim_end_matches(',');
                        player.tmp = num_str.parse().expect("Not a valid number");
                    }

                }
                if buffer.contains("imhere") {
                    player.nb_player_join += 1;
                }
            },
            _ => {
                println!("Error: your spontaneous packet doesn't match the spontaneous patterns : {}", buffer);
            }
        }
    }

    pub fn process_command_packet(player: &mut Player, received_data: &str, dimensions: &Dimensions, teamname: String, port: u16) -> ServerAnswer
    {
        match first_word(received_data) {
            "ok" => {
                exec_cmd(player, dimensions, teamname, port);
                return ServerAnswer::Ok;
            },
            "ko" => {
                if let Some(cmd) = &player.current_cmd {
                    player.map[player.coord.y as usize][player.coord.x as usize].deraumere = None;
                    player.map[player.coord.y as usize][player.coord.x as usize].linemate = None;
                    player.map[player.coord.y as usize][player.coord.x as usize].sibur = None;
                    player.map[player.coord.y as usize][player.coord.x as usize].phiras = None;
                    player.map[player.coord.y as usize][player.coord.x as usize].mendiane = None;
                    player.map[player.coord.y as usize][player.coord.x as usize].thystame = None;
                    player.map[player.coord.y as usize][player.coord.x as usize].food = None;
                    player.map[player.coord.y as usize][player.coord.x as usize].player = None;
                }
                return ServerAnswer::Ko;
            },
            "elevation" => { return ServerAnswer::Elevation; },
            "niveau" => {
                // increase player level only if I receive 'niveau actuel : level+1'
                player.level = player.level + (player.level + 1 == get_word_by_index(received_data, 3).unwrap_or(&player.level.to_string()).parse::<u8>().unwrap_or(0)) as u8;
                //println!("player new level is {}", player.level);
                return ServerAnswer::Incantation(player.level);
            },
            answer => {
                if let Some(cmd) = &player.current_cmd
                {
                    match cmd.name {
                        "voir" => {
                            // check if voir answer have a valid format
                            if let Some(cells_content) = check_voir_answer(received_data) {
                                player.voir(dimensions, cells_content);
                                return ServerAnswer::Voir(received_data.to_string());
                            }
                            println!("error: receiving a bad voir answer");
                            return ServerAnswer::Unknown;

                        },
                        "inventaire" => {
                            // check if inventaire answer have a valid format
                            if let Some(ressource) = check_inventaire_answer(received_data) {
                                player.inventaire(ressource);
                                return ServerAnswer::Inventaire(received_data.to_string());
                            }
                            println!("error: receiving a bad inventaire answer");
                            return ServerAnswer::Unknown
                        },
                        _ => { println!("error: receiving bad msg from server {}", answer) }
                    }
                }
            },
        }
        ServerAnswer::Unknown
        }

}