pub mod stream {
    use std::{
        io::{self, BufReader, LineWriter},
        net::TcpStream,
    };

    #[derive(Debug)]
    pub enum AnswerType {
        Spontaneous, // 'deplacement X', 'mort', 'message X', 
        FromCommand, // 'Ok', 'Ko', 'Niveau Actuel : X', 'Elevation en cours', '{voir}', '{inventaire}, {connect_nbr}'
    }

    #[derive(Debug)]
    pub enum ServerAnswer {
        Unknown,
        Ok,
        Ko,
        Voir(String),
        Inventaire(String),
        Elevation, // Elevation en cours...
        Incantation(u8), // incantation level 
        ConnectNbr(u8),
        Mort, 
    }
    
    #[derive(Debug)]
    pub struct BufIo {
        pub input: BufReader<TcpStream>,
        pub output: LineWriter<TcpStream>,
    }
    
    impl BufIo {
        pub fn new(stream: &TcpStream) -> io::Result<Self> {
            let input = BufReader::new(stream.try_clone()?);
            let output = LineWriter::new(stream.try_clone()?);
    
            Ok(Self { input, output })
        }
    }
    
}