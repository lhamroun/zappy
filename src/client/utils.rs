pub mod utils
{
    use crate::map::map::Dimensions;
    use crate::player::player::{Orientation, Player, Resources};
    use crate::command::command::{Command, AVANCE, BROADCAST, CONNECT_NBR, DROITE, EXPULSE, FORK, GAUCHE, INCANTATION, INVENTAIRE, POSE, PREND, VOIR};

    pub fn first_word(s: &str) -> &str
    {
        s.split_whitespace().next().unwrap_or("")
    }

    pub fn get_word_by_index(phrase: &str, index: usize) -> Option<&str>
    {
        phrase.split_whitespace().nth(index)
    }

    /***********************************************************************************
     * Simple implementation of cpy_from_slice use for translate the buffer receive 
     * in the stream to the teamname
     * 
     * params:
     *      buffer: [u8; 32]
     * 
     * return:
     *       String
    *********************************************************************************/
    pub fn copy_until_char(buffer: &[u8], char: u8) -> String
    {
        let string_dst = buffer
            .iter() // into_iter 
            .take_while(|&x| *x != char)
            .map(|x| *x as char)
            .collect();
        string_dst
    }

    /*
    **  Check if the player inventaire contains enough ressources for level up.
    **  return :
    **      true if enough ressources for level up
    */
    pub fn requirement_for_level(_inventaire: &Resources, _level: u8) -> bool
    {
        false
    }

    pub fn add_ressource_into_inventaire(player: &mut Player, ressource: String)
    {
        match ressource.as_str()
        {
            "food" => player.inventaire.food += 1,
            "phiras" => player.inventaire.phiras += 1,
            "thystame" => player.inventaire.thystame += 1,
            "mendiane" => player.inventaire.mendiane += 1,
            "deraumere" => player.inventaire.deraumere += 1,
            "linemate" => player.inventaire.linemate += 1,
            "sibur" => player.inventaire.sibur += 1,
            _ => { return ; }
        }
        return ;
    }

    pub fn remove_ressource_into_inventaire(player: &mut Player, ressource: String)
    {
        match ressource.as_str()
        {
            "food" => player.inventaire.food -= 1,
            "phiras" => player.inventaire.phiras -= 1,
            "thystame" => player.inventaire.thystame -= 1,
            "mendiane" => player.inventaire.mendiane -= 1,
            "deraumere" => player.inventaire.deraumere -= 1,
            "linemate" => player.inventaire.linemate -= 1,
            "sibur" => player.inventaire.sibur -= 1,
            _ => { println!("inventaire don't contains {}", ressource); }
        }
    }

    pub fn get_ressource_from_cmd(cmd: Command) -> String
    {
        if let Some(ressource) = cmd.arg
        {
            return ressource;
        }
        return String::from(""); // should be other thing
    }

    /*
    **  Move `player` after someone expulse he
    */
    pub fn move_after_someone_expulse_me(player: &mut Player, dimensions: &Dimensions, direction: u8)
    {
        match direction
        {
            1 => {
                match player.orientation
                {
                    Orientation::N => {
                        if player.coord.y == dimensions.height - 1 {
                            player.coord.y = 0;
                        }
                        else {
                            player.coord.y += 1;
                        }
                    },
                    Orientation::S => {
                        if player.coord.y == 0 {
                            player.coord.y = dimensions.height - 1;
                        }
                        else {
                            player.coord.y -= 1;
                        }
                    },
                    Orientation::E => {
                        if player.coord.x == 0 {
                            player.coord.x = dimensions.width - 1;
                        }
                        else {
                            player.coord.x -= 1;
                        }
                    },
                    Orientation::O => {
                        if player.coord.x == dimensions.width - 1 {
                            player.coord.x = 0;
                        }
                        else {
                            player.coord.x += 1;
                        }
                    },
                }
            },
            3 => {
                match player.orientation
                {
                    Orientation::N => {
                        if player.coord.x == dimensions.width - 1 {
                            player.coord.x = 0;
                        }
                        else {
                            player.coord.x += 1;
                        }
                    },
                    Orientation::S => {
                        if player.coord.x == 0 {
                            player.coord.x = dimensions.width - 1;
                        }
                        else {
                            player.coord.x -= 1;
                        }
                    },
                    Orientation::E => {
                        if player.coord.y == dimensions.height - 1 {
                            player.coord.y = 0;
                        }
                        else {
                            player.coord.y += 1;
                        }
                    },
                    Orientation::O => {
                        if player.coord.y == 0 {
                            player.coord.y = dimensions.height - 1;
                        }
                        else {
                            player.coord.y -= 1;
                        }
                    },
                }
            },
            5 => {
                match player.orientation
                {
                    Orientation::N => {
                        if player.coord.y == 0 {
                            player.coord.y = dimensions.height - 1;
                        }
                        else {
                            player.coord.y -= 1;
                        }
                    },
                    Orientation::S => {
                        if player.coord.y == dimensions.height - 1 {
                            player.coord.y = 0;
                        }
                        else {
                            player.coord.y += 1;
                        }
                    },
                    Orientation::E => {
                        if player.coord.x == dimensions.width - 1 {
                            player.coord.x = 0;
                        }
                        else {
                            player.coord.x += 1;
                        }
                    },
                    Orientation::O => {
                        if player.coord.x == 0 {
                            player.coord.x = dimensions.width - 1;
                        }
                        else {
                            player.coord.x -= 1;
                        }
                    },
                }
            },
            7 => {
                match player.orientation
                {
                    Orientation::N => {
                        if player.coord.x == 0 {
                            player.coord.x = dimensions.width - 1;
                        }
                        else {
                            player.coord.x -= 1;
                        }
                    },
                    Orientation::S => {
                        if player.coord.x == dimensions.width - 1 {
                            player.coord.x = 0;
                        }
                        else {
                            player.coord.x += 1;
                        }
                    },
                    Orientation::E => {
                        if player.coord.y == 0 {
                            player.coord.y = dimensions.height - 1;
                        }
                        else {
                            player.coord.y -= 1;
                        }
                    },
                    Orientation::O => {
                        if player.coord.y == dimensions.height - 1 {
                            player.coord.y = 0;
                        }
                        else {
                            player.coord.y += 1;
                        }
                    },
                }
            },
            _ => { println!("error during expulse --> player not move"); },
        }
    }

    /*
    **  Move the players registered in the `player` map
    **  `player` store ressources and other detected players into his own map
    **  This function will move other players from the `player` map
    */
    pub fn move_players_from_expulse(player: &mut Player, dimensions: &Dimensions)
    {
        let mut nb_players = 0;

        if let Some(mut tmp) = &player.map[player.coord.y as usize][player.coord.x as usize].player
        {
            nb_players = tmp.count;
            tmp.count = 0;
            // move players
            match player.orientation {
                Orientation::N => {
                    if player.coord.y == 0 {
                        player.map[(dimensions.height - 1) as usize][player.coord.x as usize].player.unwrap().count += nb_players;
                    }
                    else {
                        player.map[(player.coord.y - 1) as usize][player.coord.x as usize].player.unwrap().count += nb_players;
                    }
                },
                Orientation::E => {
                    if player.coord.x == dimensions.width - 1 {
                        player.map[(dimensions.height - 1) as usize][0].player.unwrap().count += nb_players;
                    }
                    else {
                        player.map[(dimensions.height - 1) as usize][(player.coord.x + 1) as usize].player.unwrap().count += nb_players;
                    }
                },
                Orientation::S => {
                    if player.coord.y == dimensions.height - 1 {
                        player.map[0][player.coord.x as usize].player.unwrap().count += nb_players;
                    }
                    else {
                        player.map[(player.coord.y + 1) as usize][player.coord.x as usize].player.unwrap().count += nb_players;
                    }
                },
                Orientation::O => {
                    if player.coord.x == 0 {
                        player.coord.x = dimensions.width - 1;
                        player.map[player.coord.y as usize][(dimensions.width - 1) as usize].player.unwrap().count += nb_players;
                    }
                    else {
                        player.map[player.coord.y as usize][(player.coord.x - 1) as usize].player.unwrap().count += nb_players;
                    }
                }
            }
        }
    }

    /*
    **  convert String into Command obj when string is valid
    */
    pub fn string_to_cmd(buf: &String) -> Option<Command>
    {
        let first = first_word(buf);
        let param: String;
        let ret = match first {
            // command without arg
            "avance" => Some(Command::new(AVANCE, None)),
            "droite" => Some(Command::new(DROITE, None)),
            "gauche" => Some(Command::new(GAUCHE, None)),
            "voir" => Some(Command::new(VOIR, None)),
            "inventaire" => Some(Command::new(INVENTAIRE, None)),
            "expulse" => Some(Command::new(EXPULSE, None)),
            "incantation" => Some(Command::new(INCANTATION, None)),
            "fork" => Some(Command::new(FORK, None)),
            "connect_nbr" => Some(Command::new(CONNECT_NBR, None)),
            // command with arg
            buf_tmp => {
                let tmp_ret: Option<Command>;
                let buf_prefix = buf_tmp.to_string();
                if buf_prefix.starts_with("prend") {
                    param = buf.replacen("prend ", "", 1);
                    let param = param[..param.len() - 1].to_string(); // remove \n
                    // verify here, that obj is valid else -> None
                    println!("'prend' param is : {}", param);
                    tmp_ret = Some(Command::new(PREND, Some(param.clone())));
                }
                else if buf_prefix.starts_with("pose") {
                    param = buf.replacen("pose ", "", 1);
                    let param = param[..param.len() - 1].to_string(); // remove \n
                    // verify here, that obj is valid else -> None
                    println!("'pose' param is : {}", param);
                    tmp_ret = Some(Command::new(POSE, Some(param.clone())));
                }
                else if buf_prefix.starts_with("broadcast") {
                    param = buf.replacen("broadcast ", "", 1);
                    let param = param[..param.len() - 1].to_string(); // remove \n
                    // verify here, that msg is present else -> None
                    println!("'broadcast' msg is : {}", param);
                    tmp_ret = Some(Command::new(BROADCAST, Some(param.clone())));
                }
                else {
                    tmp_ret = None;
                }
                tmp_ret
            },
        };
        ret
    }


    pub fn check_voir_answer(obj_str: &str) -> Option<Vec<Vec<&str>>>
    {
        let mut cells_content: Vec<Vec<&str>> = obj_str.split(',').map(|s| s.split(' ').collect()).collect();

        cells_content.iter_mut().for_each(|content| content.retain(|str| *str != ""));

        for cell_content in &cells_content {
            for entity in cell_content {
                match *entity {
                    "player"
                    | "food"
                    | "linemate"
                    | "sibur"
                    | "phiras"
                    | "thystame"
                    | "mendiane"
                    | "deraumere" => (),
                    "" => println!("PROBLEM"),
                    _ => return None,
                }
            }
        }
        Some(cells_content)
    }


    pub fn check_inventaire_answer(obj_str: &str) -> Option<Resources>
    {
        let mut inventaire = Resources::new();

        for resource in obj_str.split(", ")
        {
            let mut parts = resource.split_whitespace();
            let name = parts.next().unwrap();
            let quantity: u8 = parts.next().unwrap().parse().unwrap();

            match name
            {
                "food" => inventaire.food = quantity,
                "sibur" => inventaire.sibur = quantity,
                "mendiane" => inventaire.mendiane = quantity,
                "linemate" => inventaire.linemate = quantity,
                "deraumere" => inventaire.deraumere = quantity,
                "phiras" => inventaire.phiras = quantity,
                "thystame" => inventaire.thystame = quantity,
                _ => {
                    println!("Unknown resource: {}", name);
                    return None;
                },
            }
        }
        Some(inventaire)
    }



}