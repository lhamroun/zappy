pub mod action {
    use crate::{
        env::env::{Egg, Env},
        map::map::Cell,
        player_entities::player_entities::{Action, EggId, Player, PlayerId},
    };
    use bevy::{
        animation::AnimationPlayer,
        app::{App, Plugin, Update},
        ecs::{
            component::Component,
            entity::Entity,
            system::{Command, Query, Res, ResMut},
        },
        hierarchy::Parent,
        math::Vec3,
        prelude::{Commands, Gizmos, ParamSet, With, Without},
        render::color::Color,
        time::Time,
        transform::components::Transform,
    };
    use std::{collections::VecDeque, fmt::Debug, time::{Duration, Instant}};

    use crate::TILES_WIDTH;
    pub const SIZE_MAX_TEAM: usize = 10;
    pub const VEC_COLORS: [Color; SIZE_MAX_TEAM] = [
        Color::RED,
        Color::GREEN,
        Color::YELLOW,
        Color::PINK,
        Color::BLUE,
        Color::PURPLE,
        Color::BLACK,
        Color::MAROON,
        Color::SALMON,
        Color::SILVER,
    ];

    impl Plugin for Action {
        fn build(&self, app: &mut App) {
            app.add_systems(Update, set_exec_action)
                // .add_systems(Update, draw_broadcast)
                .add_systems(Update, exec_action)
                .add_systems(Update, change_animation_depending_of_typeaction);
        }
    }

    #[derive(Component, Debug)]
    pub enum TypeofMovement {
        Translation,
        Rotation,
        Nothing,
    }

    #[derive(Component, Debug)]
    pub struct Movementinprogress {
        pub distance_restante: f32,
        orientation: u8,
        type_of_mvmt: TypeofMovement,
    }

    impl Movementinprogress {
        pub fn new() -> Self {
            Movementinprogress {
                distance_restante: 0.,
                orientation: 0,
                type_of_mvmt: TypeofMovement::Nothing,
            }
        }
        pub fn set_distance(&mut self, dist: f32, o: u8) {
            self.distance_restante = dist;
            self.orientation = o;
        }
    }

    #[derive(Clone)]
    pub enum TypeAction {
        Movement(u8, u8, u8),
        Expulse,
        BeingExpulse(u8, u8, u8),
        Fork(u8),    // besoin d'u8 ?
        EndFork(u8), // besoin d'u8 ?
        PrendPose,
        Broadcast(u8, Entity, Vec<Entity>), // id src, id dst
        Inc,
        EndInc,
        Nothing,
    }

    impl PartialEq for TypeAction {
        fn eq(&self, other: &Self) -> bool {
            match (self, other) {
                (Self::Movement(_, _, _), Self::Movement(_, _, _)) => true,
                (Self::BeingExpulse(_, _, _), Self::BeingExpulse(_, _, _)) => true,
                (Self::Fork(_), Self::Fork(_)) => true,
                _ => core::mem::discriminant(self) == core::mem::discriminant(other),
            }
        }
    }
    impl Debug for TypeAction {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
            match self {
                Self::Movement(arg0, arg1, arg2) => f
                    .debug_tuple("Movement")
                    .field(arg0)
                    .field(arg1)
                    .field(arg2)
                    .finish(),
                Self::Expulse => write!(f, "Expulse"),
                Self::BeingExpulse(arg0, arg1, arg2) => f
                    .debug_tuple("BeingExpulse")
                    .field(arg0)
                    .field(arg1)
                    .field(arg2)
                    .finish(),
                Self::Fork(i) => f.debug_tuple("Fork").field(i).finish(),
                Self::EndFork(arg0) => f.debug_tuple("EndFork").field(arg0).finish(),
                Self::PrendPose => write!(f, "PrendPose"),
                Self::Inc => write!(f, "Inc"),
                Self::EndInc => write!(f, "EndInc"),
                Self::Broadcast(_, _, _) => write!(f, "Broadcast"),
                Self::Nothing => write!(f, "Nothing"),
                // Self::Broadcast(_, vec) => todo!(),
            }
        }
    }
    #[derive(Debug, PartialEq)]
    pub enum StateAction {
        InAction,
        Idle,
    }

    #[derive(Component, Debug)]
    pub struct ActionPlayer {
        pub vecdeque: VecDeque<TypeAction>, // Fifo [Mov(), Fork(), Expulse()]
        pub state_action: StateAction,      // InAction or Idle
        pub action_in_progress: TypeAction, // Movemement Expulse PrendPose Inc .. Nothing
        pub instant: Option<Instant>,
        pub duration: Option<Duration>,
    }

    impl ActionPlayer {
        pub fn new() -> Self {
            ActionPlayer {
                vecdeque: VecDeque::new(),               // [Brodcast]
                state_action: StateAction::Idle,         // in_progress
                action_in_progress: TypeAction::Nothing, // Nothing -> Broadcast
                instant: None,
                duration: None,
            }
        }
    }

    pub fn out_of_bound(transform: &mut Transform, asset_map: &ResMut<Env>) {
        if transform.translation.z > asset_map.pixel_y_max {
            transform.translation.z = asset_map.pixel_y_min
        }
        if transform.translation.x > asset_map.pixel_x_max {
            transform.translation.x = asset_map.pixel_x_min
        }
        if transform.translation.x < asset_map.pixel_x_min {
            transform.translation.x = asset_map.pixel_x_max
        }
        if transform.translation.z < asset_map.pixel_y_min {
            transform.translation.z = asset_map.pixel_y_max
        }
    }

    pub fn update_cell(movement: &Movementinprogress, cell: &mut Cell, env: &ResMut<Env>) {
        match movement.orientation {
            1 => {
                if cell.1 == 0 {
                    cell.1 = (env.y - 1) as u8
                } else {
                    cell.1 -= 1;
                }
            } // Nord
            2 => {
                cell.0 += 1;
                if cell.0 == env.x as u8 {
                    cell.0 = 0;
                }
            } // Est
            3 => {
                cell.1 += 1;
                if cell.1 == env.y as u8 {
                    cell.1 = 0;
                }
            } // Sud
            4 => {
                if cell.0 == 0 {
                    cell.0 = (env.x - 1) as u8
                } else {
                    cell.0 -= 1;
                }
            } // Ouest
            _ => {
                panic!()
            }
        }
    }

    pub fn debug_action(query_debug: Query<(&mut ActionPlayer, &PlayerId, &Cell)>) {
        //*
        for (mut action_player, playerid, cell) in query_debug.iter() {
            println!(
                "playerid : {:?} action player {:?}",
                playerid.0, action_player
            );
            //println!("cell {:?}", cell);
        }
        //*/
    }
    /*
        Each Entity has been given an id at its creation, we're using it to iteract with them.
        Each Player has a structure ActionPlayer
    */
    pub fn add_action(
        query_action_player: &mut Query<&mut ActionPlayer>,
        id: &Entity,
        type_action: TypeAction,
    ) {
        if let Ok(mut action_player) = query_action_player.get_mut(*id) {
            action_player.instant = Some(Instant::now());
            action_player.vecdeque.push_back(type_action);
            //println!("action player {:?}", action_player);
        }
    }

    pub fn reset_action(action_player: &mut ActionPlayer)
    {
        if let Some(start) = action_player.instant{
            action_player.duration = Some(start.elapsed());

        }
        //println!("RESET_ACTION type: {:?}, duration: {:?}", action_player.action_in_progress, action_player.duration);
        action_player.state_action = StateAction::Idle;
        action_player.action_in_progress = TypeAction::Nothing;
    }

    pub fn ray_state_drawer(
        time: &Res<Time>,
        action_player: &mut ActionPlayer,
        movement: &mut Movementinprogress,
        env: &ResMut<Env>,
    ) {
        let t_prime: f32 = env.time as f32 / 7.0; // cpy
        let mut distance_delta = TILES_WIDTH * time.delta_seconds() * t_prime as f32 * 2.0; // cpy
        if distance_delta > movement.distance_restante {
            distance_delta = movement.distance_restante;
        }
        //println!("ray_state_drawer dist restante={:?}", movement.distance_restante);
        movement.distance_restante -= distance_delta;
        if movement.distance_restante == 0. {
            reset_action(action_player);
            return;
        } // cpy
    }

    pub fn player_translation(
        time: &Res<Time>,
        action_player: &mut ActionPlayer,
        movement: &mut Movementinprogress,
        transform: &mut Transform,
        cell: &mut Cell,
        env: &ResMut<Env>,
    ) {
        let t_prime: f32 = env.time as f32 / 7.0; // cpy

        let mut distance_delta = TILES_WIDTH * time.delta_seconds() * t_prime as f32 * 2.0; // cpy

        if distance_delta > movement.distance_restante {
            distance_delta = movement.distance_restante;
        } // cpy

        match movement.orientation {
            1 => transform.translation.z -= distance_delta, // Nord
            2 => transform.translation.x += distance_delta, // Est
            3 => transform.translation.z += distance_delta, // Sud
            4 => transform.translation.x -= distance_delta, // Ouest
            _ => {
                panic!("erreur orientation")
            }
        }

        out_of_bound(transform, env);

        movement.distance_restante -= distance_delta;

        if movement.distance_restante == 0. {
            reset_action(action_player);
            update_cell(movement, cell, env);
            return;
        } // cpy
    }

    pub fn player_rotation_3d(
        time: &Res<Time>,
        action_player: &mut ActionPlayer,
        movement: &mut Movementinprogress,
        transform: &mut Transform,
        cell: &mut Cell,
        asset_map: &ResMut<Env>,
    ) {
        let rotation = movement.orientation;
        let t_prime: f32 = asset_map.time as f32 / 7.0;
        let mut distance_delta = 3.14 / 2.0 * time.delta_seconds() * t_prime as f32 * 2.0; // time
        if distance_delta > movement.distance_restante {
            distance_delta = movement.distance_restante;
        }
        match (cell.2, rotation) {
            (1, 4) => transform.rotate_local_y(distance_delta),
            (4, 1) => transform.rotate_local_y(-distance_delta),
            (_, _) => match cell.2 < rotation {
                true => transform.rotate_local_y(-distance_delta),
                false => transform.rotate_local_y(distance_delta),
            },
        }
        movement.distance_restante -= distance_delta;
        if movement.distance_restante == 0. {
            reset_action(action_player);
            cell.2 = movement.orientation;
            return;
        }
    }

    pub fn get_top_parent(
        mut curr_entity: Entity,
        all_entities_with_parents_query: &Query<&Parent>,
    ) -> Entity {
        //Loop up all the way to the top parent
        loop {
            if let Ok(ref_to_parent) = all_entities_with_parents_query.get(curr_entity) {
                curr_entity = ref_to_parent.get();
            } else {
                break;
            }
        }
        curr_entity
    }
    enum Animation {
        Dance = 0,
        Death = 1,
        Hello = 2,
        HitReceive1 = 3,
        HitReceive2 = 4,
        Idle = 5,
        Jump = 6,
        JumpLanding = 7,
        JumpNoHeight = 8,
        Kick = 9,
        No = 10,
        PickUp = 11,
        Run = 12,
        ShootBig = 13,
        ShootSmall = 14,
        Walk = 15,
        Yes = 16,
    }

    // Need to add comments and probably fix
    pub fn change_animation_depending_of_typeaction(
        env: Res<Env>,
        mut animation_players: Query<(&mut AnimationPlayer, Entity)>,
        all_entities_with_parents_query: Query<&Parent>,
        mut query_action_player: Query<&mut ActionPlayer>,
    ) {
        for (mut player, entity) in &mut animation_players {
            // We need the top parent because bevy is bullshit with his filter (first guess)
            player.set_speed(env.time as f32 / 2.); // t
            let parent_entity = get_top_parent(entity, &all_entities_with_parents_query);
            if let Ok(mut action_player) = query_action_player.get_mut(parent_entity) {
                if action_player.state_action == StateAction::InAction {
                    match &action_player.action_in_progress {
                        //Run -> 12 OR Walk -> 15
                        TypeAction::Movement(_, _, _) => {
                            // player.play(asset_map.vec_animation[3].clone_weak()).repeat();
                            player
                                .play(
                                    env.vec_animation[Animation::Walk as usize].clone_weak(),
                                )
                                .repeat();
                        }
                        TypeAction::Expulse => {
                            if player
                                .play(
                                    env.vec_animation[Animation::Kick as usize].clone_weak(),
                                )
                                .is_finished()
                                == true
                            {
                                reset_action(&mut action_player);
                            }
                        }
                        TypeAction::BeingExpulse(_, _, _) => {
                            player
                                .play(
                                    env.vec_animation[Animation::Walk as usize].clone_weak(),
                                )
                                .repeat();
                        }
                        TypeAction::Fork(_) => {
                            player.play(
                                env.vec_animation[Animation::Death as usize].clone_weak(),
                            );
                        }
                        TypeAction::EndFork(_) => {
                            if player
                                .play(
                                    env.vec_animation[Animation::Jump as usize].clone_weak(),
                                )
                                .is_finished()
                                == true
                            {
                                reset_action(&mut action_player);
                            }
                        }
                        TypeAction::PrendPose => {
                            if player
                                .play(
                                    env.vec_animation[Animation::PickUp as usize]
                                        .clone_weak(),
                                )
                                .is_finished()
                                == true
                            {
                                reset_action(&mut action_player);
                            }
                        }
                        TypeAction::Inc => {
                            player
                                .play(
                                    env.vec_animation[Animation::Dance as usize].clone_weak(),
                                )
                                .repeat();
                        }
                        TypeAction::EndInc => {
                            if player
                                .play(
                                    env.vec_animation[Animation::Jump as usize].clone_weak(),
                                )
                                .is_finished()
                                == true
                            {
                                //println!("END INC ANIM");
                                reset_action(&mut action_player);
                            }
                        }
                        TypeAction::Broadcast(_, _, _) => (),
                        TypeAction::Nothing => todo!(),
                    }
                } else {
                    //Idle -> 5 OR Dance -> 0
                    player
                        .play(env.vec_animation[Animation::Idle as usize].clone_weak())
                        .repeat();
                }
            } else if let Err(err) = query_action_player.get(parent_entity) {
                println!("{:?}", err);
            }
        }
    }

    pub fn set_exec_action(
        mut query_action_player: Query<(
            &mut ActionPlayer,
            &mut Movementinprogress,
            &mut Cell,
            &Player,
        )>,
    ) {
        for (mut action_player, mut movement, cell, _) in query_action_player.iter_mut() {
            if let StateAction::Idle = action_player.state_action {
                if let Some(current_action) = action_player.vecdeque.pop_front() {
                    action_player.state_action = StateAction::InAction;
                    action_player.action_in_progress = current_action;
                    let action_player_action = action_player.action_in_progress.clone();
                    match &action_player_action {
                        TypeAction::Movement(x_finish, y_finish, o) => {
                            if (cell.0 != *x_finish || cell.1 != *y_finish) && cell.2 == *o {
                                *movement = Movementinprogress {
                                    distance_restante: TILES_WIDTH,
                                    orientation: *o,
                                    type_of_mvmt: TypeofMovement::Translation,
                                }
                            } else if cell.2 != *o {
                                *movement = Movementinprogress {
                                    distance_restante: 3.14 / 2.,
                                    orientation: *o,
                                    type_of_mvmt: TypeofMovement::Rotation,
                                }
                            } else {
                                reset_action(&mut action_player);
                                // panic!(
                                //     "\n\n-------------{:?} {:?} condition-translation :{:?} && {:?}----------\n\n",
                                //     debug_cell,
                                //     debug_typeaction,
                                //     (cell.0 != *x_finish || cell.1 != *y_finish),
                                //     cell.2 == *o
                                // )
                            }
                        }
                        TypeAction::BeingExpulse(x_finish, y_finish, o) => {
                            if cell.0 != *x_finish || cell.1 != *y_finish {
                                //ici l'orientation est celle du player qui expulse et non celui qui est expulse
                                *movement = Movementinprogress {
                                    distance_restante: TILES_WIDTH,
                                    orientation: *o,
                                    type_of_mvmt: TypeofMovement::Translation,
                                }
                            } else {
                                println!("ISSUE BEING EXPULSE ???");
                            }
                        }
                        TypeAction::Broadcast(_, _, _) => {
                            *movement = Movementinprogress {
                                distance_restante: TILES_WIDTH,
                                orientation: 0,
                                type_of_mvmt: TypeofMovement::Translation,
                            }
                        }
                        // TypeAction::Fork(_, sprite_fork) => {
                        //     change_sprite(& mut handle, & mut texture, & mut animation, sprite_fork.clone());
                        // },
                        // TypeAction::EndFork(_, sprite_mvmt) => {
                        //     change_sprite(& mut handle, & mut texture, & mut animation, sprite_mvmt.clone())
                        // },
                        _ => (),
                    }
                }
            } else if let Some(next_action) = action_player.vecdeque.back() {
                // println!("next_action {:?}", next_action);
                // println!("&action_player.action_in_progress {:?}", &action_player.action_in_progress);
                match (next_action, &action_player.action_in_progress) {
                    (TypeAction::EndFork(_), TypeAction::Fork(_)) => {
                        reset_action(&mut action_player);
                    }
                    (TypeAction::EndInc, TypeAction::Inc) => {
                        reset_action(&mut action_player);
                    }
                    (_, _) => {}
                }
            }
        }
    }

    pub fn draw_broadcast_test(
        guizmo: &mut Gizmos,
        source: &Entity,
        dest: &Vec<Entity>,
        num_team: usize,
        query_action_player: &Query<(
            &mut ActionPlayer,
            &mut Movementinprogress,
            &mut Transform,
            &mut Cell,
            &Player,
        )>,
        // also access the whole world ... why not
    ) {
        let mut vec_position_end = vec![];
        let mut vec_origin = Vec3::ZERO;

        for entity_dest in dest {
            for (_, _, transform, _, _) in query_action_player.get(*entity_dest) {
                vec_position_end.push(transform.translation + Vec3::new(0., 2., 0.));
            }
        }
        for (_, _, transform, _, _) in query_action_player.get(*source) {
            vec_origin = transform.translation + Vec3::new(0., 2., 0.);
        }

        for end_position in vec_position_end {
            //println!("{:?}", vec_origin);
            //println!("{:?}", end_position);
            guizmo.line(vec_origin, end_position, VEC_COLORS[num_team]);
        }
    }
    #[derive(Component, Debug)]
    pub struct Broadcast;

    pub fn exec_action(
        mut gizmo: Gizmos,
        time: Res<Time>,
        mut command: Commands,
        mut query_action_player: Query<(
            &mut ActionPlayer,
            &mut Movementinprogress,
            &mut Transform,
            &mut Cell,
            &Player,
        )>,
        // query_entity_idle: Query<(&Transform), Without<Broadcast>>,
        env: ResMut<Env>,
    ) {
        let mut broadcast_source = None;
        let mut broadcast_dest = None;
        let mut broadcast_team = None;
        for (mut action_player, mut movement, mut transform, mut cell, _) in
            query_action_player.iter_mut()
        {
            let typeofmvmt = &movement.type_of_mvmt;
            if let StateAction::InAction = action_player.state_action {
                let actioninprogress = action_player.action_in_progress.clone();
                match actioninprogress {
                    TypeAction::Movement(_, _, _) => match typeofmvmt {
                        TypeofMovement::Translation => {
                            player_translation(
                                &time,
                                &mut action_player,
                                &mut movement,
                                &mut transform,
                                &mut cell,
                                &env,
                            );
                        }
                        TypeofMovement::Rotation => {
                            player_rotation_3d(
                                &time,
                                &mut action_player,
                                &mut movement,
                                &mut transform,
                                &mut cell,
                                &env,
                            );
                        }
                        _ => (),
                    },
                    TypeAction::BeingExpulse(_, _, _) => {
                        if let TypeofMovement::Translation = typeofmvmt {
                            player_translation(
                                &time,
                                &mut action_player,
                                &mut movement,
                                &mut transform,
                                &mut cell,
                                &env,
                            );
                        } else {
                            action_player.state_action = StateAction::Idle;
                            action_player.action_in_progress = TypeAction::Nothing;
                        }
                    }
                    TypeAction::Expulse => (),

                    TypeAction::Inc => (),

                    TypeAction::Broadcast(num_team, source, dest) => {
                        // get the 3d vecs
                        broadcast_source = Some(source);
                        broadcast_dest = Some(dest);
                        broadcast_team = Some(num_team);
                        //println!("exec_action source={:?}", broadcast_source);
                        ray_state_drawer(&time, &mut action_player, &mut movement, &env);
                        // create fn with param mnmt
                        // copy past all the thing leftovers in player_translation
                    }

                    // TypeAction::Fork(_, _) =>{
                    //     // set fucking timer player.play(asset_map.vec_animation[9].clone_weak());
                    // },
                    // TypeAction::EndFork(_, _) => {
                    //     action_player.state_action = StateAction::Idle;
                    //     action_player.action_in_progress = TypeAction::Nothing;
                    // },
                    // TypeAction::Nothing => todo!(),
                    _ => (),
                }
            }
        }
        if let Some(source) = broadcast_source {
            if let Some(dest) = &broadcast_dest {
                if let Some(tmp_num_team) = broadcast_team {
                    draw_broadcast_test(
                        &mut gizmo,
                        &source,
                        dest,
                        tmp_num_team as usize,
                        &query_action_player,
                    );
                }
            }
        }
    }

    pub fn query_nbr_player_cell(
        query_player_cell: &mut Query<(Entity, &Cell, &Player)>,
        player_entity: Entity,
    ) -> usize {
        let mut nbr_player: usize = 0;
        let cell_ref; // impl default ?
        if let Ok((_, cell, _)) = query_player_cell.get_mut(player_entity) {
            cell_ref = Cell(cell.0, cell.1, cell.2);
            for (entity, cell, _) in query_player_cell.iter() {
                if cell_ref == *cell && entity != player_entity {
                    nbr_player += 1;
                }
            }
        }
        nbr_player
    }

    pub fn query_cell_player(
        query_player_cell: &Query<(Entity, &Cell, &Player)>,
        player_entity: Entity,
    ) -> Cell {
        if let Ok((entity, cell, _)) = query_player_cell.get(player_entity) {
            Cell(cell.0, cell.1, cell.2)
        } else {
            panic!("GET CELL HAVE SOME ISSUES HERE");
        }
    }
}
