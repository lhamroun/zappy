pub mod craftbutton {

    use bevy::{
        asset,
        diagnostic::{DiagnosticsStore, FrameTimeDiagnosticsPlugin},
        ecs::system::Command,
        input::mouse::{MouseMotion, MouseWheel},
        prelude::*,
        render::{
            camera::{self, Viewport},
            view::{visibility, RenderLayers, VisibleEntities},
        },
        sprite::Anchor,
        text::{BreakLineOn, Text2dBounds},
        ui::RelativeCursorPosition,
        window::PrimaryWindow,
        winit::WinitSettings,
    };
    use bevy_pancam::PanCam;
    use bevy_panorbit_camera::PanOrbitCamera;

    use crate::{
        action::action::get_top_parent,
        dispatch::dispatch::condition_start_game,
        env::env::{format_all_ressource_button, format_player_info, Env},
        map::map::{Coor, Position, Tile, TileBundle},
        player_entities::player_entities::{Player, PlayerId},
        TILES_WIDTH,
    };
    use crate::{map::map::Cell, player_entities::player_entities::EggId};
    pub const SIZE_FONT: f32 = 12.0;
    pub const VEC_POS_FOOD: bevy::prelude::Vec3 = Vec3::new(-3.0 * SIZE_FONT, 3.0 * SIZE_FONT, 0.);
    pub const VEC_POS_LINEMATE: bevy::prelude::Vec3 =
        Vec3::new(-3.0 * SIZE_FONT, 2.0 * SIZE_FONT, 0.);
    pub const VEC_POS_DERAUMERE: bevy::prelude::Vec3 =
        Vec3::new(-3.0 * SIZE_FONT, 1.0 * SIZE_FONT, 0.);
    pub const VEC_POS_SIBUR: bevy::prelude::Vec3 = Vec3::new(-3.0 * SIZE_FONT, 0.0 * SIZE_FONT, 0.);
    pub const VEC_POS_MENDIANE: bevy::prelude::Vec3 =
        Vec3::new(-3.0 * SIZE_FONT, -1.0 * SIZE_FONT, 0.);
    pub const VEC_POS_PHIRAS: bevy::prelude::Vec3 =
        Vec3::new(-3.0 * SIZE_FONT, -2.0 * SIZE_FONT, 0.);
    pub const VEC_POS_THYSTAME: bevy::prelude::Vec3 =
        Vec3::new(-3.0 * SIZE_FONT, -3.0 * SIZE_FONT, 0.);

    pub struct Craftbutton;

    impl Plugin for Craftbutton {
        fn build(&self, app: &mut App) {
            //*
            app.add_systems(Startup, setup_box_info_case)
                .add_systems(Startup, setup_fps)
                .add_systems(Startup, setup_box_info_player)
                // .add_systems(Startup, setup_box_info_player)
                // .add_systems(Startup,setup_box_info_egg)
                // .add_systems(Update, cursor_to_world_coordinates)
                .add_systems(Update, draw_cursor)
                .add_systems(Update, update_info_case)
                .add_systems(Update, update_info_button_player)
                .add_systems(Update, text_update_system_fps);
                // .add_systems(Update, update_info_button_player);
            // .add_systems(Update, update_info_button_egg); text_update_system_fps
            //*/
        }
    }

    /// We will store the world position of the mouse cursor here.
    #[derive(Resource, Default)]
    struct MyWorldCoords(Vec2);

    #[derive(Component)]
    struct Player_Box;
    #[derive(Component)]
    pub struct Player_Text;

    #[derive(Component)]
    pub struct Egg_Box;
    #[derive(Component)]
    struct Egg_Text;

    #[derive(Component)]
    pub struct Case_Box;

    #[derive(Component)]
    pub struct Case_Text;

    #[derive(Component)]
    struct FpsText;

    fn setup_fps(mut commands: Commands, asset_server: Res<AssetServer>) {
        // UI camera
        // Text with multiple sections
        commands
            .spawn(NodeBundle {
                style: Style {
                    width: Val::Percent(15.0),
                    height: Val::Percent(15.0),
                    position_type: PositionType::Absolute,
                    top: Val::Px(0.),
                    right: Val::Px(500.),
                    justify_content: JustifyContent::Default,
                    align_items: AlignItems::Default,
                    ..default()
                },
                visibility: Visibility::Visible,
                // background_color:
                ..default()
            })
            .with_children(|parent| {
                parent.spawn((
                    // Create a TextBundle that has a Text with a list of sections.
                    TextBundle::from_sections([
                        TextSection::new(
                            "FPS: ",
                            TextStyle {
                                // This font is loaded and will be used instead of the default font.
                                font: asset_server.load("fonts/FiraSans-Bold.ttf"),
                                font_size: 60.0,
                                ..default()
                            },
                        ),
                        TextSection::from_style(if cfg!(feature = "default_font") {
                            TextStyle {
                                font_size: 60.0,
                                color: Color::GOLD,
                                // If no font is specified, the default font (a minimal subset of FiraMono) will be used.
                                ..default()
                            }
                        } else {
                            // "default_font" feature is unavailable, load a font to use instead.
                            TextStyle {
                                font: asset_server.load("fonts/FiraSans-Bold.ttf"),
                                font_size: 60.0,
                                color: Color::GOLD,
                            }
                        }),
                    ]),
                    FpsText,
                ));
            });
    }

    fn text_update_system_fps(
        diagnostics: Res<DiagnosticsStore>,
        mut query: Query<&mut Text, With<FpsText>>,
    ) {
        for mut text in &mut query {
            if let Some(fps) = diagnostics.get(FrameTimeDiagnosticsPlugin::FPS) {
                if let Some(value) = fps.smoothed() {
                    // Update the value of the second section
                    text.sections[1].value = format!("{value:.2}");
                }
            }
        }
    }

    // cursor to world coordinates
    fn cursor_to_world_coordinates(
        mut asset_map: ResMut<Env>,
        q_window: Query<&Window, With<PrimaryWindow>>,
        q_camera: Query<(&Camera, &GlobalTransform), With<PanOrbitCamera>>,
    ) {
        let window = q_window.single();

        for (camera, camera_transform) in &q_camera {
            if let Some(world_position) = window
                .cursor_position()
                .and_then(|cursor| camera.viewport_to_world(camera_transform, cursor))
                .map(|ray| ray.origin.truncate())
            {
                // asset_map.coor_mouse = world_position;
                // eprintln!("World coords: {}/{}", world_position.x, world_position.y);
            }
        }
    }

    fn draw_cursor(
        camera_query: Query<(&Camera, &GlobalTransform)>,
        ground_query: Query<(&GlobalTransform, &Name, &ViewVisibility)>,
        windows: Query<&Window, With<PrimaryWindow>>,
        mut gizmos: Gizmos,
        mut asset_map: ResMut<Env>,
    ) {
        if let Ok((camera, camera_transform)) = camera_query.get_single() {
            for (ground, name, visible) in &ground_query
            // a modifier, debile d'iterer en perma sur tous les grounds
            {
                if name.contains("ground") && visible.get() == true {
                    let Some(cursor_position) = windows.single().cursor_position() else {
                        return;
                    };

                    // Calculate a ray pointing from the camera into the world based on the cursor's position.
                    let Some(ray) = camera.viewport_to_world(camera_transform, cursor_position)
                    else {
                        return;
                    };

                    // Calculate if and where the ray is hitting the ground plane.
                    let Some(distance) = ray.intersect_plane(ground.translation(), ground.up())
                    else {
                        return;
                    };
                    let mut point = ray.get_point(distance);
                    // println!("{:?}", point);
                    asset_map.coor_mouse = point;
                    // let mut distance = point.distance(ground.translation());
                }
            }
        }

        // Draw a circle just above the ground plane at that position.
        // gizmos.circle(
        //     point + ground.up() * 0.01,
        //     Direction3d::new_unchecked(ground.up()), // Up vector is already normalized.
        //     0.2,
        //     Color::WHITE,
        // );
    }

    fn setup_box_info_case(mut commands: Commands, asset_server: Res<AssetServer>) {
        let font = asset_server.load("fonts/FiraSans-Bold.ttf");

        let text_style = TextStyle {
            font,
            font_size: SIZE_FONT,
            color: Color::WHITE,
        };
        // let box_size = Vec2::new(300.0, 200.0);
        // let box_position = Vec2::new(0.0, -250.0);

        let test_vec = Vec3::new(-27., 27., 0.);
        let other_box_size = Vec2::new(100.0, 100.0);
        let other_box_position = Vec2::new(120.0, -150.0);

        commands
            .spawn(NodeBundle {
                style: Style {
                    width: Val::Percent(15.0),
                    height: Val::Percent(15.0),
                    position_type: PositionType::Absolute,
                    bottom: Val::Px(15.),
                    right: Val::Px(10.),
                    justify_content: JustifyContent::Default,
                    align_items: AlignItems::Default,
                    ..default()
                },
                visibility: Visibility::Hidden,
                // background_color:
                ..default()
            })
            .with_children(|parent| {
                parent.spawn((
                    TextBundle::from_sections([
                        TextSection::new("case.x:", text_style.clone()),
                        TextSection::new("case.y", text_style.clone()),
                        TextSection::new("\nfood:", text_style.clone()),
                        TextSection::new("nbr_food \n", text_style.clone()),
                        TextSection::new("linemate:", text_style.clone()),
                        TextSection::new("nbr_linemate \n", text_style.clone()),
                        TextSection::new("deraumere:", text_style.clone()),
                        TextSection::new("nbr_deraumere \n", text_style.clone()),
                        TextSection::new("Sibur:", text_style.clone()),
                        TextSection::new("nbr_Sibur \n", text_style.clone()),
                        TextSection::new("Mendiane:", text_style.clone()),
                        TextSection::new("nbr_Mendiane \n", text_style.clone()),
                        TextSection::new("Phiras:", text_style.clone()),
                        TextSection::new("nbr_Phiras \n", text_style.clone()),
                        TextSection::new("Thystame: ", text_style.clone()),
                        TextSection::new("nbr_Thystame  \n", text_style.clone()),
                    ]),
                    Case_Text,
                ));
            });
    }

    fn setup_box_info_player(mut commands: Commands, asset_server: Res<AssetServer>) {
        let font = asset_server.load("fonts/FiraSans-Bold.ttf");

        let text_style = TextStyle {
            font,
            font_size: SIZE_FONT,
            color: Color::WHITE,
        };
        // let box_size = Vec2::new(300.0, 200.0);
        // let box_position = Vec2::new(0.0, -250.0);

        let test_vec = Vec3::new(-27., 27., 0.);
        let other_box_size = Vec2::new(100.0, 100.0);
        let other_box_position = Vec2::new(120.0, -150.0);

        commands
            .spawn(NodeBundle {
                style: Style {
                    width: Val::Percent(15.0),
                    height: Val::Percent(15.0),
                    position_type: PositionType::Absolute,
                    top: Val::Px(15.),
                    right: Val::Px(10.),
                    justify_content: JustifyContent::Default,
                    align_items: AlignItems::Default,
                    ..default()
                },
                visibility: Visibility::Hidden,
                // background_color:
                ..default()
            })
            .with_children(|parent| {
                parent.spawn((
                    TextBundle::from_sections([
                        TextSection::new("------PLAYER------\n", text_style.clone()),
                        TextSection::new("Team: ", text_style.clone()),
                        TextSection::new("String Team Name\n", text_style.clone()),
                        TextSection::new("Id: ", text_style.clone()),
                        TextSection::new("Id_number \n", text_style.clone()),
                        TextSection::new("coor: ", text_style.clone()),
                        TextSection::new("coor number \n", text_style.clone()),
                        TextSection::new("lvl: ", text_style.clone()),
                        TextSection::new("lvl number \n", text_style.clone()),
                        TextSection::new("inventory: ", text_style.clone()),
                        TextSection::new("alors la mon pote", text_style.clone()),
                    ]),
                    Player_Text,
                ));
            });
    }

    // fn setup_box_info_player(mut commands: Commands, asset_server: Res<AssetServer>) {
    //     let font = asset_server.load("fonts/FiraSans-Bold.ttf");

    //     let slightly_smaller_text_style = TextStyle {
    //         font,
    //         font_size: 9.0,
    //         color: Color::WHITE,
    //     };
    //     // let box_size = Vec2::new(300.0, 200.0);
    //     // let box_position = Vec2::new(0.0, -250.0);

    //     let other_box_size = Vec2::new(100.0, 100.0);
    //     let other_box_position = Vec2::new(120.0, -150.0);
    //     commands
    //         .spawn((SpriteBundle {
    //             sprite: Sprite {
    //                 color: Color::rgb(0.70, 0., 0.),
    //                 custom_size: Some(Vec2::new(other_box_size.x, other_box_size.y)),
    //                 ..default()
    //             },
    //             transform: Transform::from_translation(other_box_position.extend(15.)),
    //             visibility: Visibility::Hidden,
    //             ..default()
    //         },
    //         Player_Box,
    //         )
    //     )
    //         .with_children(|builder| {
    //             builder.spawn((Text2dBundle {
    //                 text: Text {
    //                     sections: vec![
    //                     TextSection::new(
    //                         "------PLAYER------\n",
    //                         slightly_smaller_text_style.clone(),
    //                     ),
    //                     TextSection::new(
    //                         "Team: ",
    //                         slightly_smaller_text_style.clone(),
    //                     ),
    //                     TextSection::new(
    //                         "String Team Name\n",
    //                         slightly_smaller_text_style.clone(),
    //                     ),
    //                     TextSection::new(
    //                         "Id: ",
    //                         slightly_smaller_text_style.clone(),
    //                     ),
    //                     TextSection::new(
    //                         "Id_number \n",
    //                         slightly_smaller_text_style.clone(),
    //                     ),
    //                     TextSection::new(
    //                         "coor: ",
    //                         slightly_smaller_text_style.clone(),
    //                     ),
    //                     TextSection::new(
    //                         "coor number \n",
    //                         slightly_smaller_text_style.clone(),
    //                     ),
    //                     TextSection::new(
    //                         "lvl: ",
    //                         slightly_smaller_text_style.clone(),
    //                     ),
    //                     TextSection::new(
    //                         "lvl number \n",
    //                         slightly_smaller_text_style.clone(),
    //                     ),
    //                     TextSection::new(
    //                         "inventory: ",
    //                         slightly_smaller_text_style.clone(),
    //                     ),
    //                     TextSection::new(
    //                         "alors la mon pote",
    //                         slightly_smaller_text_style.clone(),
    //                     ),
    //                     ],
    //                     // justify: JustifyText::Left,
    //                     alignment: TextAlignment::Left,
    //                     linebreak_behavior: BreakLineOn::WordBoundary,
    //                     ..default()
    //                 },
    //                 text_2d_bounds: Text2dBounds {
    //                     // Wrap text in the rectangle
    //                     size: other_box_size,
    //                 },
    //                 // ensure the text is drawn on top of the box
    //                 transform: Transform::from_translation(Vec3::Z),
    //                 visibility: Visibility::Inherited,
    //                 ..default()
    //                 },
    //             Player_Text)
    //         );
    //         });
    // }

    fn setup_box_info_egg(mut commands: Commands, asset_server: Res<AssetServer>) {
        let font = asset_server.load("fonts/FiraSans-Bold.ttf");

        let slightly_smaller_text_style = TextStyle {
            font,
            font_size: 9.0,
            color: Color::WHITE,
        };
        // let box_size = Vec2::new(300.0, 200.0);
        // let box_position = Vec2::new(0.0, -250.0);

        let other_box_size = Vec2::new(100.0, 100.0);
        let other_box_position = Vec2::new(120.0, -150.0);
        commands
            .spawn((
                SpriteBundle {
                    sprite: Sprite {
                        color: Color::rgb(0.70, 0.50, 0.),
                        custom_size: Some(Vec2::new(other_box_size.x, other_box_size.y)),
                        ..default()
                    },
                    transform: Transform::from_translation(other_box_position.extend(15.)),
                    visibility: Visibility::Hidden,
                    ..default()
                },
                Egg_Box,
            ))
            .with_children(|builder| {
                builder.spawn((
                    Text2dBundle {
                        text: Text {
                            sections: vec![
                                TextSection::new(
                                    "------Egg------\n",
                                    slightly_smaller_text_style.clone(),
                                ),
                                TextSection::new("Team: ", slightly_smaller_text_style.clone()),
                                TextSection::new(
                                    "String Team Name\n",
                                    slightly_smaller_text_style.clone(),
                                ),
                                TextSection::new("Id: ", slightly_smaller_text_style.clone()),
                                TextSection::new(
                                    "Id_number \n",
                                    slightly_smaller_text_style.clone(),
                                ),
                                TextSection::new("coor: ", slightly_smaller_text_style.clone()),
                                TextSection::new(
                                    "coor number \n",
                                    slightly_smaller_text_style.clone(),
                                ),
                            ],
                            // justify: JustifyText::Left,
                            alignment: TextAlignment::Left,
                            linebreak_behavior: BreakLineOn::WordBoundary,
                            ..default()
                        },
                        text_2d_bounds: Text2dBounds {
                            // Wrap text in the rectangle
                            size: other_box_size,
                        },
                        // ensure the text is drawn on top of the box
                        transform: Transform::from_translation(Vec3::Z),
                        visibility: Visibility::Inherited,
                        ..default()
                    },
                    Egg_Text,
                ));
            });
    }

    fn update_info_button_player(
        q_window: Query<&Window, With<PrimaryWindow>>,
        mut keys: Res<Input<KeyCode>>,
        mut asset_map: ResMut<Env>,
        mut query_players_pos: Query<
            (&mut Transform, &PlayerId, &Cell, Entity),
            (With<Player>, Without<Case_Box>, Without<Player_Box>),
        >,
        mut query_player_text: Query<
            (&mut Text, &mut Visibility),
            (With<Player_Text>, Without<Egg_Box>),
        >,
        mut test: Query<(&mut Handle<StandardMaterial>, Entity)>, //materials: Res<Assets<StandardMaterial>>,
        mut materials: ResMut<Assets<StandardMaterial>>,
        all_entities_with_parents_query: Query<&Parent>,
        query_player: Query<(&Cell, &PlayerId)>,
    ) {
        let window = q_window.single();
        if keys.pressed(KeyCode::ControlLeft) {
            let mut dist_min: f32 = 1000.0;
            let dist = asset_map.coor_mouse;
            //println!("dist {:?}", dist);
            for (pos, playerid, cell, entity) in &mut query_players_pos {
                let dist = asset_map.coor_mouse.distance(pos.translation);
                // pos.translation.truncate()
                // println!("pos = {:?}", pos.translation);
                dist_min = dist_min.min(dist);
                //println!("dist_min {:?} {:?}", dist_min, playerid.0);
                if dist_min < 10. && dist_min == dist {
                    // change_alpha_material(
                    //     entity,
                    //     &mut test,
                    //     &mut materials,
                    //     &all_entities_with_parents_query,
                    //     0.,
                    // );

                    // sprite.color.set_a(0.1);
                    // println!("bonjour");
                    for (mut output, mut visibility) in &mut query_player_text {
                        format_player_info(&mut output, &asset_map, &playerid.0, cell);
                        *visibility = Visibility::Visible;
                        // for (mut box_pos , mut visibility) in & mut query_text_box_player
                        // {
                        //     // println!("box_pos = {:?}", box_pos);
                        //     *visibility = Visibility::Visible;
                        //     box_pos.translation.x = asset_map.coor_mouse.x + 100.;
                        //     box_pos.translation.y = asset_map.coor_mouse.y - 100.;
                        // }
                    }
                    // format_all_ressource_button(& mut output, &asset_map, &coor.0);

                    // println!("hasmap playable ??? {:?}", asset_map.print_info_playable(playerid.0));
                } else if dist_min > 10. {
                    // change_alpha_material(
                    //     entity,
                    //     &mut test,
                    //     &mut materials,
                    //     &all_entities_with_parents_query,
                    //     255.,
                    // );
                    // sprite.color.set_a(1.);
                    for (mut box_pos, mut visibility) in &mut query_player_text {
                        *visibility = Visibility::Hidden;
                    }
                }
            }
        }
        if keys.just_released(KeyCode::ControlLeft) {
            for (_, mut visibility) in &mut query_player_text {
                *visibility = Visibility::Hidden;
            }
            for (_, _, _, entity) in &mut query_players_pos {
                // change_alpha_material(
                //     entity,
                //     &mut test,
                //     &mut materials,
                //     &all_entities_with_parents_query,
                //     255.,
                // );
                // sprite.color.set_a(1.);
            }
        }
    }

    // fn update_info_button_egg(
    //     q_window: Query<&Window, With<PrimaryWindow>>,
    //     mut keys: Res<Input<KeyCode>>,
    //     mut asset_map: ResMut<RessCommandId>,
    //     mut query_egg_pos: Query<(& mut Transform, &mut Sprite, &EggId, &Cell), (With<EggId>)>,
    //     mut query_egg_text: Query<&mut Text, (With<Egg_Text>, Without<Player_Text>)>,
    //     mut query_text_box_egg: Query<(& mut Transform, & mut Visibility), (Without<EggId>, Without<Player_Box>, Without<Case_Box>, With<Egg_Box>)>,
    // )
    // {
    //     let window = q_window.single();
    //     if keys.pressed(KeyCode::ControlLeft)
    //     {
    //       let mut dist_min: f32 = 1000.0;
    //       for (pos, mut sprite, eggid, cell) in & mut query_egg_pos
    //           {

    //               // pos.translation.truncate()
    //               // println!("pos = {:?}", pos.translation);
    //               let dist_min = asset_map.coor_mouse;
    //             //   dist_min = dist_min.min(dist);
    //               // println!("dist_min {:?}", dist_min);
    //               if dist_min < 10.
    //               {
    //                   sprite.color.set_a(0.1);
    //                   // println!("bonjour");
    //                   for mut output in & mut query_egg_text{
    //                       format_player_info(&mut output, &asset_map, &eggid.0, cell);
    //                       for (mut box_pos , mut visibility) in & mut query_text_box_egg
    //                       {
    //                           // println!("box_pos = {:?}", box_pos);
    //                           *visibility = Visibility::Visible;
    //                           box_pos.translation.x = asset_map.coor_mouse.x + 100.;
    //                           box_pos.translation.y = asset_map.coor_mouse.y - 100.;
    //                       }

    //                   }
    //                   // format_all_ressource_button(& mut output, &asset_map, &coor.0);

    //                   // println!("hasmap playable ??? {:?}", asset_map.print_info_playable(playerid.0));
    //               }
    //               else if dist_min > 10.
    //               {
    //                   sprite.color.set_a(1.);
    //                   for (mut box_pos , mut visibility) in & mut query_text_box_egg
    //                   {
    //                       *visibility = Visibility::Hidden;
    //                   }
    //               }
    //           }
    //     }
    //     if keys.just_released(KeyCode::ControlLeft)
    //     {
    //         for (_ , mut visibility) in & mut query_text_box_egg
    //         {
    //             *visibility = Visibility::Hidden;
    //         }
    //         for (_, mut sprite,_, _) in & mut query_egg_pos
    //         {
    //             sprite.color.set_a(1.);

    //         }
    //     }
    // }
    pub fn change_alpha_material(
        entity_tile: Entity,
        test: &mut Query<(&mut Handle<StandardMaterial>, Entity)>, //materials: Res<Assets<StandardMaterial>>,
        materials: &mut ResMut<Assets<StandardMaterial>>,
        all_entities_with_parents_query: &Query<&Parent>,
        alpha: f32,
    ) {
        // let nbr_team = asset_map.nbr_equipe;
        for (mut test_mat, entity) in test {
            let parent_entity = get_top_parent(entity, &all_entities_with_parents_query);
            if entity_tile == parent_entity {
                if let Some(mat) = materials.get_mut(test_mat.id()) {
                    let mut new_material = mat.clone();
                    // println!("base_color = {:?}", new_material.base_color);
                    // let mut white = Color::WHITE;
                    new_material.alpha_mode = AlphaMode::Premultiplied;
                    new_material.base_color = *new_material.base_color.set_a(alpha);
                    *test_mat = materials.add(new_material);
                    return;
                };
            }
        }
    }

    fn update_info_case(
        mut keys: Res<Input<KeyCode>>,
        mut env: ResMut<Env>,
        // query to get the window (so we can read the current cursor position)
        q_window: Query<&Window, With<PrimaryWindow>>,
        // query to get camera transform
        mut query_tiles: Query<(&Transform, &Position, &ViewVisibility, &Coor, Entity), With<Tile>>,
        mut query_text_box: Query<(&mut Text, &mut Visibility), (With<Case_Text>, Without<Tile>)>,
        mut test: Query<(&mut Handle<StandardMaterial>, Entity)>, //materials: Res<Assets<StandardMaterial>>,
        mut materials: ResMut<Assets<StandardMaterial>>,
        all_entities_with_parents_query: Query<&Parent>,
        query_player: Query<(&Cell, &PlayerId)>,
        // mut q_translation: Query<& mut Transform, With<Button>>,
    ) {
        // get the camera info and transform
        // assuming there is exactly one main camera entity, so Query::single() is OK
        let window = q_window.single();
        // Convert cursor to world coordinates (should be outside of the cursor system)

        // let (camera, camera_transform) = q_camera.single();

        if keys.pressed(KeyCode::ControlLeft)
        // on doit ajouter si les dernieres coordonnes de tiles sont update ou non
        {
            // println!("coor_mouse = {:?}", asset_map.coor_mouse);
            // je parcours toutes les tiles et je recupere leur metadonnees
            for (transform, pos, view, coor, entity_tile) in &query_tiles {
                // je ne parcours que les tiles qui sont visibles a l'ecran
                if view.get() == true {
                    let dist = env.coor_mouse.distance(pos.0);
                    // println!("dist = {:?}", dist);
                    if dist < TILES_WIDTH / 2.0 {
                        // println!("position {:?}", pos);
                        for (mut output, mut visibility) in &mut query_text_box {
                            format_all_ressource_button(&mut output, &env, &coor.0);
                            *visibility = Visibility::Visible;
                        }
                        // change_alpha_material(
                        //     entity_tile,
                        //     &mut test,
                        //     &mut materials,
                        //     &all_entities_with_parents_query,
                        //     0.,
                        // );
                        // sprite.color.set_a(0.1);
                    } else {
                        // change_alpha_material(
                        //     entity_tile,
                        //     &mut test,
                        //     &mut materials,
                        //     &all_entities_with_parents_query,
                        //     255.,
                        // ); // c'est ce qui bouffe le plus de fps
                           // sprite.color.set_a(1.);
                    }
                }
            }
        }
        if keys.just_released(KeyCode::ControlLeft) {
            for (_, _, _, mut sprite, entity) in &mut query_tiles {
                // sprite.color = Color::rgb_u8(1, 1, 1);
                // change_alpha_material(
                //     entity,
                //     &mut test,
                //     &mut materials,
                //     &all_entities_with_parents_query,
                //     255.,
                // );
            }
            for (mut output, mut visibility) in &mut query_text_box {
                *visibility = Visibility::Hidden;
            }
        }
        // There is only one primary window, so we can similarly get it from the query:

        // check if the cursor is inside the window and get its position
        // then, ask bevy to convert into world coordinates, and truncate to discard Z
    }
}
