pub mod camera {
    use bevy::prelude::*;
    use bevy_panorbit_camera::PanOrbitCamera;

    use crate::env::env::Env;

    pub fn spawn_camera(commands: &mut Commands) {
        // let mut spec_camera = Transform::from_translation(Vec3::new(152.62865, 191.1, 550.77405));
        // spec_camera.rotation = Quat::from_xyzw(-0.21, -0.00032220798, -7.042944e-5, 0.97693384);
        //println!("Camera init?");
        commands.spawn((
            Camera3dBundle {
                transform: Transform {
                    translation: Vec3::new(63.556747, 224.23383, 524.0234),
                    rotation: Quat::from_xyzw(-0.2135419, -0.00032220798, -7.042944e-5, 0.97693384),
                    ..default()
                },
                ..default()
            },
            PanOrbitCamera::default(),
        ));
        commands.spawn(PointLightBundle {
            point_light: PointLight {
                intensity: 400000.0,
                shadows_enabled: false,
                range: 10000.0,
                ..default()
            },
            //transform: Transform::from_xyz(100.0, 100.0, 30.0),
            transform: Transform::from_xyz(
                100.0,
                100.0,
                100.0,
            ),
            ..default()
        });
    }

    pub fn setup_lights(res: &ResMut<Env>, mut commands: Commands, mut spec_camera: Transform) {
        // spec_camera.translation = Vec3(152.62865, 191.11111, 550.77405), rotation: Quat(-0.2135419, -0.00032220798, -7.042944e-5, 0.97693384), scale: Vec3(1.0, 1.0, 1.0)
        spec_camera = Transform {
            translation: Vec3::new(63.556747, 224.23383, 524.0234),
            rotation: Quat::from_xyzw(-0.2135419, -0.00032220798, -7.042944e-5, 0.97693384),
            ..default()
        };
        // spec_camera.rotation =
        // Quat::from_xyzw(-0.2135419, -0.00032220798, -7.042944e-5, 0.97693384);
        // spec_camera =
        let (x, y) = res.center_map_new_system(res.x as f32 / 2.0, res.y as f32 / 2.0);
        let pos_center_map: Vec3 = Vec3::new(x, 100.0, y);

        // spec_camera.rotation = Quat::from_xyzw(-0.21, 0., 0., 1.);
        // commands.spawn((
        //     Camera3dBundle {
        //         transform: *spec_camera,
        //         ..default()
        //     },
        //     PanOrbitCamera::default(),
        // ));

        commands.spawn(PointLightBundle {
            point_light: PointLight {
                intensity: 400000.0,
                shadows_enabled: false,
                range: 10000.0,
                ..default()
            },
            //transform: Transform::from_xyz(100.0, 100.0, 30.0),
            transform: Transform::from_xyz(
                pos_center_map.x - 100.0,
                pos_center_map.y,
                pos_center_map.z - 100.0,
            ),
            ..default()
        });
        commands.spawn(PointLightBundle {
            point_light: PointLight {
                intensity: 400000.0,
                shadows_enabled: false,
                range: 10000.0,
                ..default()
            },
            //transform: Transform::from_xyz(100.0, 100.0, 30.0),
            transform: Transform::from_xyz(
                pos_center_map.x + 100.0,
                pos_center_map.y,
                pos_center_map.z - 100.0,
            ),
            ..default()
        });
        commands.spawn(PointLightBundle {
            point_light: PointLight {
                intensity: 400000.0,
                shadows_enabled: false,
                range: 10000.0,
                ..default()
            },
            //transform: Transform::from_xyz(100.0, 100.0, 30.0),
            transform: Transform::from_xyz(
                pos_center_map.x - 100.0,
                pos_center_map.y,
                pos_center_map.z + 100.0,
            ),
            ..default()
        });
        commands.spawn(PointLightBundle {
            point_light: PointLight {
                intensity: 400000.0,
                shadows_enabled: false,
                range: 10000.0,
                ..default()
            },
            //transform: Transform::from_xyz(100.0, 100.0, 30.0),
            transform: Transform::from_xyz(pos_center_map.x, pos_center_map.y, pos_center_map.z),
            ..default()
        });
    }
}
