/*
    Au lancement du programme on commence par init la structure Env et spawn la camera.

    Ensuite read_stream() reçoit les pkts Parse de la part de main.rs:rcv_from_server() et les envoi à la fonction dispatch (sous la forme d'Event, type propre a bevy).

    Le sujet parle de "commandes" et il est vrai que le terme est plus explicite, malheuresement le mot clé Command est deja pris par bevy.

    Dispatch se base sur le type de Parse qu'il reçoit:

        -   Si le Parse est SIMPLE, c-a-d que son evenement associé ne contient qu'un seul Parse
            (exemple Avance/Droite/Gauche -> MovementPlayer, Connexion d'un joueur -> ConnexionPlayer etc...)
            Dans ce cas son traitement est simple (fn simple_command()):
            -   Si le jeu n'est pas encore lancé, une fois avoir collecté tous les Parses pour le setup de la partie:
                => on envoi GRAPHIC au server
                => la partie commence
            -   La partie est lancé et on le traite naivement

        -   Si le Parse est dit COMPLEXE, c-a-d que son evenement associé contient plusieurs Parse:
            (ex: Prend -> [Prend, Inventaire, Resource], Expulse -> [Expulse, MovementPlayer, ... , MovementPlayer])
            On vient verifier le nbr de Parse contenu dans la structure de reference de Dispatch, ComplexParse, dont le but est de stacker les Parse:
                Dans dispatch_action_event():
                - Si ComplexeParse.nbr_parse == 0:
                    on construit partiellement ComplexeParse en settant first_parse, nbr_parse et vec_id_parse
                    (ex: Prend -> ComplexeParse{first_parse: Parse::Prend, nbr_parse: 2, vec_id_parse: [33, 34], vec_parse: vec![]})
                - Ainsi a l'arrivé du prochain Parse: ComplexeParse.nbr_parse != 0:
                    on stack les Parse dans ComplexeParse.vec_parse
                    on construit notre vec_parse en pushant les Parse dont l'id d'event (merci bevy) et stockes dans vec_id_parse
                    (ex: Prend -> ComplexeParse{first_parse: Parse::Prend, nbr_parse: 2, vec_id_parse: [33, 34], vec_parse: [Inventaire(id_event : 33), Resource(id_event :34)]})
                - On sait que c'est terminé lorsque la longueur de vec_parse == nbr_parse:
                    On peut ainsi traiter tout l'"evenement" dans son contexte et ne pas avoir a traiter les Parse un par un, dans leur ordre d'arrivé

*/

pub mod dispatch {
    use std::io::Write;

    use bevy::{
        app::{App, Plugin, Startup, Update},
        asset::{AssetServer, Assets},
        ecs::{
            entity::Entity,
            event::{EventId, EventReader, EventWriter},
            schedule::IntoSystemConfigs,
            system::{Commands, Query, Res, ResMut, Resource},
        },
        hierarchy::DespawnRecursiveExt,
        pbr::StandardMaterial,
        prelude::{AnimationPlayer, NodeBundle, With},
        render::{
            mesh::Mesh,
            view::{visibility, Visibility},
        },
        scene::{SceneInstance, SceneSpawner},
        sprite::TextureAtlas,
        text::Text,
        transform::{self, components::Transform},
        ui::{BackgroundColor, Style},
    };
    use bevy_panorbit_camera::PanOrbitCamera;

    use crate::{
        action::action::{
            add_action, query_cell_player, query_nbr_player_cell, ActionPlayer, TypeAction, VEC_COLORS,
        },
        camera::camera::setup_lights,
        env::env::{Env, Incantation},
        map::map::{spawn_map, Cell},
        parser::parser::{is_parse_simple, Parse},
        player_entities::player_entities::{
            change_color_of_egg, setup_entity_player, setup_sprite_egg, Player as Player_Component,
            PlayerId,
        },
        resources::resource::{decrease_resources, increase_resources, spawn_resources, Resources},
        LoadingResource, LoadingScreen, LoadingScreenText, ResourceStream, StreamEvent,
        StreamReceiver,
    };

    // const for teams folder name

    pub struct Dispatch;

    #[derive(Resource, Debug)]
    pub struct ComplexeParse {
        pub nbr_parse_waited: usize,
        pub vec_id_parse: Vec<usize>,
        pub first_parse: Parse,
        pub vec_parse: Vec<Parse>,
    }

    impl Default for ComplexeParse {
        fn default() -> Self {
            Self {
                nbr_parse_waited: 0,
                vec_id_parse: vec![],
                first_parse: Parse::Donothing,
                vec_parse: vec![],
            }
        }
    }

    impl ComplexeParse {
        pub fn fflush(&mut self) {
            self.nbr_parse_waited = 0;
            self.vec_id_parse.clear();
            self.first_parse = Parse::Donothing;
            self.vec_parse.clear();
        }
    }

    // Ne s'execute qu une fois, au lancement du server_gfx
    fn init(mut commands: Commands) {
        commands.insert_resource(Env::default());
        // spawn_pan_camera(commands); // TODO: En discuter de quand on veut spawn la camera (old_position here)
    }

    pub fn condition_wait_instance_player(env: Res<Env>) -> bool {
        (env.nbr_player_connected == env.nbr_player_waited) && !env.game_started
    }

    // verifie si les entities sont "pretes" (loadés par le jeu)
    pub fn player_instance_is_ready(
        unloaded_instances: Query<(Entity, &SceneInstance, &PlayerId)>,
        mut env: ResMut<Env>,
        scene_manager: Res<SceneSpawner>,
    ) {
        let mut nbr_player_is_ready = 0;
        for (entity, instance, _) in unloaded_instances.iter() {
            if scene_manager.instance_is_ready(**instance) {
                nbr_player_is_ready += 1;
                //println!("instance is ready {:?} nbr_player_is_ready = {}", entity, nbr_player_is_ready);
                if nbr_player_is_ready == env.nbr_player_connected {
                    // si oui on peut demarrer
                    env.end_connexion = true;
                    return;
                }
            } else {
                println!("Players Not Ready Yet")
            }
        }
    }

    pub fn condition_start_game(env: Res<Env>) -> bool {
        env.end_connexion && !env.game_started
    }

    pub fn game_finish(env: Res<Env>) -> bool {
        env.game_finish
    }

    fn load_animation_game_finish(
        loading_screen: Res<LoadingResource>,
        mut env: ResMut<Env>,
        mut state: ResMut<ResourceStream>,
        mut visibility: Query<(&mut Visibility, &mut BackgroundColor), With<LoadingScreen>>,
        mut text: Query<(&mut Text), With<LoadingScreenText>>,
        mut query_camera: Query<&mut Transform, With<PanOrbitCamera>>,
        commands: Commands,
    ) {
        // if let Some(stream_clone) = &mut state.stream_clone {
        // let _ = stream_clone.write(format!("GRAPHIC\n").as_bytes());
        let (mut visibility, mut bgc) = visibility.get_mut(loading_screen.loading_entity).unwrap();
        *visibility = Visibility::Visible;
        let (mut text) = text.single_mut();
        text.sections[0].value = format!("TEAM {} WIN", env.team_winner.0.clone());
        *bgc = BackgroundColor(VEC_COLORS[env.team_winner.1 as usize]);
        // setup_camera_game(&env, commands, *query_camera.single_mut()); &mut Text,
        // }
        // spawn_pan_camera(env, commands);
    }

    fn write_stream(
        loading_screen: Res<LoadingResource>,
        mut env: ResMut<Env>,
        mut state: ResMut<ResourceStream>,
        mut visibility: Query<(&mut Visibility), With<LoadingScreen>>,
        mut query_camera: Query<&mut Transform, With<PanOrbitCamera>>,
        commands: Commands,
    ) {
        if let Some(stream_clone) = &mut state.stream_clone {
            let _ = stream_clone.write(format!("GRAPHIC\n").as_bytes());
            let mut visibility = visibility.get_mut(loading_screen.loading_entity).unwrap();
            *visibility = Visibility::Hidden;
            setup_lights(&env, commands, *query_camera.single_mut());
            // commands
            // .entity(loading_screen.loading_entity)
            // .despawn_recursive();
            // for (entity, instance, _) in unloaded_instances.iter() {
            // if scene_manager.instance_is_ready(**instance) {
            // println!("instance is STILL ready {:?}", entity);
            // } else {
            // }
            // }

            // let mut visibility = *loading_screen.single_mut();
            // visibility = Visibility::Hidden;
        }
        env.game_started = true;
        // spawn_pan_camera(env, commands);
    }

    impl Plugin for Dispatch {
        fn build(&self, app: &mut App) {
            app.add_event::<StreamEvent>()
                .insert_resource(ComplexeParse::default())
                .add_systems(Startup, init)
                .add_systems(Update, read_stream)
                .add_systems(
                    Update,
                    player_instance_is_ready.run_if(condition_wait_instance_player),
                )
                .add_systems(Update, write_stream.run_if(condition_start_game))
                .add_systems(Update, load_animation_game_finish.run_if(game_finish))
                .add_systems(Update, change_color_of_egg)
                // .add_systems(Update, print_debug_camera)
                .add_systems(Update, dispatch);
        }
    }

    fn print_debug_camera(mut query_camera: Query<&mut Transform, With<PanOrbitCamera>>) {
        for vec_tranform in query_camera.iter() {
            //println!("CAMERA = {:?}", vec_tranform);
        }
    }

    fn read_stream(receiver: Res<StreamReceiver>, mut events: EventWriter<StreamEvent>) {
        for parse in receiver.try_iter() {
            events.send(StreamEvent(parse));
        }
    }

    pub fn dispatch(
        mut reader: EventReader<StreamEvent>,
        mut commands: Commands,
        asset_server: Res<AssetServer>,
        mut asset_meshes: ResMut<Assets<Mesh>>,
        mut materials: ResMut<Assets<StandardMaterial>>,
        mut query_player_cell: Query<(Entity, &Cell, &Player_Component)>,
        _: ResMut<Assets<TextureAtlas>>,
        mut asset_map: ResMut<Env>,
        mut complexcommand: ResMut<ComplexeParse>,
        mut query_action_player: Query<&mut ActionPlayer>,
        mut query_transform_res: Query<&mut Transform>,
        mut animation_players: Query<(&mut AnimationPlayer, Entity)>,
        _: ResMut<ResourceStream>,
    ) {
        //println!("TORTUE NINJA");
        for event in reader.read_with_id() {
            let parse = &event.0 .0;
            let streamevent = event.1;
            if complexcommand.nbr_parse_waited == 0 {
                if is_parse_simple(&parse) {
                    dispatch_simple_parse(
                        parse,
                        &mut commands,
                        &asset_server,
                        &mut asset_map,
                        &mut query_action_player,
                        &mut asset_meshes,
                        &mut materials,
                    );
                } else {
                    init_complexe_parse(
                        &mut asset_map,
                        &mut complexcommand,
                        &mut query_player_cell,
                        &parse,
                        &streamevent,
                    );
                }
            } else {
                handle_complexe_parse(
                    &parse,
                    &mut asset_map,
                    &mut query_player_cell,
                    &mut complexcommand,
                    &streamevent,
                    &mut commands,
                    &mut query_transform_res,
                    &mut query_action_player,
                    &asset_server,
                    &mut materials,
                    &mut asset_meshes,
                );
            }
        }
    }

    pub fn dispatch_simple_parse(
        parse: &Parse,
        commands: &mut Commands,         //spawn des entity
        asset_server: &Res<AssetServer>, // access aux assets de la partie
        env: &mut ResMut<Env>,           // ptr sur env
        mut query_action_player: &mut Query<&mut ActionPlayer>,
        asset_meshes: &mut ResMut<Assets<Mesh>>,
        materials: &mut ResMut<Assets<StandardMaterial>>,
    ) {
        match parse {
            Parse::Map(x, y) => {
                spawn_map(*x, *y, commands, &asset_server, env);
                env.set_x_y_pixel(*x, *y);
                env.set_hashmap_resources(*x, *y);
            }
            Parse::Time(t) => {
                env.time = *t;
            }
            Parse::ResourceCase(x, y, ..) => {
                let (x_pixel, y_pixel) = env.center_map_new_system(*x as f32, *y as f32);
                let mut resources = Resources::from(parse);
                resources.x_rel = x_pixel;
                resources.y_rel = y_pixel;
                spawn_resources(
                    commands,
                    &asset_server,
                    resources,
                    &mut env.id_resources,
                    &0,
                    asset_meshes,
                    materials,
                );
            }
            Parse::NomEquipe(n) => {
                env.name_equipe.push((*n.clone()).to_string());
                env.nbr_equipe += 1;
                env.load_all_animations(&asset_server);
            }
            Parse::ConnexionPlayer(id, x, y, o, l, n) => {
                let (x_pixel, y_pixel) = env.center_map_new_system(*x as f32, *y as f32);
                let team_name = n.to_string();
                let team_num = env.get_num_team_from_name(&team_name).unwrap() as usize;
                let entity = setup_entity_player(
                    asset_server,
                    commands,
                    (x_pixel, y_pixel),
                    (*x, *y, *o),
                    team_num as u8,
                    *id,
                );
                env.set_new_entry_hashmap_player(id, l, team_name, team_num as u8, entity);
                env.nbr_player_connected += 1; // est comparé dans
            }
            Parse::MovementPlayer(id, x, y, o) => {
                let mov = TypeAction::Movement {
                    0: *x,
                    1: *y,
                    2: *o,
                };
                /*
                   leftovers comment: method to get the id is wrong because if a player died the index of vector won't be reliable anymore
                   (like arsenal_id [1, 2] chelsea_id [3, 4] => arsenal_id [1, 2] chelsea_id [3], => arsenal_id [1, 2, 5(egg)] chelsea_id [3])
                */
                add_action(query_action_player, &env.get_player_entity(&id), mov);
            }
            Parse::Fork(id) => {
                let fork = TypeAction::Fork(*id);
                add_action(&mut query_action_player, &env.get_player_entity(id), fork);
            }

            Parse::EndFork(egg_id, id, x, y) => {
                // endup the animation of forking by adding an action TypeAction::EndFork
                if !env.end_connexion {
                    // lorsque le jeu n'est pas demarré endfork permet au server_gfx de savoir cmb de joueur il "attends"
                    env.nbr_player_waited += 1;
                } else {
                    let team_num = env.get_player_num_team(&id) as usize;
                    let endfork = TypeAction::EndFork(*id);
                    add_action(
                        &mut query_action_player,
                        &env.get_player_entity(id),
                        endfork,
                    );
                    // les coordonnees (pixel) de l'oeuf doivent etre celles de l'entité du joueur qui l'a pondu
                    let (x_pixel, y_pixel) = env.center_map_new_system(*x as f32, *y as f32);
                    let entity_egg = setup_sprite_egg(
                        commands,
                        asset_server,
                        (*x, *y),
                        (x_pixel, y_pixel),
                        *egg_id,
                        team_num as u8,
                        Visibility::Visible,
                    );
                    //create a new entry in a hashset table
                    env.set_new_entry_hashmap_egg(egg_id, entity_egg, team_num as u8);
                }
            }
            Parse::EggHatch(id_egg) => {
                let egg_entity = env.get_player_entity(id_egg);
                commands.entity(egg_entity).despawn_recursive();
            }
            Parse::Death(id) => {
                let dead_entity_player = env.get_player_entity(id);
                commands.entity(dead_entity_player).despawn_recursive();
                env.remove_player(id); 
            }
            Parse::Incantation(x, y, l, vec_id) => {
                let mut vec_entity: Vec<Entity> = vec![];
                for id in vec_id {
                    let entity_incantation = env.get_player_entity(id);
                    add_action(
                        &mut query_action_player,
                        &entity_incantation,
                        TypeAction::Inc,
                    );
                    vec_entity.push(env.get_player_entity(id));
                }
                let coor_pixel = env.center_map_new_system(*x as f32, *y as f32);
                let team = env.get_player_num_team(&vec_id[0]);
                let incantation = Incantation::new(
                    commands,
                    vec_id.clone(),
                    vec_entity,
                    (*x, *y),
                    coor_pixel,
                    1,
                    *l,
                    team,
                );
                // set up in hashset<Playable> les joueurs concernes par l'incantation
                env.incantation_in_progress.push_back(incantation);
            }
            Parse::Broadcast(id, _) => {
                let entity_player = env.get_player_entity(id);
                let num_team = env.get_player_num_team(&id);

                let vec_playable = env.get_all_entity_of_a_team(&id, &num_team);

                add_action(
                    query_action_player,
                    &entity_player,
                    TypeAction::Broadcast(num_team, entity_player, vec_playable),
                );
                // add_action(query_action_player, &env.get_player_entity(&id), mov);

                //println!("from dispatch_simple_parse -> id={id}");
            }
            _ => (),
        }
    }

    // [Map , ... , Expulse/Prend/Incantation, ... ]
    pub fn init_complexe_parse(
        env: &mut ResMut<Env>,
        complexcommand: &mut ResMut<ComplexeParse>,
        mut query_player_cell: &mut Query<(Entity, &Cell, &Player_Component)>,
        parse: &Parse,
        streamevent: &EventId<StreamEvent>,
    ) {
        // Complex command
        //first command => prend
        // nb_command = 2
        // 33
        // [34, 35]
        //
        match parse {
            Parse::Expulse(id) => {
                // cherche le nb de joueurs concerné par expulse equivalant au nombre de commande a attendre
                let nbr_mov_command_waited =
                    query_nbr_player_cell(&mut query_player_cell, env.get_player_entity(&id));
                complexcommand.first_parse = parse.clone();
                complexcommand.nbr_parse_waited = nbr_mov_command_waited;
                let first_id = streamevent.id + 1;
                let last_id = streamevent.id + nbr_mov_command_waited as usize + 1;
                for i in first_id..last_id {
                    complexcommand.vec_id_parse.push(i); // [32, 33]
                }
                env.last_event_id_visited = streamevent.id; //31
            }
            Parse::Prend(_, _) | Parse::Pose(_, _) => {
                let nbr_mov_command_waited = 2;
                complexcommand.first_parse = parse.clone();
                complexcommand.nbr_parse_waited = nbr_mov_command_waited;
                let first_id = streamevent.id + 1;
                let last_id = streamevent.id + nbr_mov_command_waited as usize + 1;
                for i in first_id..last_id {
                    complexcommand.vec_id_parse.push(i);
                }
                env.last_event_id_visited = streamevent.id;
            }
            Parse::EndIncantation(..) => {
                if let Some(current_inc) = env.incantation_in_progress.front() {
                    // let nbr_command_waited =
                    //     current_inc.player_concern.len() + (env.x * env.y) as usize - 1; // +1 pour la case
                    let nbr_command_waited =
                        current_inc.player_concern.len() - 1; // +1 pour la case
                    complexcommand.first_parse = parse.clone();
                    complexcommand.nbr_parse_waited = nbr_command_waited;
                    let first_id = streamevent.id + 1;
                    let last_id = streamevent.id + nbr_command_waited as usize + 1;
                    for i in first_id..last_id {
                        complexcommand.vec_id_parse.push(i); // [32, 33]
                    }
                    env.last_event_id_visited = streamevent.id; //31
                }
            }
            _ => (),
        }
    }

    pub fn handle_complexe_parse(
        parse: &Parse,
        mut env: &mut ResMut<Env>,
        query_player_cell: &mut Query<(Entity, &Cell, &Player_Component)>,
        mut complexe_parse: &mut ResMut<ComplexeParse>,
        streamevent: &EventId<StreamEvent>,
        commands: &mut Commands,
        query_transform_res: &mut Query<&mut Transform>,
        query_action_player: &mut Query<&mut ActionPlayer>,
        asset_server: &Res<AssetServer>,
        materials: &mut ResMut<Assets<StandardMaterial>>,
        asset_meshes: &mut ResMut<Assets<Mesh>>,
    ) {
        if complexe_parse.nbr_parse_waited == 0 {
            panic!("something wrong w/ complex command (expulse)")
        }
        for id in complexe_parse.vec_id_parse.clone() {
            if streamevent.id == id {
                // On ne prend que les Parse avec l'id event qui nous interesse
                //println!("id push = {:?}", id);
                complexe_parse.vec_parse.push(parse.clone());
                env.last_event_id_visited = streamevent.id; //utile ?
            }
            if complexe_parse.nbr_parse_waited as usize == complexe_parse.vec_parse.len() {
                process_complexe_parse(
                    commands,
                    &mut env,
                    &mut complexe_parse,
                    query_action_player,
                    query_player_cell,
                    query_transform_res,
                    asset_server,
                    materials,
                    asset_meshes,
                );
            }
        }
    }

    pub fn process_complexe_parse(
        mut commands: &mut Commands,
        env: &mut ResMut<Env>,
        complexe_parse: &mut ResMut<ComplexeParse>,
        mut query_action_player: &mut Query<&mut ActionPlayer>,
        query_cell: &Query<(Entity, &Cell, &Player_Component)>,
        _: &mut Query<&mut Transform>,
        asset_server: &Res<AssetServer>,
        materials: &mut ResMut<Assets<StandardMaterial>>,
        asset_meshes: &mut ResMut<Assets<Mesh>>,
    ) {
        match complexe_parse.first_parse {
            Parse::Expulse(id_1) => {
                add_action(
                    &mut query_action_player,
                    &env.get_player_entity(&id_1),
                    TypeAction::Expulse,
                );
                for mov_expulse in &complexe_parse.vec_parse {
                    if let Parse::MovementPlayer(id, x, y, _) = mov_expulse {
                        let tmp_cell = query_cell_player(query_cell, env.get_player_entity(&id_1));
                        add_action(
                            &mut query_action_player,
                            &env.get_player_entity(id),
                            TypeAction::BeingExpulse {
                                0: *x,
                                1: *y,
                                2: tmp_cell.2,
                            },
                        );
                    } else {
                        panic!("on ne devrai qu'avoir des movement");
                    }
                }
            }
            // La seule difference entre Pose et Prend et le spawn ou despawn de la ressource (pour factoriser le tout)
            Parse::Prend(num_player, num_res) => {
                if let Parse::Inventaire(..) = complexe_parse.vec_parse[0] {
                    let new_inventory = Resources::from(&complexe_parse.vec_parse[0]);
                    env.set_inventory_player(&num_player, new_inventory);
                    if let Parse::ResourceCase(x, y, ..) = complexe_parse.vec_parse[1] {
                        let (x_rel, y_rel) = env.center_map_new_system(x as f32, y as f32);
                        let mut res = Resources::from(&complexe_parse.vec_parse[1]);
                        res.x_rel = x_rel;
                        res.y_rel = y_rel;
                        decrease_resources(&mut commands, &res, num_res, &mut env.id_resources);
                        add_action(
                            &mut query_action_player,
                            &env.get_player_entity(&num_player),
                            TypeAction::PrendPose,
                        );
                    }
                }
            }
            Parse::Pose(num_player, num_res) => {
                if let Parse::Inventaire(..) = complexe_parse.vec_parse[0] {
                    let new_inventory = Resources::from(&complexe_parse.vec_parse[0]);
                    env.set_inventory_player(&num_player, new_inventory);
                    if let Parse::ResourceCase(x, y, ..) = complexe_parse.vec_parse[1] {
                        ////
                        let (x_rel, y_rel) = env.center_map_new_system(x as f32, y as f32);
                        let mut res = Resources::from(&complexe_parse.vec_parse[0]);
                        res.x_rel = x_rel;
                        res.y_rel = y_rel;
                        increase_resources(
                            commands,
                            env,
                            asset_server,
                            materials,
                            asset_meshes,
                            &res,
                            num_res,
                        );
                        add_action(
                            &mut query_action_player,
                            &env.get_player_entity(&num_player),
                            TypeAction::PrendPose,
                        );
                    }
                }
            }
            Parse::EndIncantation(.., r) => {
                if let Some(current_inc) = env.incantation_in_progress.pop_front() {
                    commands.entity(current_inc.entity_spotlight).despawn();
                }
                for incantation in &complexe_parse.vec_parse {
                    match incantation {
                        Parse::Niveau(id, ..) => {
                            if r == 1 {
                                env.increase_level_player(id);
                            }
                            add_action(
                                query_action_player,
                                &env.get_player_entity(id),
                                TypeAction::EndInc,
                            );
                        }
                        Parse::ResourceCase(x, y, ..) => {
                            let (x_pixel, y_pixel) =
                                env.center_map_new_system(*x as f32, *y as f32);
                            let mut ressource = Resources::from(incantation);
                            ressource.x_rel = x_pixel;
                            ressource.y_rel = y_pixel;
                            spawn_resources(
                                commands,
                                &asset_server,
                                ressource,
                                &mut env.id_resources,
                                &0,
                                asset_meshes,
                                materials,
                            );
                        }

                        _ => panic!("process complexe command end inc error"),
                    }
                }
            }

            _ => panic!("you should not be here"),
        }
        complexe_parse.fflush();
    }
}
