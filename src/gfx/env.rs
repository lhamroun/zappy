pub mod env {
    use std::{
        cmp::max,
        collections::{HashMap, VecDeque},
    };

    use bevy::{
        animation::AnimationClip,
        asset::{AssetServer, Handle},
        ecs::{
            entity::Entity,
            system::{Res, ResMut, Resource},
            world::Mut,
        },
        math::{Vec2, Vec3},
        pbr::{PointLight, PointLightBundle},
        prelude::{Commands, Transform},
        render::color::Color,
        text::Text,
        utils::default,
    };

    use crate::{
        action::action::VEC_COLORS,
        map::map::Cell,
        resources::resource::{ContentCase, Resources},
        TILES_WIDTH,
    };

    #[derive(Debug)]
    pub enum Playable {
        Player(Player),
        Egg(Egg),
    }
    #[derive(Debug)]
    pub struct Egg {
        pub egg_entity: Entity,
        pub team: u8,
    }

    #[derive(Debug)]
    pub struct Player {
        pub id_game: u8,
        pub num_team: u8,
        pub name_team: String,
        pub level: u8,
        pub inventory: Option<Resources>,
        pub player_entity: Entity,
    }

    impl Player {
        pub fn new(id_game: u8, num_team: u8, level: &u8, team: String, entity: Entity) -> Self {
            Player {
                id_game,
                num_team,
                name_team: team,
                level: *level,
                inventory: None,
                player_entity: entity,
            }
        }
    }

    impl Egg {
        pub fn new(egg_entity: Entity, num_team: u8) -> Self {
            Egg {
                egg_entity,
                team: num_team,
            }
        }
    }

    #[derive(Debug)]
    pub struct Incantation {
        pub player_concern: Vec<u8>,
        pub vec_entity: Vec<Entity>,
        pub case: (u8, u8),
        pub entity_spotlight: Entity,
        pub num_incantation: u8,
        pub level: u8,
    }

    impl Incantation {
        pub fn new(
            commands: &mut Commands,
            player_concern: Vec<u8>,
            vec_entity: Vec<Entity>,
            case: (u8, u8),
            coor_pixel: (f32, f32),
            num_incantation: u8,
            level: u8,
            team: u8,
        ) -> Self {
            Incantation {
                player_concern,
                vec_entity,
                case,
                entity_spotlight: spawn_light_incantation(commands, coor_pixel, &team),
                num_incantation,
                level,
            }
        }
    }

    pub fn spawn_light_incantation(commands: &mut Commands, case: (f32, f32), team: &u8) -> Entity {
        commands
            .spawn(PointLightBundle {
                point_light: PointLight {
                    color: VEC_COLORS[*team as usize],
                    intensity: 500_000.0,
                    range: 10.0,
                    ..default()
                },
                transform: Transform::from_xyz(case.0, 10.0, case.1),
                ..default()
            })
            .id()
    }

    #[derive(Resource)]
    pub struct Env {
        pub x: u8,
        pub y: u8,
        pub pixel_x_max: f32,
        pub pixel_y_max: f32,
        pub pixel_x_min: f32,
        pub pixel_y_min: f32,
        pub time: u32,
        pub nbr_equipe: u8,
        pub name_equipe: Vec<String>,
        pub nbr_player_connected: u8,
        pub nbr_player_waited: u8,
        pub id_resources: Vec<Vec<HashMap<usize, ContentCase>>>,
        pub hash_playable: HashMap<u8, Playable>,
        pub vec_animation: Vec<Handle<AnimationClip>>,
        pub last_event_id_visited: usize,
        pub vec_tile_id: Vec<Entity>,
        pub coor_mouse: Vec3,
        pub end_connexion: bool,
        pub game_started: bool,
        pub game_finish: bool,
        pub base_color_of_ground: Color,
        pub incantation_in_progress: VecDeque<Incantation>,
        pub team_winner: (String, u8),
    }

    impl Default for Env {
        fn default() -> Self {
            Self {
                x: 0,
                y: 0,
                pixel_x_max: 0.,
                pixel_y_max: 0.,
                pixel_x_min: 0.,
                pixel_y_min: 0.,
                time: 0,
                id_resources: vec![],
                hash_playable: HashMap::new(),
                nbr_equipe: 0,
                nbr_player_connected: 0,
                nbr_player_waited: 0,
                name_equipe: vec![],
                last_event_id_visited: 0, // a tej
                vec_tile_id: vec![],
                coor_mouse: Vec3::ZERO,
                vec_animation: vec![],
                end_connexion: false,
                game_started: false,
                game_finish: false,
                base_color_of_ground: Color::WHITE,
                incantation_in_progress: VecDeque::new(),
                team_winner: ("".to_string(), 0),
            }
        }
    }

    impl Env {
        pub fn get_my_coor(&self) -> (u8, u8) {
            (self.x, self.y)
        }
        pub fn center_map_new_system(&self, x_old: f32, y_old: f32) -> (f32, f32) {
            let vec_trans = Vec3::new(TILES_WIDTH / 2., 1.0, TILES_WIDTH / 2.);
            let x_new = x_old * TILES_WIDTH + vec_trans.x;
            let y_new = y_old * TILES_WIDTH + vec_trans.z;
            (x_new, y_new)
        }

        pub fn set_x_y_pixel(&mut self, x_dim: u8, y_dim: u8) {
            let (x_pixel, y_pixel) = self.center_map_new_system((x_dim - 1) as f32, 0.);
            self.pixel_y_min = y_pixel - TILES_WIDTH / 2.;
            self.pixel_x_max = x_pixel + TILES_WIDTH / 2.;
            let (x_pixel, y_pixel) = self.center_map_new_system(0., (y_dim - 1) as f32);
            self.pixel_x_min = x_pixel - TILES_WIDTH / 2.;
            self.pixel_y_max = y_pixel + TILES_WIDTH / 2.;
        }

        pub fn set_hashmap_resources(&mut self, x_dim: u8, y_dim: u8) {
            let x_size: usize;
            let y_size: usize;
            if x_dim > 0 {
                x_size = (x_dim - 1) as usize;
            } else {
                x_size = x_dim as usize;
            }
            if y_dim > 0 {
                y_size = (y_dim - 1) as usize;
            } else {
                y_size = y_dim as usize;
            }
            for y in 0..y_size + 1 {
                self.id_resources.push(vec![]);
                for _ in 0..x_size + 1 {
                    self.id_resources[y].push(HashMap::new());
                }
            }
        }

        pub fn get_all_entity_of_a_team(&self, id_game: &u8, num_team: &u8) -> Vec<Entity> {
            let all_player: Vec<&Playable> = self.hash_playable.values().collect();
            let mut other_player_id_same_team: Vec<Entity> = vec![];
            for playable in &all_player {
                match playable {
                    Playable::Player(player) => {
                        if player.id_game != *id_game && player.num_team == *num_team {
                            other_player_id_same_team.push(player.player_entity)
                        }
                    }
                    _ => (),
                }
            }
            println!("{:#?}", all_player);
            println!("{:#?}", other_player_id_same_team);
            other_player_id_same_team
        }

        pub fn get_player_entity(&self, id_game: &u8) -> Entity {
            let playable_opt = self.hash_playable.get(&id_game);
            let entity;
            if let Some(playable) = playable_opt {
                entity = match playable {
                    Playable::Player(player) => player.player_entity,
                    Playable::Egg(egg) => egg.egg_entity,
                }
            } else {
                panic!("Playable doesn't exist?"); // explicit panic si chelsea est premier player et arsenal second ??
            }
            entity
        }

        pub fn get_player_num_team(&self, id_game: &u8) -> u8 {
            let playable_opt = self.hash_playable.get(&id_game);
            //println!("{:?}", playable_opt);
            let num_team;
            if let Some(playable) = playable_opt {
                num_team = match playable {
                    Playable::Player(player) => player.num_team,
                    Playable::Egg(_) => panic!("Confusion with egg ?"), // for now
                }
            } else {
                panic!("Playable doesn't exist?");
            }
            num_team
        }

        pub fn get_num_team_from_name(&self, name_team: &String) -> Option<u8> {
            let iter = self.name_equipe.iter().enumerate();
            for (nbr_iter, name) in iter {
                //println!("name_team {} name {} nbr_iter {} ", name_team, name, nbr_iter);
                if name.eq(name_team) {
                    return Some(nbr_iter as u8);
                }
            }
            None
        }

        pub fn increase_level_player(&mut self, id_game: &u8) {
            let playable_opt = self.hash_playable.get_mut(&id_game);
            if let Some(playable) = playable_opt {
                match playable {
                    Playable::Player(ref mut player) => {
                        player.level += 1;

                        println!("LVL UP {:?} {:?}", player.id_game, player.level);
                        if player.level == 8 {
                            self.game_finish = true;
                            self.team_winner.0 = player.name_team.clone();
                            self.team_winner.1 = player.num_team;
                        }
                    }
                    Playable::Egg(_) => panic!("Confusion increase_level with egg ?"), // for now
                }
            } else {
                panic!("Playable doesn't exist?");
            }
        }

        pub fn set_inventory_player(&mut self, id_game: &u8, res: Resources) {
            let playable_opt = self.hash_playable.get_mut(&id_game);
            //println!("{:?}", playable_opt);
            if let Some(playable) = playable_opt {
                match playable {
                    Playable::Player(ref mut player) => {
                        player.inventory = Some(res);
                    }
                    Playable::Egg(_) => panic!("Confusion inventory with egg ?"), // for now
                }
            } else {
                panic!("Playable doesn't exist?");
            }
        }

        pub fn remove_player(&mut self, id_game: &u8)
        {
            let playable_opt = self.hash_playable.get(&id_game);
            if let Some(playable) = playable_opt {
                match playable {
                    Playable::Player(_) => {
                        self.hash_playable.remove(&id_game).expect("cannot remove player of hasmap ?");
                            
                    }
                    Playable::Egg(_) => panic!("Confusion inventory with egg ?"), // for now
                }
            } else {
                panic!("Playable doesn't exist?");
            }
        }

        // pub fn set_incantation_player(&mut self, player_concern: Vec<u8>, id_game: &u8, vec_entity: Vec<Entity>, case: (u8, u8), num_inc: u8, level: u8)
        // {
        //     let playable_opt = self.player_id.get_mut(&id_game);
        //     if let Some(playable) = playable_opt{
        //         match playable{
        //             Playable::Player(ref mut player) => {
        //                 player.incantation = Some(Incantation::new(player_concern, vec_entity, case, true, num_inc, level));
        //             }
        //             Playable::Egg(_) => panic!("Confusion incantation with egg ?"), // for now
        //         }
        //     }
        //     else{
        //         panic!("Playable doesn't exist?");
        //     }
        // }

        //
        // cette methode est appelee a chaque Parse::NomEquipe
        // elle load toutes les images qui permettront de set les entitees des players et eggs
        pub fn load_all_animations(&mut self, asset_server: &Res<AssetServer>) //(nbr_teams)
        {
            //Dance
            self.vec_animation
                .push(asset_server.load("3d/Fox.glb#Animation0"));
            //Death
            self.vec_animation
                .push(asset_server.load("3d/Fox.glb#Animation1"));
            //Hello
            self.vec_animation
                .push(asset_server.load("3d/Fox.glb#Animation2"));
            //Hit_Recieve_1
            self.vec_animation
                .push(asset_server.load("3d/Fox.glb#Animation3"));
            //Hit_Recieve_2
            self.vec_animation
                .push(asset_server.load("3d/Fox.glb#Animation4"));
            //Idle
            self.vec_animation
                .push(asset_server.load("3d/Fox.glb#Animation5"));
            //Jump
            self.vec_animation
                .push(asset_server.load("3d/Fox.glb#Animation6"));
            //Jump_landing
            self.vec_animation
                .push(asset_server.load("3d/Fox.glb#Animation7"));
            //Jump_NoHeight
            self.vec_animation
                .push(asset_server.load("3d/Fox.glb#Animation8"));
            //Kick
            self.vec_animation
                .push(asset_server.load("3d/Fox.glb#Animation9"));
            //No
            self.vec_animation
                .push(asset_server.load("3d/Fox.glb#Animation10"));
            //PickUp
            self.vec_animation
                .push(asset_server.load("3d/Fox.glb#Animation11"));
            //Run
            self.vec_animation
                .push(asset_server.load("3d/Fox.glb#Animation12"));
            //Shoot_Big
            self.vec_animation
                .push(asset_server.load("3d/Fox.glb#Animation13"));
            //Shoot_Small
            self.vec_animation
                .push(asset_server.load("3d/Fox.glb#Animation14"));
            //Walk
            self.vec_animation
                .push(asset_server.load("3d/Fox.glb#Animation15"));
            //Yes
            self.vec_animation
                .push(asset_server.load("3d/Fox.glb#Animation16"));
            //asset_server.load("Fox.glb#Animation2"),
            // asset_server.load("Fox.glb#Animation1"),
            // asset_server.load("Fox.glb#Animation0"),
            // //println!("{:?}", self.vec_sprite_player_mvmt[num_team].len());
        }

        pub fn set_new_entry_hashmap_player(
            &mut self,
            id: &u8,
            level: &u8,
            team: String,
            num_team: u8,
            entity: Entity,
        ) {
            let player = Player::new(*id, num_team, level, team, entity);
            let playable = Playable::Player(player);
            //println!("add Player {:?}", playable);
            self.hash_playable.insert(*id, playable);
            // let player = Playable::Player(())
        }

        pub fn set_new_entry_hashmap_egg(&mut self, id: &u8, entity: Entity, num_team: u8) {
            // let player = Player::new(*id, num_team, level, team, entity);
            // let playable = Playable::Player(player);
            let egg = Egg::new(entity, num_team);
            let playable = Playable::Egg(egg);
            //println!("add Player {:?}", playable);
            self.hash_playable.insert(*id, playable);
            // let player = Playable::Player(())
        }
        //Print info player in button.rs

        pub fn print_info_playable(&self, playerid: u8) {
            //println!("player -> {}", format_option_player(self.hash_playable.get(&playerid)));
        }

        pub fn get_info_monitoring(&self) -> Vec<(&String, i32, u8)> // add struct info_monitoring in env ?
        {
            let mut vec_info = vec![];
            for team in &self.name_equipe {
                let mut lvl_max = 0;
                let mut player_count = 0;
                for player in &self.hash_playable {
                    match player.1 {
                        Playable::Player(player) => {
                            if player.name_team == *team {
                                player_count += 1;
                                lvl_max = max(lvl_max, player.level);
                            }
                        }
                        Playable::Egg(egg) => (),
                    }
                }
                vec_info.push((team, player_count, lvl_max));
            }
            vec_info
        }

        pub fn print_ressources_case(&self, x: &usize, y: &usize) -> String {
            format!(
                "{} {:?}\n{} {:?}\n{} {:?}\n{} {:?}\n{} {:?}\n{} {:?}",
                "food",
                self.id_resources[*x][*y].get(&0),
                "Linemate",
                self.id_resources[*x][*y].get(&1),
                "Deraumère ",
                self.id_resources[*x][*y].get(&2),
                "Sibur ",
                self.id_resources[*x][*y].get(&3),
                "Mendiane ",
                self.id_resources[*x][*y].get(&4),
                "Thystame ",
                self.id_resources[*x][*y].get(&5)
            )
            // "{} {:?}","Linemate",self.id_Ressource[*x][*y].get(&1),
            // "{} {:?}","Deraumère ",self.id_Ressource[*x][*y].get(&2),
            // "{} {:?}","Sibur ",self.id_Ressource[*x][*y].get(&3),
            // "{} {:?}","Mendiane ",self.id_Ressource[*x][*y].get(&4),
            // "{} {:?}","Thystame ",self.id_Ressource[*x][*y].get(&5))
        }

        pub fn get_nbr_ressource_string(&self, x: &usize, y: &usize, indice: usize) -> String {
            format!(
                "{}",
                format_option_ressource(self.id_resources[*y][*x].get(&indice))
            )
        }

        // get all the component with id and orientation
    }

    pub fn format_option_player(option: Option<&Playable>) -> String {
        match option {
            Some(player) => format!("{:?}\n", player),
            None => format!("0\n"),
        }
    }

    pub fn format_option_ressource(option: Option<&ContentCase>) -> String {
        match option {
            Some(content_case) => format!("{}\n", content_case.nbr_resource),
            None => format!("0\n"),
        }
    }

    pub fn format_player_info(
        output: &mut Mut<'_, Text>,
        env: &ResMut<'_, Env>,
        player_id: &u8,
        cell: &Cell,
    ) // miss id
    {
        match env.hash_playable.get(&player_id) {
            Some(Player) => {
                match Player {
                    Playable::Player(player) => {
                        /*  pub id_game : u8,
                        pub num_team : u8,
                        pub name_team: String,
                        pub level: u8,
                        pub inventory: Option<Ressource>,
                        pub player_entity: Entity, */
                        output.sections[2].value = format!("{}\n", player.name_team);
                        output.sections[4].value = format!("{}\n", player.id_game); // panic avec la window monitoring ??? 
                        output.sections[6].value = format!("{} {} {}\n", cell.0, cell.1, cell.2);
                        output.sections[8].value = format!("{}\n", player.level);
                        output.sections[10].value = format!("{:?}\n", player.inventory);
                        // output.sections[9].value = format!("{}", player.name_team);
                    }
                    Playable::Egg(egg) => {
                        // output.sections.clear();
                        output.sections[2].value = format!("{}\n", egg.team);
                        output.sections[4].value = format!("{:?}\n", player_id);
                        output.sections[6].value = format!("{} {}\n", cell.0, cell.1);
                        // output.sections[7].value.clear();
                        // output.sections[9].value.clear();
                    }
                }
            }
            None => todo!(),
        }
    }

    pub fn format_all_ressource_button(
        output: &mut Mut<'_, Text>,
        asset_map: &ResMut<'_, Env>,
        coor: &Vec2,
    ) {
        output.sections[0].value = format!("(x :{},", coor.x);
        output.sections[1].value = format!("y: {})", coor.y);
        output.sections[3].value = format!(
            "{}",
            asset_map.get_nbr_ressource_string(&(coor.x as usize), &(coor.y as usize), 0 as usize)
        );
        output.sections[5].value = format!(
            "{}",
            asset_map.get_nbr_ressource_string(&(coor.x as usize), &(coor.y as usize), 1 as usize)
        );
        output.sections[7].value = format!(
            "{}",
            asset_map.get_nbr_ressource_string(&(coor.x as usize), &(coor.y as usize), 2 as usize)
        );
        output.sections[9].value = format!(
            "{}",
            asset_map.get_nbr_ressource_string(&(coor.x as usize), &(coor.y as usize), 3 as usize)
        );
        output.sections[11].value = format!(
            "{}",
            asset_map.get_nbr_ressource_string(&(coor.x as usize), &(coor.y as usize), 4 as usize)
        );
        output.sections[13].value = format!(
            "{}",
            asset_map.get_nbr_ressource_string(&(coor.x as usize), &(coor.y as usize), 5 as usize)
        );
        output.sections[15].value = format!(
            "{}",
            asset_map.get_nbr_ressource_string(&(coor.x as usize), &(coor.y as usize), 6 as usize)
        );
    }
}
