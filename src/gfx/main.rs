/*
    Welcome to the server_gfx part of our zappy
    We're using the motor Bevy to design the graphical representation of the our game.
    This choice has been motivated to keep the langage Rust to implement our graphics, which implies
    a deep understanding of the design patten ECS (for ENTITY-COMPONENT-SYSTEM) //Startup Update

    The thing you need to know here about this paradigm, is that we have needed to split our data
    depending on how the game will have access to them, and need to be represented or not.

    If a bunch of data needs to be represented as an individual thing, on screen, it will be an ENTITY.
    Entities are made up of one of more COMPONENTS.

    Components are structure made of data. They are use for describe entities and are access point to modify them.

    You can spawn and interact with entities using QUERIES, whose purpose is to iterate on entites in order to be able to
    modify the data constituting their components.

    You can find a good exemple to understand this : example/tuto_bevy_ecs.rs

    A video game is a big infinite for-loop. The way Bevy help you to manage the subroutines of this for loop are called SYSTEM.


*/

pub mod action;
pub mod button;
pub mod camera;
pub mod dispatch;
pub mod env;
pub mod map;
pub mod monitoring;
pub mod parser;
pub mod player_entities;
pub mod resources;

use bevy::asset::AssetLoader;
use bevy::audio::{AudioLoader, Volume};
use bevy::diagnostic::FrameTimeDiagnosticsPlugin;
use bevy::ecs::schedule::ScheduleLabel;
use bevy::prelude::*;
use bevy_inspector_egui::quick::WorldInspectorPlugin;
use bevy_panorbit_camera::PanOrbitCameraPlugin;
use button::craftbutton::{Craftbutton, SIZE_FONT};
use camera::camera::spawn_camera;
use crossbeam_channel::bounded;
use dispatch::dispatch::Dispatch;
use monitoring::monitoring::Monitoring;
use player_entities::player_entities::Action;
use std::io::Read;
use std::net::{TcpListener, TcpStream};
use std::thread;

// Using crossbeam_channel instead of std as std `Receiver` is `!Sync`
use crossbeam_channel::Receiver;

use crate::parser::parser::{copy_until_char, parser_server_packet, Parse};

const TILES_WIDTH: f32 = 20.0;

#[derive(Resource)]
pub struct ResourceStream {
    listener: TcpListener,
    stream_clone: Option<TcpStream>,
}

#[derive(ScheduleLabel, Debug, Clone, PartialEq, Eq, Hash)]
struct PrepareUpdate;

fn main() {
    // Create a Listener on port 8080
    let listener = TcpListener::bind("127.0.0.1:8080").expect("Failed to bind to address");

    let mut app = App::new();

    // start the window
    app.add_plugins((
        DefaultPlugins.set(WindowPlugin {
            primary_window: Some(Window {
                title: "zappy_42".into(),
                resolution: (1000., 1000.).into(),
                visible: true,
                ..default()
            }),
            ..default()
        }),
        FrameTimeDiagnosticsPlugin,
    ))
    .add_plugins(PanOrbitCameraPlugin)
    // Plugin debug Bevy
    //.add_plugins(WorldInspectorPlugin::new())
    // add Resource communication with server
    .insert_resource(ResourceStream {
        listener,
        stream_clone: None,
    })
    // add communication inter thread (useless ?)
    /*
        Add system intend to handle connexion and data exchanges with server
        This is an isolated thread, so we use a (tx, rx) = Bounded::<Parse>
        to communicate with the rest of the program
        With Parse:
        */
    .add_systems(Startup, setup_loading_screen)
    .add_systems(Startup, rcv_from_server) // Thread exterieur a l'app qui ecoute
    // .add_systems(Update, lolo_fn)
    .add_systems(Startup, setup_music)
    .add_plugins(Dispatch)
    .add_plugins(Action)
    .add_plugins(Craftbutton)
    .add_plugins(Monitoring)
    .run();
}

#[derive(Component)]
pub struct LoadingScreen;

#[derive(Component)]
pub struct LoadingScreenText;

#[derive(Resource)]
pub struct LoadingResource {
    pub loading_entity: Entity,
}

fn setup_loading_screen(mut commands: Commands, asset_server: Res<AssetServer>) {
    spawn_camera(&mut commands);
    let font = asset_server.load("fonts/FiraMono-Regular.ttf");

    // Spawns the UI that will show the user prompts.
    let text_style = TextStyle {
        font,
        font_size: 32.0,
        color: Color::WHITE,
    };
    let text_sections = vec![TextSection::new("LOADING...\n", text_style.clone())];

    // Spawn the UI that will make up the loading screen.
    let pannel_id = commands
        .spawn((
            NodeBundle {
                style: Style {
                    height: Val::Percent(100.0),
                    width: Val::Percent(100.0),
                    justify_content: JustifyContent::Center,
                    align_items: AlignItems::Center,
                    ..default()
                },
                background_color: BackgroundColor(Color::BLACK),
                visibility: Visibility::Visible,
                ..default()
            },
            LoadingScreen,
        ))
        .with_children(|parent| {
            parent.spawn((TextBundle::from_sections(text_sections), LoadingScreenText));
        })
        .id();
    commands.insert_resource(LoadingResource {
        loading_entity: pannel_id,
    });
}

pub fn setup_music(asset_server: Res<AssetServer>, mut commands: Commands) {
    commands.spawn((
        AudioBundle {
            source: asset_server.load("ghosts_n_goblins.mp3"),
            settings: PlaybackSettings::LOOP.with_volume(Volume::new_absolute(0.8)),
            ..default()
        },
        MyMusic,
    ));
}

#[derive(Component)]
struct MyMusic;

// Leftovers because we will probably use guizmo for broadcast and see ?
pub fn lolo_fn(mut guizmo: Gizmos) {
    guizmo.line(
        Vec3::new(0., 12., 0.),
        Vec3::new(150., 12., 150.),
        Color::RED,
    );
    // guizmo.line_2d(Vec2::ZERO, Vec2::new(0., 150.), Color::RED);
}

#[derive(Resource, Deref)]
struct StreamReceiver(Receiver<Parse>);

#[derive(Event, Debug)]
pub struct StreamEvent(Parse);
#[derive(Resource, Debug)]
pub struct Txrx2 {
    pub channel: (crossbeam_channel::Sender<bool>, Receiver<bool>),
}

fn rcv_from_server(mut state: ResMut<ResourceStream>, mut command: Commands) {
    // We're listening the data from the server
    // state.listener.set_nonblocking(true);
    for stream in state.listener.incoming() {
        match stream {
            Ok(mut stream) => {
                /*
                    Spawn a channel who links the thread who treats incoming packets
                    and the main thread.
                */
                let (tx, rx) = bounded::<Parse>(1);

                // clone and give a copy of the stream to the thread parent
                if let Ok(stream_clone) = stream.try_clone() {
                    state.stream_clone = Some(stream_clone);
                }

                // Spawn a new thread to handle each incoming packets
                thread::spawn(move || {
                    let mut buffer = [0; 32];
                    loop {
                        match stream.read(&mut buffer) {
                            Ok(n) if n == 0 => {
                                // Connection closed by the server
                                println!("----------------------");
                                println!("--- end connection ---");
                                println!("----------------------");
                                std::process::exit(0);
                            }
                            Ok(n) => {
                                // Process the received data
                                let received_data = &buffer[..n];
                                let str = copy_until_char(received_data, b'\n');

                                // turn row data into Parse variant
                                let parse: Parse = parser_server_packet(str);

                                // send Parse to the thread parent
                                if parse != Parse::Donothing {
                                    // println!("{:?}", parse);
                                    tx.send(parse).unwrap();
                                }
                            }
                            Err(e) => {
                                // Handle errors
                                eprintln!("Error reading from stream: {}", e);
                                break;
                            }
                        }
                    }
                });
                // (Components/Entities vs Resource vs Event)
                // Turn Parse variant into Receiver<Parse> to be treated in dispatch
                command.insert_resource(StreamReceiver(rx));
                break;
            }
            Err(e) => {
                // Handle errors
                eprintln!("Error accepting connection: {}", e);
            }
        }
    }
}
