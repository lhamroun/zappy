pub mod map {

    use bevy::prelude::*;

    use crate::env::env::Env;

    #[derive(Component, Debug, PartialEq)]
    pub struct Cell(pub u8, pub u8, pub u8);

    #[derive(Component, Debug, Clone)]
    pub struct Position(pub Vec3);

    #[derive(Component, Clone)]
    pub struct Tile; //vouer a disparaitre (a remplacer par Cell)

    #[derive(Component, Debug, Clone)]
    pub struct Coor(pub Vec2);

    #[derive(Bundle, Clone)]
    pub struct TileBundle {
        // pub sprite_sheet_bundle: SpriteSheetBundle,
        pub tile: Tile,
        pub position: Position,
        pub interact: Interaction,
        pub coor: Coor,
    }

    impl TileBundle {
        pub fn new(vec: &Vec3, vec_coor: &Vec2) -> Self {
            TileBundle {
                tile: Tile,
                position: Position(*vec),
                interact: Interaction::None,
                coor: Coor(*vec_coor),
            }
        }
    }

    pub fn spawn_map(
        x: u8,
        y: u8,
        commands: &mut Commands,
        asset_server: &Res<AssetServer>,
        env: &mut ResMut<Env>,
    ) {
        let start_x = 0;
        let mut end_x = x;
        let start_y = 0;
        let mut end_y = y;
        let mut i = 0;

        env.x = x;
        env.y = y;
        if x > 0 {
            end_x = x - 1;
        }
        if y > 0 {
            end_y = y - 1;
        }
        for y_iter in start_y..=end_y {
            for x_iter in start_x..=end_x {
                let vec_coor = Vec2::new(x_iter as f32, y_iter as f32);
                let (x_rel, y_rel) = env.center_map_new_system(x_iter as f32, y_iter as f32);
                let vec = Vec3 {
                    x: x_rel,
                    y: 1.,
                    z: y_rel,
                };
                let entity_tile = commands
                    .spawn((
                        SceneBundle {
                            scene: asset_server.load("3d/ground.glb#Scene0"),
                            transform: Transform::from_translation(vec),
                            ..default()
                        },
                        Name::new(format!("ground {}", i)),
                        TileBundle::new(&vec, &vec_coor),
                    ))
                    .id();
                i += 1;
                env.vec_tile_id.push(entity_tile);
            }
        }
    }
}
