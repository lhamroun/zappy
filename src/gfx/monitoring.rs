pub mod monitoring {
    use bevy::{
        app::{App, Plugin, Startup, Update},
        asset::{AssetServer, Assets, Handle},
        input::Input,
        math::{Quat, Vec2, Vec3},
        pbr::StandardMaterial,
        prelude::{
            BuildChildren, Camera3dBundle, Color, Commands, Component, Entity, IntoSystemConfigs, KeyCode, Mut, NodeBundle, Parent, Query, Res, ResMut, TextBundle, Transform, ViewVisibility, Visibility, With, Without
        },
        text::{Text, TextSection, TextStyle},
        ui::{AlignItems, BackgroundColor, JustifyContent, PositionType, Style, Val},
        utils::default,
        window::{PrimaryWindow, Window},
    };
    use bevy_panorbit_camera::PanOrbitCamera;

    use crate::{
        action::action::VEC_COLORS, button::craftbutton::{
            change_alpha_material, Case_Box, Case_Text, Egg_Box, Player_Text, SIZE_FONT,
        }, camera::camera::spawn_camera, dispatch::dispatch::condition_start_game, env::env::{format_all_ressource_button, Env, Playable}, map::map::{Cell, Coor, Position, Tile}, player_entities::player_entities::{Player, PlayerId}, TILES_WIDTH
    };
    #[derive(Component)]
    pub struct WindowMonitoring;
    pub struct Monitoring;

    impl Plugin for Monitoring {
        fn build(&self, app: &mut App) {
            app.add_systems(Startup, setup_monitoring_info_player)
                //     // .add_systems(Startup, setup_box_info_player)
                //     // .add_systems(Startup, setup_fps)
                //     // .add_systems(Startup, setup_box_info_player)
                //     // .add_systems(Startup,setup_box_info_egg)
                //     // .add_systems(Update, cursor_to_world_coordinates)
                //     // .add_systems(Update, draw_cursor)
                .add_systems(Update, update_info_button_player);
            // .add_systems(Update, text_update_system_fps)
            // .add_systems(Update, update_info_button_player);
            // .add_systems(Update, update_info_button_player)
            // .add_systems(Update, update_info_button_egg); text_update_system_fps
        }
    }

    fn setup_monitoring_info_player(mut commands: Commands, asset_server: Res<AssetServer>) {
        // spawn_camera(&mut commands);

        // let mut spec_camera = Transform::from_translation(Vec3::new(63., 195., 129.846));
        // spec_camera.rotation = Quat::from_xyzw(-1., 0., 0., 0.);
        // commands.spawn((
        //     Camera3dBundle {
        //         transform: spec_camera,
        //         ..default()
        //     },
        //     PanOrbitCamera::default(),
        // ));
        let font = asset_server.load("fonts/FiraMono-Regular.ttf");

        let text_style = TextStyle {
            font,
            font_size: SIZE_FONT,
            color: Color::WHITE,
        };
        // let box_size = Vec2::new(300.0, 200.0);
        // let box_position = Vec2::new(0.0, -250.0);

        let test_vec = Vec3::new(-27., 27., 0.);
        let other_box_size = Vec2::new(100.0, 100.0);
        let other_box_position = Vec2::new(120.0, -150.0);

        // Create a Resource with this Vector ?

        let mut text_sections = vec![
            TextSection::new("________________________ TEAMS ________________________\n", text_style.clone()),
            TextSection::new(
                format!("{:^12}    |   {:^12}    |     {:^12}\n{}\n", "name", "nb players", "lvl max", "-------------------------------------------------------"),
                text_style.clone(),
            ),
            TextSection::new(
                "Team info",
                TextStyle {
                    color: Color::RED,
                    ..text_style.clone()
                },
            ),
            // TextSection::new("Nb_player: ", text_style.clone()),
            // TextSection::new("Nb_player_value \n", text_style.clone()),
            // TextSection::new("lvl_max: ", text_style.clone()),
            // TextSection::new("lvl number \n", text_style.clone()),
        ];

        // let mut vec_ref = vec![TextSection::new("Team: ", text_style.clone()), TextSection::new("String Team Name\n", text_style.clone()), TextSection::new("Nb_player: ", text_style.clone()), TextSection::new("Nb_player_value \n", text_style.clone()), TextSection::new("lvl_max: ", text_style.clone()), TextSection::new("lvl number \n", text_style.clone())];
        // for _ in 0..env.nbr_equipe
        // {
        // for text_section in &vec_ref{
        // vec_text_section.push(text_section.clone());
        // }
        // }
        // vec_text_section.push()
        commands
            .spawn(NodeBundle {
                style: Style {
                    width: Val::Percent(40.),
                    height: Val::Percent(10.),
                    position_type: PositionType::Absolute,
                    bottom: Val::Px(15.),
                    left: Val::Px(15.),
                    justify_content: JustifyContent::Default,
                    align_items: AlignItems::Default,
                    ..default()
                },
                visibility: Visibility::Visible,
                background_color: BackgroundColor(Color::rgba(0.182, 0.59, 0.71, 0.50)), //182, 59, 71
                ..default()
            })
            .with_children(|parent| {
                parent.spawn((TextBundle::from_sections(text_sections), WindowMonitoring));
            });
    }

    /***
       Teamname    |   nb players    |     lvl max        *
        --------------------------------------------------*
           lala    |       3         |         1          *
           toto    |       6         |         6          *
    ***/

    pub fn format_player_info(
        asset_server: &Res<AssetServer>,
        output: &mut Mut<'_, Text>,
        env: &ResMut<'_, Env>,
        player_id: &u8,
        cell: &Cell,
    ) // miss id
    {
        let font = asset_server.load("fonts/FiraMono-Regular.ttf");

        let infos_player_team = env.get_info_monitoring();
        if infos_player_team.len() >= output.sections.len() - 1 {
            let text_style = TextStyle {
                font,
                font_size: SIZE_FONT,
                color: VEC_COLORS[output.sections.len() - 2],
            };
            output
                .sections
                .push(TextSection::new("Team info", text_style.clone()))
        }
        for (index, info_player) in infos_player_team.iter().enumerate() {
            if index + 2 < output.sections.len() {
                output.sections[index + 2].value = format!(
                    "{:^12}    |   {:^12}    |     {:^12}\n",
                    info_player.0, info_player.1, info_player.2
                );
            }
            // output.sections[3].value = format!("{}\n", info_player.1);
            // output.sections[5].value = format!("{}\n", info_player.2);
        }
    }

    fn update_info_button_player(
        asset_server: Res<AssetServer>,
        mut env: ResMut<Env>,
        mut query_players_pos: Query<
            (&mut Transform, &PlayerId, &Cell, Entity),
            (With<Player>, Without<Case_Box>, Without<Player_Text>),
        >,
        mut query_player_text: Query<
            (&mut Text, &mut Visibility),
            (With<WindowMonitoring>, Without<Egg_Box>),
        >,
    ) {
        // if keys.pressed(KeyCode::ControlLeft) {
        // let mut dist_min: f32 = 1000.0;
        // let dist = asset_map.coor_mouse;
        for (pos, playerid, cell, entity) in &mut query_players_pos {
            for (mut output, mut visibility) in &mut query_player_text {
                format_player_info(&asset_server, &mut output, &env, &playerid.0, cell);
                // *visibility = Visibility::Visible;
            }
        }
    }
}
