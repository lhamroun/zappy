pub mod parser {
    #[derive(Clone, Debug, PartialEq)]
    pub enum Parse {
        Map(u8, u8),                                        //"msz X Y\n"
        ResourceCase(u32, u32, u8, u8, u8, u8, u8, u8, u8), //"bct X Y q q q q q q q\n" * nbr_cases
        Time(u32),
        NomEquipe(String),
        ConnexionPlayer(u8, u8, u8, u8, u8, String), //"pnw #n X Y O L N\n"
        Niveau(u8, u8),                              // "plv #n L\n"
        MovementPlayer(u8, u8, u8, u8),              // "ppo #n X Y O\n"
        Inventaire(u8, u32, u32, u8, u8, u8, u8, u8, u8, u8),
        Expulse(u8),
        Broadcast(u8, String), // pbc #n M
        //Resources Action
        Prend(u8, u8),
        Pose(u8, u8),
        //EGG
        Fork(u8),                //pfk #n\n
        EndFork(u8, u8, u8, u8), // "enw #e #n X Y\n"
        EggHatch(u8),            // "eht #e\n"

        Death(u8),
        //incantation
        Incantation(u8, u8, u8, Vec<u8>), // "pic X Y L #n #n …\n"
        EndIncantation(u8, u8, u8),       // "pie X Y R #n #n …\n"
        Donothing,
    }

    // pub enum TypeParse{
    //     Simple(Parse)
    //     Stacking(Ex)
    // }
    pub fn is_parse_simple(parse: &Parse) -> bool {
        match parse {
            Parse::Expulse(_)
            | Parse::Prend(_, _)
            | Parse::Pose(_, _)
            | Parse::EndIncantation(_, _, _) => false,
            Parse::Donothing => false,
            _ => true,
        }
    }

    pub fn copy_until_char(buffer: &[u8], char: u8) -> String {
        let string_dst = buffer
            .iter() // into_iter
            .take_while(|&x| *x != char)
            .map(|x| *x as char)
            .collect();
        string_dst
    }

    pub fn parse_into_integer(content: String) -> Vec<i32> {
        //println!("{}", content);
        let mut iter = content.split_ascii_whitespace().skip(1);
        // //println!("{:?}", iter);
        let vec: Vec<i32> = iter.map(|x| x.parse::<i32>().ok().unwrap()).collect();
        vec
    }

    pub fn parse_teams(content: String) -> Parse {
        let nom_equipe: Vec<String> = content
            .split_ascii_whitespace()
            .skip(1)
            .map(|str| str.to_string())
            .collect();

        if nom_equipe.len() > 1 {
            panic!("packet equip wrong");
        } else {
            return Parse::NomEquipe(nom_equipe[0].clone());
        }
    }

    pub fn parse_resource(content: String) -> Parse {
        let vec_parsing = parse_into_integer(content);

        let res = Parse::ResourceCase(
            vec_parsing[0] as u32,
            vec_parsing[1] as u32,
            vec_parsing[2] as u8,
            vec_parsing[3] as u8,
            vec_parsing[4] as u8,
            vec_parsing[5] as u8,
            vec_parsing[6] as u8,
            vec_parsing[7] as u8,
            vec_parsing[8] as u8,
        );
        res
    }

    pub fn parse_player_movement(content: String) -> Parse {
        let vec_parsing = parse_into_integer(content);
        let res = Parse::MovementPlayer(
            vec_parsing[0] as u8,
            vec_parsing[1] as u8,
            vec_parsing[2] as u8,
            vec_parsing[3] as u8,
        );
        res
    }

    pub fn parse_connexion_player(content: String) -> Parse {
        let mut vec_parsing_u8: Vec<u8> = vec![];
        let mut team: String = format!("");
        for i in content.split_ascii_whitespace().skip(1).enumerate()
        //pnw #n X Y O L arsenal\n"
        {
            if i.0 < 5 {
                // //println!("{:?}", i.1);
                vec_parsing_u8.push(i.1.parse::<u8>().ok().unwrap());
            } else {
                team = i.1.to_string().clone();
            }
        }
        Parse::ConnexionPlayer(
            vec_parsing_u8[0],
            vec_parsing_u8[1],
            vec_parsing_u8[2],
            vec_parsing_u8[3],
            vec_parsing_u8[4],
            team,
        )
    }
    pub fn parse_time(content: String) -> Parse {
        let iter = content.split_ascii_whitespace().skip(1);
        let time = iter.map(|str| str.parse::<u32>()).next().unwrap().unwrap();

        Parse::Time(time)
    }

    pub fn parse_fork(content: String) -> Parse {
        let iter = content.split_ascii_whitespace().skip(1);
        let id = iter.map(|str| str.parse::<u8>()).next().unwrap().unwrap();

        Parse::Fork(id)
    }

    pub fn parse_endfork(content: String) -> Parse {
        let vec_parsing = parse_into_integer(content);
        Parse::EndFork(
            vec_parsing[0] as u8,
            vec_parsing[1] as u8,
            vec_parsing[2] as u8,
            vec_parsing[3] as u8,
        )
    }

    pub fn parse_endinc(content: String) -> Parse {
        let vec_parsing = parse_into_integer(content);
        Parse::EndIncantation(
            vec_parsing[0] as u8,
            vec_parsing[1] as u8,
            vec_parsing[2] as u8,
        )
    }

    pub fn parse_hatching(content: String) -> Parse {
        let iter = content.split_ascii_whitespace().skip(1);
        let id = iter.map(|str| str.parse::<u8>()).next().unwrap().unwrap();

        Parse::EggHatch(id)
    }

    pub fn parse_expulse(content: String) -> Parse {
        let iter = content.split_ascii_whitespace().skip(1);
        let id = iter.map(|str| str.parse::<u8>()).next().unwrap().unwrap();

        Parse::Expulse(id)
    }

    pub fn parse_death(content: String) -> Parse {
        let iter = content.split_ascii_whitespace().skip(1);
        let id = iter.map(|str| str.parse::<u8>()).next().unwrap().unwrap();

        Parse::Death(id)
    }

    pub fn parse_take_ressouce(content: String) -> Parse {
        let iter = content.split_ascii_whitespace().skip(1);
        let mut map = iter.map(|str| str.parse::<u8>());
        let id = map.next().unwrap().unwrap();
        let resource = map.next().unwrap().unwrap();
        Parse::Prend(id, resource)
    }

    //"plv #n L\n"
    pub fn parse_niveau(content: String) -> Parse {
        let iter = content.split_ascii_whitespace().skip(1);
        let mut map = iter.map(|str| str.parse::<u8>());
        let id = map.next().unwrap().unwrap();
        let niveau = map.next().unwrap().unwrap();
        Parse::Niveau(id, niveau)
    }

    pub fn parse_drop_ressouce(content: String) -> Parse {
        let iter = content.split_ascii_whitespace().skip(1);
        let mut map = iter.map(|str| str.parse::<u8>());
        let id = map.next().unwrap().unwrap();
        let resource = map.next().unwrap().unwrap();
        Parse::Pose(id, resource)
    }

    pub fn parse_inventaire(content: String) -> Parse {
        let vec_parsing = parse_into_integer(content);

        let res = Parse::Inventaire(
            vec_parsing[0] as u8,
            vec_parsing[1] as u32,
            vec_parsing[2] as u32,
            vec_parsing[3] as u8,
            vec_parsing[4] as u8,
            vec_parsing[5] as u8,
            vec_parsing[6] as u8,
            vec_parsing[7] as u8,
            vec_parsing[8] as u8,
            vec_parsing[9] as u8,
        );
        res
    }

    pub fn parse_broadcast(content: String) -> Parse {
        let mut iter = content.split_ascii_whitespace().skip(1);
        let id = iter.next().unwrap().parse::<u8>().ok().unwrap();
        let msg = iter.next().unwrap().to_string();

        //println!("parse_broadcast --> id={id}, msg={msg}");
        Parse::Broadcast(id, msg)
    }

    pub fn parse_incantation(content: String) -> Parse {
        let mut vec_parsing_u8: Vec<u8> = vec![];
        let mut vec_id: Vec<u8> = vec![];
        for i in content.split_ascii_whitespace().skip(1).enumerate()
        //pnw #n X Y O L arsenal\n"
        {
            if i.0 < 3 {
                // //println!("{:?}", i.1);
                vec_parsing_u8.push(i.1.parse::<u8>().ok().unwrap());
            } else {
                vec_id.push(i.1.parse::<u8>().ok().unwrap());
            }
        }
        Parse::Incantation(
            vec_parsing_u8[0],
            vec_parsing_u8[1],
            vec_parsing_u8[2],
            vec_id,
        )
    }

    // Row data from server into Parse variant
    pub fn parser_server_packet(pkt_receive: String) -> Parse {
        let mut iter = pkt_receive.split_ascii_whitespace();
        let parse: Parse;
        match iter.nth(0) {
            Some(content) => {
                match content {
                    "msz" => {
                        parse = parse_map(pkt_receive);
                    }
                    "bct" => {
                        parse = parse_resource(pkt_receive);
                    }
                    "tna" => {
                        parse = parse_teams(pkt_receive);
                    }
                    "pnw" => {
                        parse = parse_connexion_player(pkt_receive);
                    }
                    "ppo" => {
                        parse = parse_player_movement(pkt_receive);
                    }
                    "plv" => {
                        parse = parse_niveau(pkt_receive);
                    }
                    "pin" => {
                        parse = parse_inventaire(pkt_receive);
                    }
                    "pex" => {
                        parse = parse_expulse(pkt_receive);
                    }
                    "pic" => {
                        parse = parse_incantation(pkt_receive);
                    }
                    "pie" => {
                        parse = parse_endinc(pkt_receive);
                    }
                    "pfk" => {
                        parse = parse_fork(pkt_receive);
                    }
                    "pdr" => {
                        parse = parse_drop_ressouce(pkt_receive);
                    }
                    "pgt" => {
                        parse = parse_take_ressouce(pkt_receive);
                    }
                    "pdi" => {
                        parse = parse_death(pkt_receive);
                    }
                    "enw" => {
                        parse = parse_endfork(pkt_receive);
                    }
                    "eht" => {
                        parse = parse_hatching(pkt_receive);
                    }
                    "sgt" => {
                        parse = parse_time(pkt_receive);
                    }
                    "pbc" => {
                        parse = parse_broadcast(pkt_receive);
                    }
                    /*
                    // "   pgt #n i\n"
                    //     "pin #n X Y q q q q q q q\n"
                    //     "bct X Y q q q q q q q\n"
                     */
                    // todo!();

                    // }
                    // "ebo" => {
                    //     todo!();
                    // }
                    // "edi" => {
                    //     todo!();
                    // }
                    // "seg" => {
                    //     todo!();
                    // }
                    // "smg" => {
                    //     todo!();
                    // }
                    // "suc" => {
                    //     todo!();
                    // }
                    // "sbp" => {
                    //     todo!();
                    // }
                    _ => {
                        parse = Parse::Donothing;
                    }
                }
            }
            None => todo!(),
        }
        parse
    }

    fn parse_map(string_map: String) -> Parse {
        let iter = string_map.split_ascii_whitespace().skip(1);
        let mut vec_map: Vec<u8> = vec![];
        for i in iter {
            let string = i;
            // //println!("STRING MAP = {:?}", string);
            vec_map.push(
                string
                    .parse::<u8>()
                    .ok()
                    .expect("I can't overflow over the dimension of the map man"),
            );
        }
        // let x = vec_map[0].parse::<u32>;
        Parse::Map {
            0: vec_map[0],
            1: vec_map[1],
        }
    }
}
