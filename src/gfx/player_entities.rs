//! Renders an animated sprite by loading all animation frames from a single image (a sprite sheet)
//! into a texture atlas, and changing the displayed image periodically.

pub mod player_entities {
    use crate::{
        action::action::{get_top_parent, ActionPlayer, Movementinprogress, VEC_COLORS},
        map::map::Cell,
        TILES_WIDTH,
    };
    use bevy::prelude::*;
    use rand::{thread_rng, Rng};

    const SCALE_EGG: f32 = 0.015;
    const SCALEPLAYER: f32 = 1.8;
    const VECSCALEPLAYER: Vec3 = Vec3::new(SCALEPLAYER, SCALEPLAYER, SCALEPLAYER);
    const SCENESPLAYER: [&'static str; 4] = [
        "3d/Fox.glb#Scene0",
        "3d/Frog.glb#Scene0",
        "3d/Bee.glb#Scene0",
        "3d/Floyd.glb#Scene0",
    ];

    pub struct Action;

    #[derive(Component)]
    pub struct Player(pub u8);

    #[derive(Component, Deref, DerefMut)]
    pub struct AnimationTimer(Timer);

    #[derive(Component)]
    pub struct PlayerId(pub u8);

    pub fn setup_entity_player(
        asset_server: &Res<AssetServer>,
        commands: &mut Commands,
        coord_pixel: (f32, f32),
        coord_cell: (u8, u8, u8),
        team_num: u8,
        player_id: u8,
    ) -> Entity {
        // let sprite_component = animation_to_sprite_component(sprite_animation, &coord_pixel.0, &coord_pixel.1);
        let mut rng = thread_rng();
        // on devrait rajouter un tmp sprite_component et un sprite_bundle
        let name = format!("Player {}", player_id);
        // println!("coor_x {}, coor_y {}", coord_pixel.0, coord_pixel.1);
        // println!("coor_rng_x {}, coor_rng_y {}", coord_pixel.0, coord_pixel.1);

        let coor_rng_pixel_min = (
            coord_pixel.0 - TILES_WIDTH / 4.,
            coord_pixel.1 - TILES_WIDTH / 4.,
        );
        let coor_rng_pixel_max = (
            coord_pixel.0 + TILES_WIDTH / 4.,
            coord_pixel.1 + TILES_WIDTH / 4.,
        );
        let vec_random_position = Vec3::new(
            rng.gen_range(coor_rng_pixel_min.0..coor_rng_pixel_max.0),
            8.,
            rng.gen_range(coor_rng_pixel_min.1..coor_rng_pixel_max.1),
        );

        // let angle = 3.14 + 3.14 / 2.0 * (coord_cell.2 - 1) as f32;
        commands
            .spawn((
                SceneBundle {
                    scene: asset_server.load(SCENESPLAYER[team_num as usize]),
                    transform: Transform::from_translation(vec_random_position)
                        .with_rotation(Quat::from_rotation_y(
                            (3 - coord_cell.2 as i8) as f32 * 3.14 / 2.0,
                        ))
                        .with_scale(VECSCALEPLAYER),
                    ..default()
                },
                // AnimationTimer(Timer::from_seconds(0.1, TimerMode::Repeating)),
                ActionPlayer::new(), // FIFO Actions [TypeActions: Mov(), Expulse(), Fork() ... ]
                Player(team_num),    //Team ?
                Cell(coord_cell.0, coord_cell.1, coord_cell.2), // manque orientation
                Movementinprogress::new(),
                PlayerId(player_id),
                Name::new(name),
            ))
            .id()
    }

    #[derive(Component)]
    pub struct EggId(pub u8);
    #[derive(Component)]
    pub struct EggTeam(pub u8);
    #[derive(Bundle)]
    pub struct EggComponent {
        pub cell: Cell,
        pub scene: SceneBundle,
        pub egg_id: EggId,
        pub eggteam: EggTeam,
    }

    impl EggComponent {
        pub fn new(
            asset_server: &Res<AssetServer>,
            coord_cell: (u8, u8),
            coord_pixel: (f32, f32),
            egg_id: u8,
            egg_team: u8,
            visibility: Visibility,
        ) -> Self {
            EggComponent {
                cell: Cell(coord_cell.0, coord_cell.1, 0),
                scene: SceneBundle {
                    scene: asset_server.load("3d/Egg.glb#Scene0"),
                    transform: Transform::from_xyz(coord_pixel.0, 8., coord_pixel.1)
                        .with_scale(Vec3::new(SCALE_EGG, SCALE_EGG, SCALE_EGG)),
                    visibility,
                    ..default()
                },
                egg_id: EggId(egg_id),
                eggteam: EggTeam(egg_team),
            }
        }
    }

    // pub fn setup_scene_player()

    pub fn setup_sprite_egg(
        commands: &mut Commands,
        asset_server: &Res<AssetServer>,
        coord_cell: (u8, u8),
        coord_pixel: (f32, f32),
        egg_id: u8,
        egg_team: u8,
        visibility: Visibility,
    ) -> Entity {
        commands
            .spawn(EggComponent::new(
                asset_server,
                coord_cell,
                coord_pixel,
                egg_id,
                egg_team,
                visibility,
            ))
            .id()
    }

    /*
       It is always used ?
    */
    pub fn change_color_of_egg(
        mut query_parent: Query<(&Children, &EggId), Changed<Cell>>,
        mut q_handle: Query<(Entity, &mut Handle<StandardMaterial>)>,
        q_child: Query<(Entity, &Children)>,
        query_team: Query<&EggTeam>,
        all_entities_with_parents_query: Query<&Parent>,
        mut materials: ResMut<Assets<StandardMaterial>>,
    ) {
        for (children, _) in query_parent.iter_mut() {
            //println!("color_egg print children = {:?}", children);
            for mut entity_child in children {
                let (_, children) = q_child.get(entity_child.clone()).unwrap();
                entity_child = &children[0];
                let (_, children) = q_child.get(entity_child.clone()).unwrap();
                entity_child = &children[0];
                if let Ok((_, _)) = q_handle.get_mut(*entity_child) {
                    //println!("egg_debug inter 0");
                    entity_child = &children[0];
                    if let Ok((entity, mut test_mat)) = q_handle.get_mut(*entity_child) {
                        //println!("egg_debug inter 1");
                        let parent_entity =
                            get_top_parent(entity, &all_entities_with_parents_query);
                        if let Ok(num_team) = query_team.get(parent_entity) {
                            //println!("egg_debug inter 2");
                            if let Some(mat) = materials.get_mut(test_mat.id()) {
                                //println!("egg_debug team = {:?}", num_team.0);
                                let mut new_material = mat.clone();
                                new_material.base_color = VEC_COLORS[num_team.0 as usize];
                                // nbr_player_color_changed += 1;
                                *test_mat = materials.add(new_material);
                            }
                        }
                    }
                }
            }
        }
    }
}
