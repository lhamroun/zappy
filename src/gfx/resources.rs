pub mod resource {
    use std::collections::HashMap;

    use bevy::{
        asset::{AssetServer, Assets},
        core::Name,
        ecs::{
            entity::Entity,
            system::{Commands, Res, ResMut},
        },
        hierarchy::DespawnRecursiveExt,
        math::{Quat, Vec3},
        pbr::{PbrBundle, StandardMaterial},
        render::{
            color::Color,
            mesh::{shape, Mesh},
        },
        scene::SceneBundle,
        sprite::SpriteBundle,
        transform::components::Transform,
    };
    use rand::{thread_rng, Rng};

    use crate::{env::env::Env, parser::parser::Parse, TILES_WIDTH};

    const SCALE_FOOD: f32 = 0.03;
    const SCALE_RESOURCE: f32 = 0.6; // 0.05 pour le fox
    const VEC_SCALE_FOOD: Vec3 = Vec3::new(SCALE_FOOD, SCALE_FOOD, SCALE_FOOD);
    const VEC_SCALE_RESOURCE: Vec3 = Vec3::new(SCALE_RESOURCE, SCALE_RESOURCE, SCALE_RESOURCE);

    #[derive(Debug)]
    pub struct ContentCase {
        pub nbr_resource: u8,
        pub all_entity: Entity,
    }

    #[derive(Debug, Clone, Copy)]
    pub struct Resources {
        pub x_rel: f32,
        pub x: u32,
        pub y_rel: f32,
        pub y: u32,
        ///****** TRANSFORMER TOUT CA EN VECTOR */
        pub n: u8,
        pub l: u8,
        pub d: u8,
        pub s: u8,
        pub m: u8,
        pub ph: u8,
        pub th: u8,
        /****                                    */
    }

    impl From<&Parse> for Resources {
        fn from(value: &Parse) -> Self {
            match value {
                Parse::ResourceCase(x, y, n, l, d, s, m, ph, th) => Resources {
                    x_rel: 0.,
                    x: *x,
                    y_rel: 0.,
                    y: *y,
                    n: *n,
                    l: *l,
                    d: *d,
                    s: *s,
                    m: *m,
                    ph: *ph,
                    th: *th,
                },
                Parse::Inventaire(_, _, _, n, l, d, s, m, ph, th) => Resources {
                    x_rel: 0.,
                    x: 0,
                    y_rel: 0.,
                    y: 0,
                    n: *n,
                    l: *l,
                    d: *d,
                    s: *s,
                    m: *m,
                    ph: *ph,
                    th: *th,
                },
                _ => panic!("{:?}", value),
            }
        }
    }

    const OFFSET: f32 = 0.01;
    // use shape::Icosphere as RES_MESH;
    use shape::Box as RES_MESH;

    pub fn spawn_food(
        commands: &mut Commands,
        asset_server: &Res<AssetServer>,
        pos_x: f32,
        pos_z: f32,
    ) -> ContentCase {
        let mut rng = thread_rng();
        let coor_rng_pixel_min = (pos_x - TILES_WIDTH * 1. / 3., pos_z - TILES_WIDTH * 1. / 3.);
        let coor_rng_pixel_max = (pos_x + TILES_WIDTH * 1. / 3., pos_z + TILES_WIDTH * 1. / 3.);
        let vec_random_position = Vec3::new(
            rng.gen_range(coor_rng_pixel_min.0..coor_rng_pixel_max.0),
            9.,
            rng.gen_range(coor_rng_pixel_min.1..coor_rng_pixel_max.1),
        );
        let entity = commands
            .spawn((
                SceneBundle {
                    scene: asset_server.load("3d/food.glb#Scene0"),
                    transform: Transform::from_translation(vec_random_position)
                        .with_scale(VEC_SCALE_FOOD)
                        .with_rotation(Quat::from_rotation_x(3.14 / 2.0))
                        .with_rotation(Quat::from_rotation_z(3.14 / 2.0)),
                    ..Default::default()
                },
                Name::new("Food"),
            ))
            .id();
        ContentCase {
            nbr_resource: 1,
            all_entity: entity,
        }
    }

    pub fn add_food(
        commands: &mut Commands,
        asset_server: &Res<AssetServer>,
        content_case: &mut ContentCase,
        pos_x: f32,
        pos_z: f32,
    ) {
        let mut rng = thread_rng();
        let coor_rng_pixel_min = (pos_x - TILES_WIDTH * 1. / 3., pos_z - TILES_WIDTH * 1. / 3.);
        let coor_rng_pixel_max = (pos_x + TILES_WIDTH * 1. / 3., pos_z + TILES_WIDTH * 1. / 3.);
        let vec_random_position = Vec3::new(
            rng.gen_range(coor_rng_pixel_min.0..coor_rng_pixel_max.0),
            9.,
            rng.gen_range(coor_rng_pixel_min.1..coor_rng_pixel_max.1),
        );
        let new_res_entity = commands
            .spawn(SpriteBundle {
                texture: asset_server.load("3d/apple.glb"),
                transform: Transform::from_translation(vec_random_position)
                    .with_scale(VEC_SCALE_FOOD),
                ..Default::default()
            })
            .id();
        content_case.nbr_resource += 1;
        content_case.all_entity = new_res_entity;
    }

    pub fn spawn_linemate(
        commands: &mut Commands,
        asset_meshes: &mut ResMut<Assets<Mesh>>,
        materials: &mut ResMut<Assets<StandardMaterial>>,
        pos_x: f32,
        pos_z: f32,
    ) -> ContentCase {
        let mut rng = thread_rng();
        let coor_rng_pixel_min = (pos_x - TILES_WIDTH * 1. / 3., pos_z - TILES_WIDTH * 1. / 3.);
        let coor_rng_pixel_max = (pos_x + TILES_WIDTH * 1. / 3., pos_z + TILES_WIDTH * 1. / 3.);
        let vec_random_position = Vec3::new(
            rng.gen_range(coor_rng_pixel_min.0..coor_rng_pixel_max.0),
            9.,
            rng.gen_range(coor_rng_pixel_min.1..coor_rng_pixel_max.1),
        );
        let entity = commands
            .spawn((PbrBundle {
                mesh: asset_meshes.add(RES_MESH::default().try_into().unwrap()),
                material: materials.add(Color::BLUE.into()), //debug_material.clone(),
                transform: Transform::from_translation(vec_random_position)
                    .with_scale(VEC_SCALE_RESOURCE),
                ..Default::default()
            },))
            .id();

        ContentCase {
            nbr_resource: 1,
            all_entity: entity,
        }
    }

    pub fn add_linemate(
        commands: &mut Commands,
        asset_server: &Res<AssetServer>,
        content_case: &mut ContentCase,
        pos_x: f32,
        pos_z: f32,
    ) {
        let nbr_res = content_case.nbr_resource + 1;
        let pos_res = Vec3::new(
            pos_x + (nbr_res * 2) as f32,
            pos_z + OFFSET * TILES_WIDTH / 2.,
            13.,
        );
        let new_res_entity = commands
            .spawn(SpriteBundle {
                texture: asset_server.load("Linemate.png"),
                transform: Transform::from_translation(pos_res),
                ..Default::default()
            })
            .id();
        content_case.nbr_resource += 1;
        content_case.all_entity = new_res_entity;
    }

    pub fn spawn_deraumere(
        commands: &mut Commands,
        asset_meshes: &mut ResMut<Assets<Mesh>>,
        mut materials: &mut ResMut<Assets<StandardMaterial>>,
        pos_x: f32,
        pos_z: f32,
    ) -> ContentCase {
        let mut rng = thread_rng();
        let coor_rng_pixel_min = (pos_x - TILES_WIDTH * 1. / 3., pos_z - TILES_WIDTH * 1. / 3.);
        let coor_rng_pixel_max = (pos_x + TILES_WIDTH * 1. / 3., pos_z + TILES_WIDTH * 1. / 3.);
        let vec_random_position = Vec3::new(
            rng.gen_range(coor_rng_pixel_min.0..coor_rng_pixel_max.0),
            9.,
            rng.gen_range(coor_rng_pixel_min.1..coor_rng_pixel_max.1),
        );
        let entity = commands
            .spawn((PbrBundle {
                mesh: asset_meshes.add(RES_MESH::default().try_into().unwrap()),
                material: materials.add(Color::RED.into()), //debug_material.clone(),
                transform: Transform::from_translation(vec_random_position)
                    .with_scale(VEC_SCALE_RESOURCE),
                ..Default::default()
            },))
            .id();

        ContentCase {
            nbr_resource: 1,
            all_entity: entity,
        }
    }

    pub fn add_deraumere(
        commands: &mut Commands,
        asset_server: &Res<AssetServer>,
        content_case: &mut ContentCase,
        pos_x: f32,
        pos_z: f32,
    ) {
        let nbr_res = content_case.nbr_resource + 1;
        let pos_res = Vec3::new(
            pos_x + OFFSET * TILES_WIDTH / 2. + (nbr_res * 2) as f32,
            pos_z + OFFSET * TILES_WIDTH / 2.,
            13.,
        );
        let new_res_entity = commands
            .spawn(SpriteBundle {
                texture: asset_server.load("deraumere.png"),
                transform: Transform::from_translation(pos_res),
                ..Default::default()
            })
            .id();
        content_case.nbr_resource += 1;
        content_case.all_entity = new_res_entity;
    }

    pub fn spawn_sibur(
        commands: &mut Commands,
        asset_meshes: &mut ResMut<Assets<Mesh>>,
        materials: &mut ResMut<Assets<StandardMaterial>>,
        pos_x: f32,
        pos_z: f32,
    ) -> ContentCase {
        let mut rng = thread_rng();

        let coor_rng_pixel_min = (pos_x - TILES_WIDTH * 1. / 3., pos_z - TILES_WIDTH * 1. / 3.);
        let coor_rng_pixel_max = (pos_x + TILES_WIDTH * 1. / 3., pos_z + TILES_WIDTH * 1. / 3.);
        let vec_random_position = Vec3::new(
            rng.gen_range(coor_rng_pixel_min.0..coor_rng_pixel_max.0),
            9.,
            rng.gen_range(coor_rng_pixel_min.1..coor_rng_pixel_max.1),
        );
        let entity = commands
            .spawn((PbrBundle {
                mesh: asset_meshes.add(RES_MESH::default().try_into().unwrap()),
                material: materials.add(Color::PURPLE.into()), //debug_material.clone(),
                transform: Transform::from_translation(vec_random_position)
                    .with_scale(VEC_SCALE_RESOURCE),
                ..Default::default()
            },))
            .id();

        ContentCase {
            nbr_resource: 1,
            all_entity: entity,
        }
    }

    pub fn add_sibur(
        commands: &mut Commands,
        asset_server: &Res<AssetServer>,
        content_case: &mut ContentCase,
        pos_x: f32,
        pos_z: f32,
    ) {
        let nbr_res = content_case.nbr_resource + 1;
        let pos_res = Vec3::new(
            pos_x - OFFSET * TILES_WIDTH / 2. + (nbr_res * 2) as f32,
            pos_z,
            13.,
        );
        let new_res_entity = commands
            .spawn(SpriteBundle {
                texture: asset_server.load("Sibur.png"),
                transform: Transform::from_translation(pos_res),
                ..Default::default()
            })
            .id();
        content_case.nbr_resource += 1;
        content_case.all_entity = new_res_entity;
    }

    pub fn spawn_mendiane(
        commands: &mut Commands,
        asset_meshes: &mut ResMut<Assets<Mesh>>,
        materials: &mut ResMut<Assets<StandardMaterial>>,
        pos_x: f32,
        pos_z: f32,
    ) -> ContentCase {
        let mut rng = thread_rng();

        let coor_rng_pixel_min = (pos_x - TILES_WIDTH * 1. / 3., pos_z - TILES_WIDTH * 1. / 3.);
        let coor_rng_pixel_max = (pos_x + TILES_WIDTH * 1. / 3., pos_z + TILES_WIDTH * 1. / 3.);
        let vec_random_position = Vec3::new(
            rng.gen_range(coor_rng_pixel_min.0..coor_rng_pixel_max.0),
            9.,
            rng.gen_range(coor_rng_pixel_min.1..coor_rng_pixel_max.1),
        );
        let entity = commands
            .spawn((PbrBundle {
                mesh: asset_meshes.add(RES_MESH::default().try_into().unwrap()),
                material: materials.add(Color::GREEN.into()), //debug_material.clone(),
                transform: Transform::from_translation(vec_random_position)
                    .with_scale(VEC_SCALE_RESOURCE),
                ..Default::default()
            },))
            .id();

        ContentCase {
            nbr_resource: 1,
            all_entity: entity,
        }
    }

    pub fn add_mendiane(
        commands: &mut Commands,
        asset_server: &Res<AssetServer>,
        content_case: &mut ContentCase,
        pos_x: f32,
        pos_z: f32,
    ) {
        let nbr_res = content_case.nbr_resource + 1;
        let pos_res = Vec3::new(
            pos_x + OFFSET * TILES_WIDTH / 2. + (nbr_res * 2) as f32,
            pos_z,
            13.,
        );
        let new_res_entity = commands
            .spawn(SpriteBundle {
                texture: asset_server.load("Mendiane.png"),
                transform: Transform::from_translation(pos_res),
                ..Default::default()
            })
            .id();
        content_case.nbr_resource += 1;
        content_case.all_entity = new_res_entity;
    }

    pub fn spawn_phiras(
        commands: &mut Commands,
        asset_meshes: &mut ResMut<Assets<Mesh>>,
        materials: &mut ResMut<Assets<StandardMaterial>>,
        pos_x: f32,
        pos_z: f32,
    ) -> ContentCase {
        let mut rng = thread_rng();

        let coor_rng_pixel_min = (pos_x - TILES_WIDTH * 1. / 3., pos_z - TILES_WIDTH * 1. / 3.);
        let coor_rng_pixel_max = (pos_x + TILES_WIDTH * 1. / 3., pos_z + TILES_WIDTH * 1. / 3.);
        let vec_random_position = Vec3::new(
            rng.gen_range(coor_rng_pixel_min.0..coor_rng_pixel_max.0),
            9.,
            rng.gen_range(coor_rng_pixel_min.1..coor_rng_pixel_max.1),
        );
        let entity = commands
            .spawn((PbrBundle {
                mesh: asset_meshes.add(RES_MESH::default().try_into().unwrap()),
                material: materials.add(Color::YELLOW.into()), //debug_material.clone(),
                transform: Transform::from_translation(vec_random_position)
                    .with_scale(VEC_SCALE_RESOURCE),
                ..Default::default()
            },))
            .id();

        ContentCase {
            nbr_resource: 1,
            all_entity: entity,
        }
    }

    pub fn add_phiras(
        commands: &mut Commands,
        asset_server: &Res<AssetServer>,
        content_case: &mut ContentCase,
        pos_x: f32,
        pos_z: f32,
    ) {
        let nbr_res = content_case.nbr_resource + 1;
        let pos_res = Vec3::new(
            pos_x - OFFSET * TILES_WIDTH / 2. + (nbr_res * 2) as f32,
            pos_z - OFFSET * TILES_WIDTH / 2.,
            13.,
        );
        let new_res_entity = commands
            .spawn(SpriteBundle {
                texture: asset_server.load("Phiras.png"),
                transform: Transform::from_translation(pos_res),
                ..Default::default()
            })
            .id();
        content_case.nbr_resource += 1;
        content_case.all_entity = new_res_entity;
    }

    pub fn spawn_thystame(
        commands: &mut Commands,
        asset_meshes: &mut ResMut<Assets<Mesh>>,
        materials: &mut ResMut<Assets<StandardMaterial>>,
        pos_x: f32,
        pos_z: f32,
    ) -> ContentCase {
        let mut rng = thread_rng();

        let coor_rng_pixel_min = (pos_x - TILES_WIDTH * 1. / 3., pos_z - TILES_WIDTH * 1. / 3.);
        let coor_rng_pixel_max = (pos_x + TILES_WIDTH * 1. / 3., pos_z + TILES_WIDTH * 1. / 3.);
        let vec_random_position = Vec3::new(
            rng.gen_range(coor_rng_pixel_min.0..coor_rng_pixel_max.0),
            9.,
            rng.gen_range(coor_rng_pixel_min.1..coor_rng_pixel_max.1),
        );
        let entity = commands
            .spawn((PbrBundle {
                mesh: asset_meshes.add(RES_MESH::default().try_into().unwrap()),
                material: materials.add(Color::GOLD.into()), //debug_material.clone(),
                transform: Transform::from_translation(vec_random_position)
                    .with_scale(VEC_SCALE_RESOURCE),
                ..Default::default()
            },))
            .id();

        ContentCase {
            nbr_resource: 1,
            all_entity: entity,
        }
    }

    pub fn add_thystame(
        commands: &mut Commands,
        asset_server: &Res<AssetServer>,
        content_case: &mut ContentCase,
        pos_x: f32,
        pos_z: f32,
    ) {
        let nbr_res = content_case.nbr_resource + 1;
        let pos_res = Vec3::new(
            pos_x + OFFSET * TILES_WIDTH / 2. + (nbr_res * 2) as f32,
            pos_z - OFFSET * TILES_WIDTH / 2.,
            13.,
        );
        let new_res_entity = commands
            .spawn(SpriteBundle {
                texture: asset_server.load("Thystame.png"),
                transform: Transform::from_translation(pos_res),
                ..Default::default()
            })
            .id();
        content_case.nbr_resource += 1;
        content_case.all_entity = new_res_entity;
    }

    pub fn compare_resources(
        commands: &mut Commands,
        res: Resources,
        vec_hashress: &mut Vec<Vec<HashMap<u32, ContentCase>>>,
    ) {
        // bonne fonction mais il faut introduire les states d'abord
        let vec_resource = vec![res.n, res.l, res.d, res.s, res.m, res.ph, res.th];
        vec_resource
            .into_iter()
            .enumerate()
            .for_each(|(index, actual_res)| {
                if let Some(x) =
                    vec_hashress[res.y as usize][res.x as usize].get_mut(&(index as u32))
                {
                    if x.nbr_resource != actual_res {
                        commands.get_entity(x.all_entity);
                    }
                }
            });
    }

    pub fn add_all_resources(
        commands: &mut Commands,
        asset_server: &Res<AssetServer>,
        res: Resources,
        vec_hash_res: &mut Vec<Vec<HashMap<usize, ContentCase>>>,
        asset_meshes: &mut ResMut<Assets<Mesh>>,
        materials: &mut ResMut<Assets<StandardMaterial>>,
    ) {
        if res.n > 0 {
            let mut content_case = spawn_food(commands, asset_server, res.x_rel, res.y_rel);
            content_case.nbr_resource = res.n;
            vec_hash_res[res.y as usize][res.x as usize].insert(0, content_case);
        }
        if res.l > 0 {
            let mut content_case =
                spawn_linemate(commands, asset_meshes, materials, res.x_rel, res.y_rel);
            content_case.nbr_resource = res.l;
            vec_hash_res[res.y as usize][res.x as usize].insert(1, content_case);
        }
        if res.d > 0 {
            let mut content_case =
                spawn_deraumere(commands, asset_meshes, materials, res.x_rel, res.y_rel);
            content_case.nbr_resource = res.d;
            vec_hash_res[res.y as usize][res.x as usize].insert(2, content_case);
        }
        if res.s > 0 {
            let mut content_case =
                spawn_sibur(commands, asset_meshes, materials, res.x_rel, res.y_rel);
            content_case.nbr_resource = res.s;
            vec_hash_res[res.y as usize][res.x as usize].insert(3, content_case);
        }
        if res.m > 0 {
            let mut content_case =
                spawn_mendiane(commands, asset_meshes, materials, res.x_rel, res.y_rel);
            content_case.nbr_resource = res.m;
            vec_hash_res[res.y as usize][res.x as usize].insert(4, content_case);
        }
        if res.ph > 0 {
            let mut content_case =
                spawn_phiras(commands, asset_meshes, materials, res.x_rel, res.y_rel);
            content_case.nbr_resource = res.ph;
            vec_hash_res[res.y as usize][res.x as usize].insert(5, content_case);
        }
        if res.th > 0 {
            let mut content_case =
                spawn_thystame(commands, asset_meshes, materials, res.x_rel, res.y_rel);
            content_case.nbr_resource = res.th;
            vec_hash_res[res.y as usize][res.x as usize].insert(6, content_case);
        }
    }

    pub fn spawn_individual_resource(
        commands: &mut Commands,
        asset_server: &Res<AssetServer>,
        res: Resources,
        vec_hashress: &mut Vec<Vec<HashMap<usize, ContentCase>>>,
        num_resource: &u8,
        asset_meshes: &mut ResMut<Assets<Mesh>>,
        materials: &mut ResMut<Assets<StandardMaterial>>,
    ) {
        match num_resource {
            0 => {
                vec_hashress[res.y as usize][res.x as usize]
                    .insert(0, spawn_food(commands, asset_server, res.x_rel, res.y_rel));
            }
            1 => {
                vec_hashress[res.y as usize][res.x as usize].insert(
                    1,
                    spawn_linemate(commands, asset_meshes, materials, res.x_rel, res.y_rel),
                );
            }
            2 => {
                vec_hashress[res.y as usize][res.x as usize].insert(
                    2,
                    spawn_deraumere(commands, asset_meshes, materials, res.x_rel, res.y_rel),
                );
            }
            3 => {
                vec_hashress[res.y as usize][res.x as usize].insert(
                    3,
                    spawn_sibur(commands, asset_meshes, materials, res.x_rel, res.y_rel),
                );
            }
            4 => {
                vec_hashress[res.y as usize][res.x as usize].insert(
                    4,
                    spawn_mendiane(commands, asset_meshes, materials, res.x_rel, res.y_rel),
                );
            }
            5 => {
                vec_hashress[res.y as usize][res.x as usize].insert(
                    5,
                    spawn_phiras(commands, asset_meshes, materials, res.x_rel, res.y_rel),
                );
            }
            6 => {
                vec_hashress[res.y as usize][res.x as usize].insert(
                    6,
                    spawn_thystame(commands, asset_meshes, materials, res.x_rel, res.y_rel),
                );
            }
            _ => (),
        }
        // add_food(commands, asset_server, res_already, res.x_rel, res.y_rel);
    }

    pub fn spawn_resources(
        commands: &mut Commands,
        asset_server: &Res<AssetServer>,
        res: Resources,
        vec_hashress: &mut Vec<Vec<HashMap<usize, ContentCase>>>,
        num_resource: &u8,
        asset_meshes: &mut ResMut<Assets<Mesh>>,
        materials: &mut ResMut<Assets<StandardMaterial>>,
    ) {
        //println!("{:?}", vec_hashress[res.y as usize][res.x as usize]);
        //verifier si hashmap[y][x].is_empty -> spawn resources
        //sinon on modifie la ou les resources concerne, donc spawn ou despawn
        if vec_hashress[res.y as usize][res.x as usize].is_empty() && *num_resource == 0 {
            add_all_resources(
                commands,
                asset_server,
                res,
                vec_hashress,
                asset_meshes,
                materials,
            );
        } else {
            // spawn_individual_ressource(commands, asset_server, res, vec_hashress, num_ressource);
            //spawn individual ressource ? (compare)
        }

        // commands.spawn((
        //     SpriteBundle{
        //         texture: asset_server.load("Ressource.png"),
        //         transform: transform_for_ressource(res.x_rel as f32, res.y_rel as f32),
        //         ..Default::default()
        //     }
        // ));
        // commands.get_entity(entity)
    }
    // we will need that later to make a better animation of the player taking or dropping ressource

    pub fn decrease_resources(
        command: &mut Commands,
        res: &Resources,
        num_res: u8,
        vec_hashress: &mut Vec<Vec<HashMap<usize, ContentCase>>>,
    ) {
        let opt_content_case =
            vec_hashress[res.y as usize][res.x as usize].get_mut(&(num_res as usize));
        if let Some(content_case) = opt_content_case
        //.remove(&(num_res as usize))
        {
            if content_case.nbr_resource > 0 {
                content_case.nbr_resource -= 1;
                if content_case.nbr_resource == 0 {
                    command.entity(content_case.all_entity).despawn_recursive();
                }
            }
        } else {
            //panic!("at this point the content case must exist");
        }
    }

    pub fn increase_resources(
        commands: &mut Commands,
        env: &mut ResMut<Env>,
        asset_server: &Res<AssetServer>,
        materials: &mut ResMut<Assets<StandardMaterial>>,
        asset_meshes: &mut ResMut<Assets<Mesh>>,
        res: &Resources,
        num_res: u8,
    ) {
        let res_already =
            env.id_resources[res.y as usize][res.x as usize].get_mut(&(num_res as usize));
        if let Some(content_case) = res_already {
            if content_case.nbr_resource > 0 {
                content_case.nbr_resource += 1;
            } else {
                spawn_individual_resource(
                    commands,
                    asset_server,
                    *res,
                    &mut env.id_resources,
                    &num_res,
                    asset_meshes,
                    materials,
                )
            }
        } else {
            spawn_individual_resource(
                commands,
                asset_server,
                *res,
                &mut env.id_resources,
                &num_res,
                asset_meshes,
                materials,
            )
        }
    }
}
