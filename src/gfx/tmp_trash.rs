pub fn change_color_of_all_player(
    query_ground: Query<&Tile>,
    query_team: Query<&Player>,
    mut test: Query<(&mut Handle<StandardMaterial>, Entity)>, //materials: Res<Assets<StandardMaterial>>,
    mut materials: ResMut<Assets<StandardMaterial>>,
    all_entities_with_parents_query: Query<&Parent>,
    mut asset_map: ResMut<Env>,
) {
    let nbr_team = asset_map.nbr_equipe;
    let mut nbr_player_color_changed = 0;
    for (mut test_mat, entity) in &mut test {
        let parent_entity = get_top_parent(entity, &all_entities_with_parents_query);
        for num_team in query_team.get(parent_entity) {
            if let Some(mat) = materials.get_mut(test_mat.id()) {
                let mut new_material = mat.clone();
                new_material.base_color = VEC_COLORS[num_team.0 as usize];
                nbr_player_color_changed += 1;
                *test_mat = materials.add(new_material);
            };
        }
    }
    if asset_map.nbr_player_connected == nbr_player_color_changed {
        asset_map.game_started = true;
    }

    //println!("nbr player startup {:?}", asset_map.nbr_player_connected);
    //println!("nbr player color changed {:?}", nbr_player_color_changed);
}

pub fn animate_sprite(
    time: Res<Time>,
    mut query: Query<(
        &AnimationIndices,
        &mut AnimationTimer,
        &mut TextureAtlasSprite,
        &ActionPlayer,
    )>,
) {
    for (indices, mut timer, mut sprite, action) in &mut query {
        if action.state_action == StateAction::InAction {
            timer.tick(time.delta());
            if timer.just_finished() {
                sprite.index = if sprite.index == indices.last {
                    indices.first
                } else {
                    sprite.index + 1
                };
            }
        }
    }
}

pub fn change_color_of_player(
    // once spawn
    mut query_parent: Query<(&Children), Added<AnimationPlayer>>,
    mut q_handle: Query<(Entity, &mut Handle<StandardMaterial>)>,
    q_child: Query<(Entity, &Children)>,
    query_team: Query<&Player>,
    all_entities_with_parents_query: Query<&Parent>,
    mut materials: ResMut<Assets<StandardMaterial>>,
) {
    let mut nbr_player_color_changed = 0;
    // println!("query_parent {:?}", query_parent);
    for (children) in query_parent.iter_mut() {
        //println!("children = {:?}", children);
        // exit(0);

        for mut entity_child in children {
            let (_, children) = q_child.get(entity_child.clone()).unwrap();
            entity_child = &children[0];

            if let Ok((entity, mut test_mat)) = q_handle.get_mut(*entity_child) {
                //println!("on rentre 1");
                let parent_entity = get_top_parent(entity, &all_entities_with_parents_query);
                for num_team in query_team.get(parent_entity) {
                    //println!("on rentre 2");
                    if let Some(mat) = materials.get_mut(test_mat.id()) {
                        //println!("team = {:?}", num_team.0);
                        let mut new_material = mat.clone();
                        new_material.base_color = VEC_COLORS[num_team.0 as usize];
                        // nbr_player_color_changed += 1;
                        *test_mat = materials.add(new_material);
                    }
                }
                // println!("handle {:?}", test);
            }
        }
    }
}
