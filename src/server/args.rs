pub mod args
{
    use core::num;
    use std::error::Error;
    use std::fmt;
    use std::collections::HashSet;
    use std::path::Path;
    use serde_json::{Result as SerdeResult, Value};
    use std::fs;


/************************************************************************************
 * ParsingError structure
*************************************************************************************/
    #[derive(Debug)]
    pub struct ParsingError
    {
        pub(crate) description : String
    }

    impl ParsingError
    {
        fn new(description : &str) -> ParsingError
        {
            ParsingError { description: description.to_string() }
        }
    }

    impl Error for ParsingError {}

    impl fmt::Display for ParsingError
    {
        fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result
        {
            write!(f, "{}", self.description)
        }
    }


/************************************************************************************
 * Args structure
*************************************************************************************/
    #[derive(Debug)]
    pub struct Args
    {
        pub n: Vec<String>,
        pub c: u8,
        pub p: u16,
        pub x: u8,
        pub y: u8,
        pub t: u16,
        pub debug: Option<Value>,
        pub no_gfx: bool,
        pub auth: bool,
    }

    fn parsing_debug_file(filename: String) -> Result<Value, ParsingError>
    {
        if filename.len() == 0
        {
            return Err(ParsingError::new("Parsing error in cfg file"));
        }
        let data = fs::read_to_string(filename).unwrap();
        let v: Value = serde_json::from_str(data.as_str()).expect("json file parsing error");
        //println!("my json cfg file content:\n{:#?}", v);
        Ok(v)
    }


    /**************************************************************************************
     *  get teams name from the arg -n until -c is reach
     *  params : 
     *      env_args : arguments list
     *
     *  return :
     *      list of string with each team name
     *
     *  TODO:   - changer la dependance au -c (ex: il n'y a pa de -c)
     *          - -c placer avant -n ne doit pas declencher d'erreur
    **************************************************************************************/
    fn get_team_name(env_args: &Vec<String>) -> Result<Vec<String>, ParsingError>
    {
        let flag_n = "-n";
        let next_flag: &str = "-";
        let name_team: Vec<String>;

        // find the -n flag in env_args
        let index_flag_n = env_args.iter()
            .position(|r| r == flag_n)
            .ok_or(ParsingError::new("argument -n missing"))?;
        
        // verify if the flag is found twice
        if let Some(_) = env_args[index_flag_n + 1..].iter()
            .position(|r| r == flag_n)
        {
            return Err(ParsingError::new("argument -n specified twice"));
        };
        
        // find the next flag in env_args
        let index_next_flag = env_args[index_flag_n + 1..].iter()
            .position(|r| r.starts_with(next_flag));

        let index_next_flag = match index_next_flag
        {
            Some(val) => val + index_flag_n,
            None => env_args.len() - 1
        };

        if index_next_flag < index_flag_n + 1
        {
            let msg = "".to_string();
            return Err(ParsingError::new(&msg));
        }
        name_team = env_args[index_flag_n + 1..index_next_flag + 1].to_vec();
        Ok(name_team)
    }


    /**************************************************************************************
     * This function will return the integer value found just after the flag parameter
     * (if we found the flag in the env_args list)
     * params:
     *      env_args : list of arguments
     *      flag : can be -c, -t, -p, -x or -y
     * 
     * return:
     *      integer that follow the flag parameter
    **************************************************************************************/
    fn get_integer_param(env_args: &Vec<String>, flag: String) -> Result<u16, ParsingError>
    {
        let flag_tmp: &str = flag.as_str();
        let msg : String = format!("parameter {} is missing", flag);

        // find the flag in env_args
        let index_flag = env_args
            .iter()
            .position(|r| r == flag_tmp)
            .ok_or(ParsingError::new(&msg))?;

        // verify if the flag is found twice
        if let Some(_) = env_args[index_flag + 1..].iter()
            .position(|r| r == flag_tmp)
        {
            let msg = format!("argument {} specified twice", flag);
            return Err(ParsingError::new(&msg));
        };

        // test if the integer value is present
        if index_flag >= env_args.len() - 1
        {
            let msg : String = format!("parameter {} require integer but missing here", flag);
            return Err(ParsingError::new(&msg));
        }

        // test if a param has two value --> if yes, return Err()
        if index_flag < env_args.len() - 2
        {
            let tmp = &env_args[index_flag + 2];
            if tmp.starts_with("-") == false
            {
                let msg : String = format!("parameter {} require only one value", flag);
                return Err(ParsingError::new(&msg));
            }
        }

        // convert String parameter value into integer
        let player_by_team_slice = &env_args[index_flag + 1];
        let ret = player_by_team_slice
            .parse::<u16>()
            .ok();

        let msg : String = format!("Error: {} option require integer", flag);
        ret.ok_or(ParsingError::new(&msg))
    }

    fn get_bool_param(env_args: &Vec<String>, flag: String) -> Result<bool, ParsingError>
    {
        let flag_tmp: &str = flag.as_str();
        let msg : String = format!("parameter {} is missing", flag);

        // find the flag in env_args
        let index_flag = env_args
            .iter()
            .position(|r| r == flag_tmp);

        if let None = index_flag
        {
            return Ok(false);
        }

        // verify if the flag is found twice
        if let Some(_) = env_args[index_flag.unwrap() + 1..].iter()
            .position(|r| r == flag_tmp)
        {
            let msg = format!("argument {} specified twice", flag);
            return Err(ParsingError::new(&msg));
        };
        return Ok(true);
    }


    fn get_debug_cfg_file(env_args: &Vec<String>, flag: String) -> Result<String, ParsingError>
    {
        let flag_tmp: &str = flag.as_str();
        let tmp_index_flag = env_args
            .iter()
            .position(|r| r == flag_tmp);

        if let None = tmp_index_flag
        {
            return Ok("".to_string());
        }

        let index_flag = tmp_index_flag.unwrap();
        // verify if the flag is found twice
        if let Some(_) = env_args[index_flag + 1..].iter()
            .position(|r| r == flag_tmp)
        {
            let msg = format!("argument {} specified twice", flag);
            return Err(ParsingError::new(&msg));
        };

        // test if the debug file is present
        if index_flag >= env_args.len() - 1
        {
            let msg : String = format!("parameter {} require path to debug file but missing here", flag);
            return Err(ParsingError::new(&msg));
        }

        // test if a param has two value --> if yes, return Err()
        if index_flag < env_args.len() - 2
        {
            let tmp = &env_args[index_flag + 2];
            if tmp.starts_with("-") == false
            {
                let msg : String = format!("parameter {} require only one value", flag);
                return Err(ParsingError::new(&msg));
            }
        }

        if !Path::new(env_args[index_flag + 1].as_str()).exists()
        {
            let msg = "debug file not found".to_string();
            return Err(ParsingError::new(&msg));
        }
        let filename = &env_args[index_flag + 1];
        Ok(filename.to_string())
    }


    /**************************************************************************************
     * Return the duplicates from the list list in prameter
    **************************************************************************************/
    fn find_duplicates(list: &Vec<String>) -> Vec<String> 
    {
        let mut seen = HashSet::new();
        let mut duplicates = Vec::new();

        for item in list.iter() {
            if !seen.insert(item.clone()) {
                // If the item is not added to the set (i.e., it was already present), it's a duplicate
                duplicates.push(item.clone());
            }
        }
        duplicates
    }


    impl Args
    {
        pub fn new(env_args: Vec<String>) -> Result<Self, ParsingError>
        {
            // get the -n param value(s), this param represent the team(s) name
            let name_teams: Vec<String> = get_team_name(&env_args)?;

            // trigger an error if a team name is duplicate
            let duplicates = find_duplicates(&name_teams);
            if duplicates.is_empty() == false
            {
                return Err(ParsingError::new(format!("team name {:?} are duplicates", duplicates).as_str()));
            }
            if name_teams.len() > 4
            {
                return Err(ParsingError { description: "your -n parameter must be max 4 teams".to_owned() });
            }
            for team in name_teams.clone() {
                if team.len() > 12 {
                    return Err(ParsingError { description: "Team names len should be less than 12 chars".to_owned() });
                }
            }

            // get the -c param value, this param represent the number of client in a team
            let num_by_team = get_integer_param(&env_args, "-c".to_string())? as u8;
            if num_by_team == 0 || num_by_team > 4
            {
                return Err(ParsingError { description: "your -c parameter must be in range [1, 4]".to_owned() });
            }

            // get the -x param value, 
            let x = get_integer_param(&env_args, "-x".to_string())? as u8;
            //*
            if x < 15 || x > 20
            {
                return Err(ParsingError { description: "your -x parameter must be in range [15, 20]".to_owned() });
            }
            // */

            // get the -y param value, 
            let y = get_integer_param(&env_args, "-y".to_string())? as u8;
            //*
            if y < 15 || y > 20
            {
                return Err(ParsingError { description: "your -y parameter must be in range [15, 20]".to_owned() });
            }
            // */

            // get the -t param value, 
            let t = get_integer_param(&env_args, "-t".to_string())?;
            if t < 1 || t > 50
            {
                return Err(ParsingError { description: "your -t parameter must be in range [1, 50]".to_owned() });
            }

            // get the -p param value, 
            let p = get_integer_param(&env_args, "-p".to_string())?;
            if p < 1000
            {
                return Err(ParsingError { description: "your -p parameter must be in range [1000, 65535]".to_owned() });
            }

            // get the --debug param value,
            let debug = get_debug_cfg_file(&env_args, "--debug".to_string())?;

            let mut d: Option<Value> = None;
            // --debug option is not mandatory, so if arg not present, debug is empty
            if debug.len() > 0
            {
                // parsing of cfg file json
                d = Some(parsing_debug_file(debug)?);
            }
            //println!("cfg file for init game: {:#?}", d);

            let no_gfx = get_bool_param(&env_args, "--no_gfx".to_string())?;
            //println!("cfg file for init game: {:#?}", d);

            // get the --auth param value,
            let auth = get_bool_param(&env_args, "--auth".to_string())?;
            println!("auth param: {}", auth);

            return Ok(Args{ n: name_teams,
                            c: num_by_team,
                            x: x,
                            y: y,
                            p: p,
                            t: t,
                            debug: d,
                            no_gfx: no_gfx,
                            auth: auth,
                        });
        }
    }

}
