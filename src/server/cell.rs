pub mod cell
{
    use crate::ressources::ressources::Ressources;
    

/**********************************************************************
 * Struct Point
***********************************************************************/
    #[derive(Debug, Clone)]
    pub struct Point
    {
        pub x: u8,
        pub y: u8,
    }

    impl Point
    {
        pub fn new(x: u8, y: u8) -> Self
        {
            Point {x, y}
        }
    }

    // define '==' operation for coord
    impl PartialEq for Point
    {
        fn eq(&self, other: &Self) -> bool
        {
            self.x == other.x && self.y == other.y
        }
    }


/**********************************************************************
 * Struct Cell
***********************************************************************/
    #[derive(Debug, Clone)]
    pub struct Cell
    {
        pub ressources: Ressources,
    }

}