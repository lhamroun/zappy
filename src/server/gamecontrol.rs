
pub mod game
{
    use std::fs::{self, OpenOptions};
    use std::net::TcpStream;
    use std::time::SystemTime;
    use std::collections::HashMap;


    use crate::paket_crafter::paquet_crafter::packet_gfx_fork;
    use crate::player;
    use crate::teams::team::{init_teams_from_dbg_file, Team};
    use crate::args::args::Args;
    use crate::player::player::{Player, PlayerType};
    use crate::cell::cell::Cell;
    use crate::init::init::{init_map_cells, init_map_cells_from_dbg_file};
    use crate::game_utils::game_utils::get_dead_player_list;

    // use std::fs::{OpenOptions, File};
    use std::io::{self, Write};

/**********************************************************************
 * Struct GameController, this is the main structure of the program
***********************************************************************/
    #[derive(Debug)]
    pub struct GameController
    {
        pub x: u8,
        pub y: u8,
        pub cells: Vec<Vec<Cell>>,
        pub teams: Vec<Team>,
        pub timestamp: u32,
        pub stream_gfx: Option<TcpStream>,
        pub stream_hashmap: HashMap<u32, Option<TcpStream>>
    }

   

    fn append_to_file(timestamp: u32, theorical_time: f64, millis: u128) -> io::Result<()> {
        // Open the file in append mode
        let mut file = OpenOptions::new()
            .append(true)  // Set the mode to append
            .open("/Users/lyes/working/zappy/test/debug_timestamp.log")?;  // Open the file, returning an error if it fails
        // Format the string as "timestamp: <f64_value> <u32_value>"
        let formatted_string = format!("timestamp={:5} theorical_time={:8} milis={:8} delay={:8}\n", timestamp, theorical_time, millis, millis - theorical_time as u128);

        // Write the formatted string to the file
        file.write_all(formatted_string.as_bytes())?;


        Ok(())
    }



    impl GameController
    {
        pub fn new(args: &Args) -> Self
        {
            if let Some(debug_mode) = &args.debug
            {
                let debug_options = debug_mode.get("game").unwrap();
                //println!("debug options from file : {:?}", debug_options);
                return GameController {
                    x: debug_options.get("map_x").unwrap().as_u64().unwrap() as u8,
                    y: debug_options.get("map_y").unwrap().as_u64().unwrap() as u8,
                    cells: init_map_cells_from_dbg_file(debug_options.get("cells").unwrap(), 
                                                        debug_options.get("map_x").unwrap().as_u64().unwrap() as u8, 
                                                        debug_options.get("map_y").unwrap().as_u64().unwrap() as u8),
                    teams: init_teams_from_dbg_file(debug_options.get("teams").unwrap()),
                    timestamp: 0,
                    stream_gfx: None,
                    stream_hashmap: HashMap::new()
                };
            }


            let mut vec_teams: Vec<Team> = vec![];

            args.n
                .iter()
                .map(|x| vec_teams.push(Team::new(&x.clone(), args.c)))
                .for_each(drop);

            GameController
            {
                x: args.x,
                y: args.y,
                cells : init_map_cells(args.x, args.y),
                teams: vec_teams,
                timestamp: 0,
                stream_gfx: None,
                stream_hashmap: HashMap::new()
            }
        }

        pub fn get_team_and_push(& mut self, teamname: &String, id: u32, stream: &TcpStream, width: u8, height: u8)
        {
            let port = stream
                                .peer_addr()
                                .unwrap()
                                .port();

            for team in & mut self.teams
            {
                if team.name.eq(teamname) == true
                {
                    team.nb_total_players += 1;
                    team.players.push(Player::new(id, port, width, height));
                }
            }
        }


        pub fn update_timestamp(& mut self, start_time: &SystemTime, t: u16) -> bool
        {
            let now = start_time.elapsed();
            let millis = now.unwrap().as_millis();

            let theorical_time: f64 = 1000.0 / (t as f64);
            let theorical_time: f64 = theorical_time * self.timestamp as f64;
            if theorical_time <= millis as f64
            {
                self.timestamp = self.timestamp + 1;
                // fs::write(debug_timestamp.log, "")
                //let _ = append_to_file(self.timestamp, theorical_time, millis);
                return true;
            }
            false
        }



        /*
        **  update life, action counter, egg counter and return a vector of dead players.
        **  this vector is a tuple of player id and player type (Egg or Player)
        **/
        pub fn update_game_datas(& mut self) -> Vec<(u32, PlayerType)>
        {
            let dead_list = get_dead_player_list(&mut self.teams);
            self.teams.iter_mut().for_each(|t| t.update());
            dead_list
        }

        pub fn get_hatch_eggs_list(&self) -> Vec<u32>
        {
            let mut eggs: Vec<u32> = Vec::new();

            for team in self.teams.clone()
            {
                for egg in team.eggs.clone()
                {
                    if egg.count - 1 == 0 && egg.life - 1 > 0
                    {
                        eggs.push(egg.id);
                    }
                }
            }
            eggs
        }

        pub fn packet_gfx_ressources_map(&self) -> Vec<String>
        {
            let mut vec_packet: Vec<String> = vec![];
            let mut x = 0;
            let mut y = 0; 
        
            for line in &self.cells{
                x = 0;
                for cell in line{
                    vec_packet.push(format!("bct {} {} {}\n", x, y, cell.ressources));
                    x += 1;
                }
                y += 1;
            }
            vec_packet
        }

        

        pub fn packet_gfx_map_size(&self) -> String
        {
            format!("msz {} {}\n", self.x, self.y)
        }

        pub fn packet_gfx_all_players_of_all_teams(&self) -> Vec<Vec<String>>
        {
            let mut packet_gfx_all_players_of_all_teams = vec![];
            for team in &self.teams
            {
                packet_gfx_all_players_of_all_teams.push(team.packet_gfx_add_all_players_team());
            }
            packet_gfx_all_players_of_all_teams
        }
        pub fn packet_gfx_all_teams(&self) -> Vec<String>
        {
            let mut vec_gfx_packets_teams = vec![];
            for team in &self.teams
            {
                vec_gfx_packets_teams.push(team.packet_gfx_add_team());
            }
            vec_gfx_packets_teams
        }

        pub fn packet_eggs_all_teams(&self) -> Vec<String>
        {
            let mut vec_gfx_packets_teams = vec![];
            for team in &self.teams
            {
                for player in &team.players
                {
                    vec_gfx_packets_teams.push(packet_gfx_fork(player.id, player.id, player.coord.clone()));
                }
            }
            vec_gfx_packets_teams
        }


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////// Utils ////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        pub fn get_players_eggs_id(&self) -> Vec<(u32, PlayerType)>
        {
            let mut players_id = Vec::new();

            for team in self.teams.clone()
            {
                let mut tmp: Vec<(u32, PlayerType)> = team
                    .eggs
                    .iter()
                    .map(|e| (e.id as u32, PlayerType::Egg))
                    .collect();
                players_id.append(&mut tmp);
                let mut tmp: Vec<(u32, PlayerType)> = team
                    .players
                    .iter()
                    .map(|p| (p.id, PlayerType::Player))
                    .collect();
                players_id.append(&mut tmp);
            }
            players_id
        }

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////// Print for debug //////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        pub fn print_all_players(&self)
        {
            for team in & self.teams
            {
                println!("- - - - - - - - - - - - - - - - - - team {} - - - - - - - - - - - - - - - - - -", team.name);
                //println!("         name -----------> {}", team.name);
                //println!("         connect_nbr ----> {}", team.connect_nbr);
                //println!("         nb_players -----> {}", team.nb_total_players);
                team.print_players_from_team();
                println!("---------------------------------------------------------------------------------");
                team.print_eggs_from_team();
                println!("- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -\n");
            }
            println!("");
        }

    }

}