pub mod init
{
    use rand::{thread_rng, Rng};
    use serde_json::Value;

    use crate::ressources::ressources::Ressources;
    use crate::cell::cell::Cell;

    /********************************************************************************
     * from a x and y size, we create a 2d map fill by ressources on each cell
     * params:
     *      x: map width
     *      y: map height
     * 
     * return:
     *      vec<vec<Cell>> which is the 2d vector containing all the cells of the map 
    *********************************************************************************/
    pub fn init_map_cells(x: u8, y: u8) -> Vec<Vec<Cell>>
    {
        let mut map : Vec<Vec<Cell>> = Vec::with_capacity(y as usize);

        for i in 0..y
        {
            let mut line : Vec<Cell> = Vec::with_capacity(x as usize);
            for j in 0..x
            {
                line.push(fill_map_cell());
            }
            map.push(line);
        }
        println!("map ---> {:?}", map);
        map
    }

    /********************************************************************************
     * by using random, fill a cell with random quantity of ressources
     * 
     * return:
     *      Cell fill randomly 
    *********************************************************************************/
    pub fn fill_map_cell() -> Cell
    {
        let mut rng = thread_rng();

        let ressource : Ressources = Ressources
        {
            food        : rng.gen_range(4..10),
            sibur       : rng.gen_range(0..5),
            mendiane    : rng.gen_range(0..5),
            linemate    : rng.gen_range(0..5),
            deraumere   : rng.gen_range(0..5),
            phiras      : rng.gen_range(0..5),
            thystame    : rng.gen_range(0..2),
        };
        let cell : Cell = Cell {ressources : ressource};
        cell
    }

    pub fn fill_map_cell_debug(food: u8, ressource: u8) -> Cell
    {

        let tmp : Ressources = Ressources
        {
            food        : food,
            sibur       : ressource,
            mendiane    : ressource,
            linemate    : ressource,
            deraumere   : ressource,
            phiras      : ressource,
            thystame    : ressource,
        };
        Cell {ressources : tmp}
    }

    pub fn init_map_cells_from_dbg_file(json_cells: &Value, x: u8, y: u8) -> Vec<Vec<Cell>>
    {
        let mut cells: Vec<Vec<Cell>> = Vec::new();
        let mut tmp_cells: Vec<Vec<u8>> = Vec::new();
        for (i, elem) in json_cells.as_array().unwrap().iter().enumerate()
        {
            let mut tmp = Vec::new();
            for j in 0..elem.as_array().unwrap().len()
            {
                tmp.push(elem.as_array().unwrap()[j].as_u64().unwrap() as u8);
            }
            tmp_cells.push(tmp);
        }

        let mut flat_cells: Vec<Cell> = Vec::new();
        for elem in tmp_cells
        {
            let ressources = Ressources::new_from_dbg_file(elem);
            let cell: Cell = Cell {ressources: ressources};
            flat_cells.push(cell);
        }
        for i in 0..y
        {
            let mut line : Vec<Cell> = Vec::with_capacity(x as usize);
            for j in 0..x
            {
                line.push(flat_cells[(i * x + j) as usize].clone());
            }
            cells.push(line);
        }

        cells.clone()
    }

    impl std::fmt::Display for Ressources
    {
        fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result
        {
            write!(f, "{} {} {} {} {} {} {}", self.food, self.sibur, self.mendiane, self.linemate, self.deraumere, self.phiras, self.thystame)
        }
    }
}
