use std::collections::HashMap;
use std::{env, thread, fmt};
use std::error::Error as GenericError;
use std::io::{Write, Read};
use std::net::{TcpListener, TcpStream};
use std::time::Duration;
use std::time::SystemTime;

use args::args::{Args, ParsingError};
use game_utils::game_utils::find_egg_from_id;
use gamecontrol::game::GameController;
use rand::distributions::Alphanumeric;
use rand::Rng;
use teams::team::Team;
use stream_utils::stream_utils::{flush, send_pkt_to_stream};
use utils::utils::{copy_until_char, remove_prefix};
use action::action::{action_from_action_template, Action, ActionResult, ReadyAction, INCANTATION, NO_ACTION};
use crate::action::action::get_nb_total_players;
use crate::paket_crafter::paquet_crafter::{ craft_client_packet_action_ready, craft_client_packet_broadcast, craft_client_packet_die, craft_client_packet_expulse, craft_client_packet_pre_action, craft_gfx_egg_hatch, craft_gfx_packet_action_ready, craft_gfx_packet_die, craft_gfx_packet_pre_action};
use crate::stream_utils::stream_utils::{debug_send_enw_pkt_gfx, first_connection_gfx, get_initial_gfx_packets_from_game_ctrl, get_new_connexion};
use crate::game_utils::game_utils::{find_player_from_id, find_team_from_player_id, get_incantation_groups_id, get_ready_incantation_fork};
use crate::utils::utils::{debug_print_dash_frame, debug_print_separator, find_ids_in_groups, remove_duplicate_incantation, clear_screen};


//add module in the crate root
pub mod args;
pub mod cell;
pub mod gamecontrol;
pub mod player;
pub mod ressources;
pub mod teams;
pub mod action;
pub mod init;
pub mod paket_crafter;
pub mod utils;
pub mod game_utils;
pub mod stream_utils;

static GFX_SERVER_PORT: u16 = 8080;
const COMMAND_SLICE: [&'static str; 12] = ["avance\n", "droite\n", "gauche\n", "voir\n", "inventaire\n", "expulse\n", "incantation\n", "fork\n", "connect_nbr\n", "prend ", "pose ", "broadcast "]; 
const RESSOURCES_SLICE: [&'static str; 7] = ["food\n", "linemate\n", "deraumere\n", "sibur\n", "mendiane\n", "phiras\n", "thystame\n"];
const BUF_SIZE: usize = 4096;
const BUF_SIZE_GFX: usize = 160;


/*
**  check if their is a winner 
**  (need one guy at level 8)
**/
fn check_winner(teams: &Vec<Team>) -> bool
{
    for team in teams
    {
        for player in &team.players
        {
            if player.level == 8
            {
                println!("\n\n\n\n\n\n\n");
                println!("_______________________________________");
                println!("|                                     |");
                println!("|     game win by team :              |");
                println!("|                                     |");
                println!("_______________________________________");
                println!("  {:#?}", team);
                println!("_______________________________________");
                println!("\n\n\n\n\n\n\n");
                return true;
            }
        }
    }
    false
}

fn check_everybody_die(teams: &Vec<Team>) -> bool
{
    for team in teams
    {
        if team.players.len() > 0
        {
            return false;
        }
    }
    true
}

fn waiting_graphic_answer(stream:  &mut TcpStream)
{
    let mut buf = [0 as u8; 8];
    let mut ret = 0;

    while ret != 1
    {
        if let Ok(_) = stream.read(&mut buf)
        {
            if stream.peer_addr().unwrap().port() == GFX_SERVER_PORT
            {
                let string_buf = copy_until_char(&buf, b'\n');
                match string_buf.as_str()
                {
                    "GRAPHIC" => { ret = 1 }
                    _ => { }
                }
            }
            
        }
    }
}

 /***********************************************************************************
 * Check if the packet contains the correct teamname, then proceed our handshake.
 * Kick the player if his team is full and drop the connexion
 * Generate the player in the Gamecontroller structure with a new id
 * 
 * 
 * params:
 *      mut stream: TcpStream
 *      hashmap: & mut HashMap<String, u8>
 *      args: & mut Args
 *      id: & mut u32, 
 *      game_ctrl: & mut GameController
 * 
 * return:
 *       ()
*********************************************************************************/
fn create_player_or_kick(stream: & mut TcpStream, hashmap: & mut HashMap<String, u8>, args: & mut Args, id: & mut u32, game_ctrl: & mut GameController)
{
    let mut teamname_buffer = [0 as u8; BUF_SIZE];
    let string_teamname_buffer: String;
    if let  Ok(_) = stream.read(& mut teamname_buffer)
    {
        string_teamname_buffer = copy_until_char(&teamname_buffer, b'\n');
        match args.n.contains(&string_teamname_buffer)
        {
            true => 
            {
                // Add the receive teamname to the hashtable and verify if the team is full or not
                let nbr_player_in_current_team =  hashmap
                    .entry( string_teamname_buffer.clone())
                    .or_insert(0);

                // compare the teamnames received with the teamnames parsed
                let auth_players = args.c - *nbr_player_in_current_team;
                if auth_players < 1
                {
                    // send the Endconnection to kill the client
                    // kick the player
                    println!("end of connection because nb player = {} > {} = arg c", nbr_player_in_current_team, args.c);
                    // remplacer "Endconnection" par "mort"
                    let _ = stream.write("Endconnection".to_string().as_bytes());
                    drop(stream)
                }
                else 
                {
                    // create a new id
                    // send back the id to the client -> Pas d'accord
                    // save the player 
                    // save the stream into player
                    *nbr_player_in_current_team += 1;
                    *id += 1;

                    // ATTENTION A CHECKER QUE LES ENVOIS SE FONT CORRECTEMENT
                    // send nb_client
                    let mut nb_client = auth_players.to_string() + "\n";
                    let _ = stream.write(nb_client.as_bytes());

                    // send size of map
                    let mut size_map = args.x.to_string() + " " + &args.y.to_string() + "\n";
                    let _ = stream.write(size_map.as_bytes());
                    let _ = stream.flush();

                    // generate a random password if --auth is sent
                    if args.auth == true
                    {
                        let random_string: String = rand::thread_rng()
                            .sample_iter(&Alphanumeric)
                            .take(8)
                            .map(char::from)
                            .collect();
                        println!("The password is : {}", random_string);
                        let mut buf = [0 as u8; 8];
                        let mut run_after_passwd = false;
                        while run_after_passwd == false {
                            if let Ok(n) = stream.read(& mut buf) {
                                if n == 0 {
                                    std::process::exit(0);
                                }
                                if std::str::from_utf8(&buf).expect("fail to convert [u8] into String") != random_string {
                                    println!("Incorrect password from team {}", string_teamname_buffer);
                                }
                                else {
                                    run_after_passwd = true;
                                }
                            }
                        }
                    }

                    game_ctrl.get_team_and_push(&string_teamname_buffer, *id, &stream, args.x, args.y);
                }
            }
            false => 
            {
                println!("create player or kick bad_entry");
            }
        }
        flush(&mut teamname_buffer);
    }
    // mut &mut hashtable   
}



/***********************************************************************************
 * Verify if all clients in a team are connected
 * 
 * params:
 *      c: number of client / team
 *      len: number of team
 *      hashmap: represent the number of player connecteds / team --> {'team_name': nb_player}
 * 
 * return:
 *       true if everybody are connected in a team
 *********************************************************************************/
 pub fn client_all_connect(c: u8, len: usize, hashmap: &mut HashMap<String, u8>) -> bool
 {
    match hashmap
        .iter()
        .filter(|&(_, &key)| key == c)
        .map(|_| 1)
        .count()
    {
        hashmap_len if hashmap_len == len   => {return true;}
        _                                          => {return false;}
    }
}
    
/*
**  parse args
**/
fn parsing() -> Result<Args, ParsingError> 
{
    let vec_args: Vec<String> = env::args().collect();
    let server_arg = Args::new(vec_args);
    if let Err(error) = server_arg {
        println!("Usage: ./server -p <port> -x <width> -y <height> -n <team> [<team>] [<team>] ... -c <nb> -t <t>");
        println!("  -p port number");
        println!("  -x world width");
        println!("  -y world height");
        println!("  -n team_name_1 team_name_2 ...");
        println!("  -c number of clients authorized at the beginning of the game");
        println!("  -t time unit divider (the greater t is, the faster the game will go)");
        println!("  --auth actvate client authentification");
        return Err(error);
    }
    //println!("server arg --> {:?}", server_arg);
    return server_arg;
}

/*
**  check if the parameter of a command is valid
**  ex: `prend food` -> true
**      `prend caca` -> false
**/
fn is_valid_obj(object: &str) -> bool
{
    let tmp = format!("{}\n", object);
    let mut ret = false;
    for ressource in RESSOURCES_SLICE
    {
        if ressource.starts_with(object)
        {
            ret = true;
        }
    }
    match ret
    {
        /* txt if RESSOURCES_SLICE.iter().any(|&s| {
            s.starts_with(txt)
        }) => true, */
        true => true,
        _ => false
    }
} 

/*
**  check if a command and it's argument is valid
**  ex: `avance` -> true
**      `pisser` -> false
**/
fn is_valid_cmd(buf: &str) -> bool
{
    match buf
    {
        tmp if tmp.starts_with("broadcast ") => {
            //println!("simple chienne ");
            true
        },
        txt if COMMAND_SLICE.iter().any(|&s| s == txt) => {
            //println!("quadruple chienne ?");
            true
        },
        txt if COMMAND_SLICE.iter().any(|&s| txt.starts_with(s)) => {
            //println!("double chienne");
            let mut tmp = buf.split_whitespace();
            if is_valid_obj(tmp.last().unwrap()) == false
            {
                //println!("double glace");
                return false;
            }
            true
        },
        _ => {/*println!("triple chienne");*/false},
    }
}

/*
**  take the second elem of a string contains whitespace, verify
**  if the second elem is a valid ressource and return it
**  ex: `toto food` -> Some("food")
**      `avance` -> None
**      `prend phiras` -> Some("phiras")
**/
fn get_obj_from_string(command: &String, broadcast: bool) -> Option<String>
{
    match broadcast
    {
        true => {
            //println!("remove prefix ---> {}", remove_prefix(command.to_string(), "broadcast".to_string()).to_string());
            Some(remove_prefix(command.to_string(), "broadcast".to_string()))
        },
        false => {
            let mut ret = false;
            let mut tmp = "";
            for ressource in RESSOURCES_SLICE
            {
                if command.ends_with(ressource)
                {
                    tmp = ressource;
                    ret = true;
                }
            }
            match ret
            {
                true => {
                    //println!("la maladie du bouger bouger");
                    Some(tmp.to_string())
                },
                false => None
            }
            /*
            match command
            {
                command if RESSOURCES_SLICE.iter().any(|&elem| {
                    println!("command: {}, elem: {}, cmd end with elem ? {}", command, elem, command.ends_with(elem));
                    command.ends_with(&elem[0..elem.len() - 1])
                }) => 
                {
                    let mut split = command.split_whitespace();
                    let object = split.nth(1);
                    let tmp = object.unwrap().to_string();
                    Some(tmp)
                }
                _ => None
            }
            */
        }
    }
} 

/*
**  receive data onto TCP stream and create Action for corresponding player
**  params:
**      stream:     the TCP stream where the data is receive
**      game_ctrl:  game datas
**  return:
**      Vec<Action>: list of all new cmd receive from stream
**/
fn receive_action(stream: & mut TcpStream, game_ctrl: & mut GameController) -> Vec<ReadyAction>
{
    // let mut action_receive = [0 as u8; BUF_SIZE];
    let mut action_receive = [0 as u8; 32];
    let mut actions : Vec<ReadyAction> = Vec::new();

    // println!("receive action from : {:?}", stream);
    if let  Ok(_) = stream.read(& mut action_receive)
    {

        if action_receive.len() > 70
        {
            //println!("from {}, receive buffer {:?}", stream.peer_addr().unwrap().port(), &action_receive[..70]);
        }
        else
        {
            //println!("from {}, receive buffer {:?}", stream.peer_addr().unwrap().port(), action_receive);
        }




        for team in & mut game_ctrl.teams
        {
            for player in & mut team.players
            {
                match stream.peer_addr()
                {
                    Ok(addr) => {


                        if player.port == addr.port()
                            && player.actions.len() < 11
                        {
                            // TODO : attention ici au mecanisme des 10 commandes
                            // si l'une echoue, les suivantes ne seront peut etre plus
                            // pertinentes ! il faudra donc modifier ca pour n'envoyer des cmd
                            // que lorsqu'on a une reponse
                            let mut vec_string_command: Vec<String> = Vec::with_capacity(10);

                            vec_string_command.push(copy_until_char(&action_receive, b'\0'));
                            if vec_string_command.is_empty() == false
                            {
                                for string_command in vec_string_command
                                {
                                    //println!("receive command string format : {}", string_command);
                                    if is_valid_cmd(&*string_command)
                                    {
                                        println!("receive cmd from player {} --> {:?}", player.id, string_command);
                                        // keep the new commands into actions in order to create corresponding gfx pkt
                                        actions.push(ReadyAction{id: player.id, action: Action::new_from_string(string_command.clone())});
                                        player.action_push(string_command);
                                    
                                    }
                                }
                            }
                        }


                    },
                    Err(msg) => {
                        //println!("client not responding with error {:?}", msg);
                    }
                }
            }

            for egg in & mut team.eggs
            {
                match stream.peer_addr()
                {
                    Ok(addr) => {


                        if egg.port == addr.port()
                            && egg.actions.len() < 11
                        {
                            // TODO : attention ici au mecanisme des 10 commandes
                            // si l'une echoue, les suivantes ne seront peut etre plus
                            // pertinentes ! il faudra donc modifier ca pour n'envoyer des cmd
                            // que lorsqu'on a une reponse
                            let mut vec_string_command: Vec<String> = Vec::with_capacity(10);

                            vec_string_command.push(copy_until_char(&action_receive, b'\0'));
                            if vec_string_command.is_empty() == false
                            {
                                for string_command in vec_string_command
                                {
                                    //println!("receive command string format : {}", string_command);
                                    if is_valid_cmd(&*string_command)
                                    {
                                        println!("receive cmd from egg {} --> {:?}", egg.id, string_command);
                                        // keep the new commands into actions in order to create corresponding gfx pkt
                                        actions.push(ReadyAction{id: egg.id, action: Action::new_from_string(string_command.clone())});
                                        egg.action_push(string_command);
                                    
                                    }
                                }
                            }
                        }


                    },
                    Err(msg) => {
                        //println!("client not responding with error {:?}", msg);
                    }
                }
            }
        }
        flush(& mut action_receive);
    }
    ////println!("actions -----------> {:?}", actions);
    actions
}

/*
**  Iterate onto each actions of each players and retrieve
**  the actions which their `count` == 0 
**
**  return:
**      Vec<ReadyAction>: list of actions ready to execute
**/
fn get_ready_action_list(teams: &Vec<Team>) -> Vec<ReadyAction>
{
    let mut ready_action: Vec<ReadyAction> = Vec::new();

    for team in teams
    {
        for player in &team.players
        {
            // TODO : instead of for loop on player.actions
            // only check player.actions[0] because the others are
            // not running
            if player.actions.len() > 0 && player.actions[0].count == 0
            {
                let action_to_push = ReadyAction{id: player.id, action: player.actions[0].clone()};
                ready_action.push(action_to_push);
            }
        }
    }
    //println!("ready actions list --> {:?}", ready_action);
    ready_action
}

/*
**  execute action from a ReadyAction 
**/
fn exec_action(ready_action: &ReadyAction, game_ctrl: & mut GameController) -> Option<ActionResult>
{
    let tmp_player = find_player_from_id(game_ctrl.teams.clone(), &ready_action.id);
    let mut player = tmp_player.unwrap();
    // TODO:    checher une autre facon plus propre de faire executer mes actions
    //          ou alors changer les method par des methodes statics 
    let action = Action::new(NO_ACTION);
    let ret = match ready_action.action.action_name.as_str()
    {
        "avance" => ActionResult::ActionBool(action.avance(&game_ctrl.x, &game_ctrl.y, &mut player)),
        "droite" => ActionResult::ActionBool(action.droite(&mut player)),
        "gauche" => ActionResult::ActionBool(action.gauche(&mut player)),
        "voir" => ActionResult::ActionVecHashMap(action.voir(&mut player, &game_ctrl.cells, &game_ctrl.teams)),
        "inventaire" => ActionResult::ActionHashMap(action.inventaire(&mut player)),
        "prend" => ActionResult::ActionBool(action.prend(&mut game_ctrl.cells[player.coord.y as usize][player.coord.x as usize], &mut player, ready_action.action.arg.clone().unwrap())),
        "pose" => ActionResult::ActionBool(action.pose(&mut game_ctrl.cells[player.coord.y as usize][player.coord.x as usize], &mut player, ready_action.action.arg.clone().unwrap())),
        "expulse" => ActionResult::ActionBool(action.expulse(&mut game_ctrl.teams, &player, &game_ctrl.x, &game_ctrl.y)),
        "broadcast" => ActionResult::ActionBool(action.broadcast(&player, &game_ctrl.teams)),
        "incantation" => ActionResult::ActionBool(action.incantation(&mut player, &mut game_ctrl.teams)),
        "fork" => ActionResult::ActionBool(action.fork(&player, &mut game_ctrl.teams)),
        "connect_nbr" => ActionResult::ActionInt(action.connect_nbr(&player, &game_ctrl.teams)),
        _ => return None,
    };

    if ready_action.action.action_name == "incantation".to_string()
    {
        match ret
        {
            ActionResult::ActionBool(true) =>
            {
                for team in &mut game_ctrl.teams
                {
                    for mut tmp_player in &mut team.players
                    {
                        //println!("player tmp id + level ---> {} {}", tmp_player.id, tmp_player.level);
                        if tmp_player.coord.x == player.coord.x
                            && tmp_player.coord.y == player.coord.y
                            && tmp_player.level == player.level
                        {
                            //println!("in exec action player {} level {}", player.id, player.level);
                            tmp_player.level += 1;
                        }
                        if tmp_player.id == player.id
                        {
                            tmp_player.ivt.clone_from(&player.ivt.clone());
                        }
                    }
                }
            },
            _ => (),
        }
    }
    


    // TODO :   find a better way to apply the modification of the
    //          player on the team directly
    if ready_action.action.action_name != "incantation"
    {
        for team in &mut game_ctrl.teams
        {
            for team_player in &mut team.players
            {
                if player.id == team_player.id
                {
                    team_player.clone_from(&player);
                }
            }
        }
    }
    //println!("exec action {} ---> {:?}", ready_action.action.action_name, ret);
    Some(ret)
}


/*
** For action incantation I must to apply incantation of the concerned players on the game
** params:
**      ready_actiom: action receive during the current loop
**      teams: teams
**  return:
**      Vec<u32> list of players concerned by the incantation
**
*/
fn apply_incantation_to_concerned_players(ready_action: &ReadyAction, teams: &mut Vec<Team>) -> Vec<u32>
{
    let mut ids: Vec<u32> = Vec::new();

    match ready_action.action.action_name.as_str()
    {
        "incantation" => 
        {
            //let mut teams = teams_ref.clone();
            let player_ref = find_player_from_id(teams.clone(), &ready_action.id);
            if let Some(player) = player_ref
            {
                // check if player waiting for other action 
                if player.actions[0].action_name != "incantation"
                {
                    println!("player {} need to wait before running incantation : {:?}", player.id, player.actions);
                    return ids;
                }
                let coord = player.coord.clone();
                //ids.retain(|value| *value != ready_action.id);
                //println!("la tomate est un fruit {:?}", ids);
                let coord = player.coord.clone();
                let incantation = action_from_action_template(INCANTATION);
                for team in teams
                {
                    for tmp_player in &mut team.players
                    {
                        //println!("tmp player {:?}", tmp_player);
                        if tmp_player.id != ready_action.id
                            && tmp_player.coord.x == coord.x
                            && tmp_player.coord.y == coord.y
                            && tmp_player.level == player.level.clone()
                        {
                            //println!("Une chose a la fois svp {:?} {:?}", tmp_player, player);
                            tmp_player.actions.insert(0, incantation.clone());
                            ids.push(tmp_player.id);
                        }
                    }

                }
                //teams_ref = &teams.clone();
            }
        },
        _ => ()
    }
    //println!("ids --------------->  {:?}", ids);
    ids
}

fn main() -> Result<(), Box<dyn GenericError>> 
{
    let mut id: u32 = 0;
    let mut gfx_stream: TcpStream;
    let mut stream_hashmap: HashMap<u32, TcpStream> = HashMap::new();
    let mut wait_for_answer: bool = true;
    let mut hashmap: HashMap<String, u8> = HashMap::new();
    let mut new_actions: Vec<ReadyAction> = Vec::new();
    let mut vec_args = parsing()?;

    // game controller initialization
    let mut game_ctrl = GameController::new(&vec_args);


    // network initialization
    let listener = TcpListener::bind(format!("127.0.0.1:{}", vec_args.p)).unwrap();
    println!("Start server");


    if let None = vec_args.debug {
    
        // listen for client connexion
        for tcpstream in listener.incoming()
        {
            println!("Connection established!");
            if let Ok(mut stream) = tcpstream
            {
                let _ = stream.write(b"BIENVENUE\n");
                // register the new client
                create_player_or_kick(& mut stream, & mut hashmap, & mut vec_args, & mut id, & mut game_ctrl);
                // set timeout
                let _ = stream.set_read_timeout(Some(Duration::new(0, 10)));
                stream_hashmap.insert(id, stream);
                // game_ctrl.print_all_players();
                if client_all_connect(vec_args.c, vec_args.n.len(), & mut hashmap) { break ; }
            }
        }

        println!("Everybody is connected, let's start the game");
    }
        

    if vec_args.no_gfx == false
    {
        // connect to GFX server
        gfx_stream = first_connection_gfx().unwrap();
        // connexion handshake with the GFX server
        println!("SENDING HANDSHAKE TO GFX");
        send_pkt_to_stream(get_initial_gfx_packets_from_game_ctrl(&game_ctrl, vec_args.t), &mut gfx_stream); 
        //println!("gfx stream --> {:?}", gfx_stream);
    
        // Waiting for "GRAPHIC\n" pkt from gfx
        println!("WAITING FOR GRAPHIC ANSWER");
        waiting_graphic_answer(&mut gfx_stream);
        println!("ANSWER GRAPHIC DONE !!!");
        if let Some(_) = vec_args.debug
        {
            debug_send_enw_pkt_gfx(&game_ctrl, &gfx_stream);
        }
    }
    else
    {
        // connect to GFX server
        gfx_stream = first_connection_gfx().unwrap();
        // connexion handshake with the GFX server
        println!("SENDING HANDSHAKE TO GFX");
        send_pkt_to_stream(get_initial_gfx_packets_from_game_ctrl(&game_ctrl, vec_args.t), &mut gfx_stream); 
        //println!("gfx stream --> {:?}", gfx_stream);
    
        // Waiting for "GRAPHIC\n" pkt from gfx
        println!("WAITING FOR GRAPHIC ANSWER");
        waiting_graphic_answer(&mut gfx_stream);
        println!("ANSWER GRAPHIC DONE !!!");
        if let Some(_) = vec_args.debug
        {
            debug_send_enw_pkt_gfx(&game_ctrl, &gfx_stream);
        }
    }

    

    // take initial timestamp
    let start_time = SystemTime::now();
    //println!("start_time ---> {:?}", start_time);


    
    Ok(loop
    {
        // BIG condition d'arret de la loop
        if check_winner(&game_ctrl.teams.clone()) || check_everybody_die(&game_ctrl.teams.clone())
        {
            //game_ctrl.print_all_players();
            break;
        }
        
        
        // 1
        // receive pkts from clients
        for stream in &mut stream_hashmap
        {
            //println!("stream --> {:?}", stream);
            new_actions.extend(receive_action(stream.1, &mut game_ctrl));
            //println!("receive actions --> {:?}", new_actions);
        }
        //println!("receive actions --> {:?}", new_actions);

        // 3
        // update le timestamp
        if game_ctrl.update_timestamp(&start_time, vec_args.t)
        {
            clear_screen();
            println!("_______________________");
            println!("| ⟹⟫ TIMESTAMP : {} ", game_ctrl.timestamp - 1);
            println!("|______________________");
            game_ctrl.print_all_players();
            //game_ctrl.packet_gfx_ressources_map();





            for team in &mut game_ctrl.teams {
                for egg in &mut team.eggs {
                    if egg.connexion_count > 0 {
                        if let Some(new_connexion) = get_new_connexion( egg.id,
                                                                                                &listener,
                                                                                         game_ctrl.x,
                                                                                        game_ctrl.y) {
                            //println!("new connexion incomming --> {:?}", new_connexion);
                            //let tmp_id = new_connexion.iter().next().unwrap().0;
                            
                            // for team in &mut game_ctrl.teams {
                            //     for egg in &mut team.eggs {
                            //         if *tmp_id == egg.id {
                            //             //println!("default egg value --> {:?}", egg);
                            //             //egg.set_egg_port(new_connexion.iter().next().unwrap().1);
                                           //egg.port = new_connexion.iter().next().unwrap().1.peer_addr().unwrap().port();
                            //             //println!("default egg value --> {:?}", egg);
                            //             break;
                            //         }
                            //     }
                            // }
                            egg.set_egg_port(new_connexion.iter().next().unwrap().1);
                            egg.port = new_connexion.iter().next().unwrap().1.peer_addr().unwrap().port();
                            stream_hashmap.extend(new_connexion);
                            egg.connexion_count = -1;
                        }
                        else {
                            egg.connexion_count -= 1;
                        }
                    }
                    else if egg.connexion_count == 0 {
                        println!("Connexion with new client fail");
                        egg.connexion_count = -1;
                    }
                }
            }






            // 3.3
            // check action finsished and exec --> if an action is fork, listen new connexion
            let mut ready_action_list = get_ready_action_list(&game_ctrl.teams);
            if ready_action_list.len() > 0
            {
                //println!("ready action list --> {:?}", ready_action_list);

                // separer les incantations des autres actions pour reorganiser les pkts
                let incantation_groups: Vec<Vec<u32>> = get_incantation_groups_id(game_ctrl.teams.clone(), &ready_action_list);
                //println!("incantation groups --> {:?}", incantation_groups);

                remove_duplicate_incantation(&game_ctrl.teams.clone(), &mut ready_action_list, incantation_groups.clone());
                //println!("ready action list after removing duplicate incantation --> {:?}", ready_action_list);


                for ready_action in ready_action_list
                {
                    // list before = get list des id de tous les joueurs
                    let action_result = exec_action(&ready_action, & mut game_ctrl);
                    //println!("action result  ---> {:?}", action_result );
                    let gfx_pkt = craft_gfx_packet_action_ready(&ready_action, &action_result, &game_ctrl, incantation_groups.clone());
                    //println!("gfx pkt ready action ---> {:?}", gfx_pkt);
                    if let Some(packet) = gfx_pkt 
                    {
                        if vec_args.no_gfx == false
                        {
                            send_pkt_to_stream(packet, &mut gfx_stream);
                        }
                    }
                    let client_pkt = craft_client_packet_action_ready(&ready_action, &action_result, &game_ctrl);
                    //println!("client pkt ready action ---> {:?}", client_pkt);
                    if let None = vec_args.debug
                    {
                        if let Some(packet) = client_pkt 
                        {
                            if ready_action.action.action_name == "incantation"
                            {
                                let ids = find_ids_in_groups(ready_action.id, incantation_groups.clone());
                                //println!("ids --> {:?}", ids);
                                for id in ids
                                {
                                    send_pkt_to_stream(packet.clone(), stream_hashmap.get(&id).unwrap());
                                }
                            }
                            else
                            {
                                send_pkt_to_stream(packet, stream_hashmap.get(&ready_action.id).unwrap());
                            } 
                        }
                    }
                    else
                    {
                        println!("action result --> {:?}", action_result);
                    }


                    // send broadcast answer
                    if ready_action.action.action_name == "broadcast".to_string()
                    {
                        // sending broadcast message
                        let client_pkt = craft_client_packet_broadcast(&ready_action, &action_result.clone().unwrap(), &game_ctrl);
                        let tmp_team = game_ctrl.teams.clone();
                        let team = find_team_from_player_id(ready_action.id, &tmp_team);
                        if let None = vec_args.debug
                        {
                            if let Some(ref packet) = client_pkt 
                            {
                                for pkt in packet
                                {
                                    if let Some(tmp_stream) = stream_hashmap.get(&pkt.0)
                                    {
                                        let mut tmp: Vec<String> = Vec::new();
                                        tmp.push(pkt.1.clone());
                                        send_pkt_to_stream(tmp, tmp_stream);
                                    }
                                }
                            } 
                        }
                        else
                        {
                            println!("client pkt --> {:?}", client_pkt);
                        }
                    }


                    // send expulse answer to clients concerned
                    if ready_action.action.action_name == "expulse".to_string()
                    {
                        let client_pkt = craft_client_packet_expulse(&ready_action, &action_result.unwrap(), &game_ctrl);
                        let tmp_team = game_ctrl.teams.clone();
                        let team = find_team_from_player_id(ready_action.id, &tmp_team);
                        if let None = vec_args.debug
                        {
                            if let Some(ref packet) = client_pkt 
                            {
                                for pkt in packet
                                {
                                    //println!("id client {} to send ---> {}", pkt.0, pkt.1);
                                    if let Some(tmp_stream) = stream_hashmap.get(&pkt.0)
                                    {
                                        let mut tmp: Vec<String> = Vec::new();
                                        tmp.push(pkt.1.clone());
                                        send_pkt_to_stream(tmp, tmp_stream);
                                    }
                                }
                            } 
                        }
                        else
                        {
                            println!("client pkt --> {:?}", client_pkt);
                        }
                    }

                    if ready_action.action.action_name.as_str() == "fork"
                    {

                        /*
                        if let Some(new_connexion) = get_new_connexion(
                            get_nb_total_players(&game_ctrl.teams),
                            &listener,
                            game_ctrl.x,
                            game_ctrl.y) /* egg id */ {
                            //println!("new connexion incomming --> {:?}", new_connexion);
                            let tmp_id = new_connexion.iter().next().unwrap().0;
                     
                            for team in &mut game_ctrl.teams {
                                for egg in &mut team.eggs {
                                    if *tmp_id == egg.id {
                                        //println!("default egg value --> {:?}", egg);
                                        //egg.set_egg_port(new_connexion.iter().next().unwrap().1);
                                        egg.port = new_connexion.iter().next().unwrap().1.peer_addr().unwrap().port();
                                        //println!("default egg value --> {:?}", egg);
                                        break;
                                    }
                                }
                            }
                            stream_hashmap.extend(new_connexion);
                        }
                        else {
                            println!("Connexion with new client fail");
                        }
                        */
                    }
                }

                // remove old actions
                for team in &mut game_ctrl.teams
                {
                    for mut player in &mut team.players
                    {
                        if player.actions.len() > 0
                        {
                            if player.actions[0].count == 0
                            {
                                player.actions.remove(0);
                            }
                        }
                    }
                }
            }






            // 2
            // get new actions for sending pre pakets (fork / incantation)
            new_actions = get_ready_incantation_fork(&game_ctrl.teams);
            //println!("ready incantation and fork --> {:?}", new_actions);
            for new_action in &new_actions
            {
                let players_incantated = apply_incantation_to_concerned_players(new_action, &mut game_ctrl.teams);
                //println!("player incantated {:?}", players_incantated);
                let gfx_pkt = craft_gfx_packet_pre_action(new_action, players_incantated.clone(), &game_ctrl.teams);
        
                if let Some(gfx_pkt_tmp) = gfx_pkt
                {
                    if vec_args.no_gfx == false
                    {
                        send_pkt_to_stream(gfx_pkt_tmp, &mut gfx_stream);
                    }
                }
                
                if let None = vec_args.debug
                {
                    let client_pkt = craft_client_packet_pre_action(&new_action);
                    if let Some(client_pkt_tmp) = client_pkt
                    {
                        //println!("incanted {:?}", players_incantated);
                        //send_pkt_to_stream(client_pkt_tmp.clone(), stream_hashmap.get(&new_action.id).unwrap());
                        for id in players_incantated
                        {
                            send_pkt_to_stream(client_pkt_tmp.clone(), stream_hashmap.get(&id).unwrap());
                        }
                        send_pkt_to_stream(client_pkt_tmp.clone(), stream_hashmap.get(&new_action.id).unwrap());
                    }
                }
            }
            if !new_actions.is_empty() { new_actions = Vec::new(); }



            // 3.0 get the hatch eggs to send gfx pkt
            let hatch_eggs = game_ctrl.get_hatch_eggs_list();
            if hatch_eggs.len() > 0 { println!("hatch eggs {:?}", hatch_eggs); }
            // send egg_hatch pkt to gfx
            let mut pkts = craft_gfx_egg_hatch(&hatch_eggs, game_ctrl.teams.clone());
            if let Some(gfx_pkt_tmp) = pkts
            {
                if vec_args.no_gfx == false
                {
                    send_pkt_to_stream(gfx_pkt_tmp, &mut gfx_stream);
                }
            }


            // 3.1
            // update game datas (life, counter etc) and retrieve dead players list
            let dead_players = game_ctrl.update_game_datas();
            if dead_players.len() > 0 { println!("dead_players {:?}", dead_players); }


            

            //3.2
            // send dead pkt to clients and gfx
            let mut pkts = craft_gfx_packet_die(&dead_players);
            if let Some(gfx_pkt_tmp) = pkts
            {
                if vec_args.no_gfx == false
                {
                    send_pkt_to_stream(gfx_pkt_tmp, &mut gfx_stream);
                }
            }
            pkts = craft_client_packet_die(&dead_players);
            if let None = vec_args.debug
            {
                if let Some(client_pkt_tmp) = pkts
                {
                    for i in 0..dead_players.len()
                    {
                        if let Some(tmp_stream) = stream_hashmap.get(&dead_players[i].0)
                        {
                            send_pkt_to_stream(vec![client_pkt_tmp[0].clone()], tmp_stream);
                        }
                        //send_pkt_to_stream(vec![client_pkt_tmp[0].clone()], stream_hashmap.get(&dead_players[i].0).unwrap());
                    }
                }
            }
            // debug_print_separator();
            //debug_print_dash_frame();
            //if game_ctrl.timestamp >= 46 { std::process::exit(0); }
        }
    })
    
}
