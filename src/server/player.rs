pub mod player
{

    use std::net::TcpStream;

    use crate::ressources::ressources::Ressources;
    use crate::cell::cell::Point;
    use crate::action::action::{Action, ActionTemplate};
    use crate::get_obj_from_string;
    use crate::action::action::*;

    use rand::{thread_rng, Rng};


    #[derive(Debug, Clone, PartialEq)]
    pub enum Orientation
    {
        N,
        E,
        S,
        O
    }

    #[derive(Debug, PartialEq, Clone)]
    pub enum PlayerType
    {
        Player,
        Egg,
    }

    #[derive(Debug, Clone)]
    pub struct Egg
    {
        pub id: u32,
        pub count: u16,
        pub coord: Point,
        pub life: u16,
        pub actions: Vec<Action>,
        pub port: u16,
        pub connexion_count: i32,
    }

    impl Egg
    {
        pub fn update(&mut self)
        {
            self.count = self.count - 1;
            self.life = self.life - 1;
        }

        pub fn new_from_dbg_file(id: u32, count: u16, coord_x: u8, coord_y: u8, life: u16, actions: Vec<Action>) -> Egg
        {
            Egg {
                id: id,
                count: count,
                coord: Point::new(coord_x, coord_y),
                life: life,
                actions: actions,
                port: 0,
                connexion_count: -1,
            }
        }

        pub fn set_egg_port(&mut self, new_connexion: &TcpStream)
        {
            println!("new port from egg {} : {}", self.id, new_connexion.peer_addr().unwrap().port());
            self.port = new_connexion.peer_addr().unwrap().port();
        }

        pub fn action_push(& mut self, command_full: String)
        {
            let ret = match command_full.starts_with("broadcast")
            {
                true => true,
                false => false
            };
            let mut action: Action = Action::new(NO_ACTION);
            let object: Option<String> = get_obj_from_string(&command_full, ret);

            for tmp_command in COMMANDS
            {
                if command_full.starts_with(tmp_command.action_name)
                {
                    action = Action::new(ActionTemplate{action_name: tmp_command.action_name, arg: object, count: tmp_command.count});
                    break ;
                }
            }
            //println!("id = {} Action {:?}", self.id, action);
            self.actions.push(action.clone());
        }

    }


    #[derive(Debug, Clone)]
    pub struct Player
    {
        pub id: u32,
        //pub stream: TcpStream, // pas sur
        pub port: u16,
        pub coord: Point,
        pub ivt: Ressources,
        pub life: u16,
        pub orientation: Orientation,
        pub level: u8,
        pub actions: Vec<Action>,
    }

    
    impl Player
    {
        pub fn new(id_a: u32, port: u16, width: u8, height: u8) -> Self
        {
            let mut rng = thread_rng();

            /* ---------TO REMOVE----------- */
            /* */ let mut lvl = 1;
            /* */ let mut life = 1260;
            /* */ let mut coord: Point = Point::new(1, 1);
            /* */ let mut orientation = Orientation::N;
            /* */ let mut ressource = Ressources { food: 5, sibur: 5, mendiane: 5, linemate: 5, deraumere: 5, phiras: 5, thystame: 5 };
            /* */ if id_a == 2
            /* */ {
                /* */ lvl = 1;
                /* */ life = 1260;
                /* */ coord = Point::new(1, 1);
                /* */ orientation = Orientation::N;
                /* */ //ressource = Ressources::new();
            /* */ }
            /* ---------------------------- */

            Player
            {
                /* --------- TO REMOVE -------- */ 
                /* */ //level: lvl,
                /* */ //life: life,
                /* */ //orientation: orientation,
                /* */ //coord: coord,
                /* */ //ivt: ressource,
                /* ---------------------------- */



                id: id_a,
                port: port,
                actions: Vec::new(),
                level: 1,
                life: 1260,
                ivt: Ressources::new(),
                orientation: get_random_orientation(),
                coord: Point::new(rng.gen_range(0..width - 1), rng.gen_range(0..height - 1)),
            }
        }

        pub fn new_from_egg(id_a: u32, coord: Point, life: u16, port: u16, actions: Vec<Action>) -> Self
        {
            Player
            {
                id: id_a,
                port: port,
                level: 1,
                life: life,
                actions: actions,
                ivt: Ressources::new(),
                coord: Point::new(coord.x, coord.y),
                orientation: get_random_orientation(),
            }
        }

        pub fn from_egg(port: u16, egg: &Egg) -> Self
        {
            Player
            {
                id: egg.id,
                port: port,
                coord: Point::new(egg.coord.x, egg.coord.y),
                ivt: Ressources::new(),
                life: egg.life,
                orientation: get_random_orientation(),
                level: 1,
                actions: egg.actions.clone(),
            }
        }

        pub fn update(& mut self)
        {
            self.life -= 1;
            if self.life < 1134 && self.ivt.food > 0
            {
                self.ivt.food = self.ivt.food - 1;
                self.life += 127;
            }
            if self.actions.len() > 0
            {
                if self.actions[0].action_name != "connect_nbr"
                {
                    self.actions[0].count = self.actions[0].count - 1;
                }
            }
        }


        pub fn action_push(& mut self, command_full: String)
        {
            let ret = match command_full.starts_with("broadcast")
            {
                true => true,
                false => false
            };
            let mut action: Action = Action::new(NO_ACTION);
            let object: Option<String> = get_obj_from_string(&command_full, ret);

            for tmp_command in COMMANDS
            {
                if command_full.starts_with(tmp_command.action_name)
                {
                    action = Action::new(ActionTemplate{action_name: tmp_command.action_name, arg: object, count: tmp_command.count});
                    break ;
                }
            }
            //println!("id = {} Action {:?}", self.id, action);
            self.actions.push(action.clone());
        }

        pub fn print_player_actions(&self)
        {
            //println!("--------print_player_actions-----------\nplayer {} actions:", self.id);
            for action in &self.actions
            {
                //println!("action : {} , arg : {:?} count : {}", action.action_name, action.arg, action.count);
            }

        }

        pub fn new_from_dbg_file(
                                id: u32,
                                port: u16,
                                coord_x: u8,
                                coord_y: u8,
                                life: u16,
                                level: u8,
                                orientation: u8,
                                ivt: Ressources,
                                actions: Vec<Action>) -> Player
        {
            let real_orientation = get_orientation_from_int(orientation);
            Player {
                id: id,
                port: port,
                coord: Point::new(coord_x, coord_y),
                life: life,
                level: level,
                orientation: real_orientation,
                ivt: ivt,
                actions: actions
            }
        }

    }

    pub fn get_orientation_from_int(orientation: u8) -> Orientation
    {
        match orientation
        {
            1 => Orientation::N,
            2 => Orientation::E,
            3 => Orientation::S,
            4 => Orientation::O,
            _ => Orientation::N,
        }
    }

    pub fn get_random_orientation() -> Orientation
    {
        let mut rng = thread_rng();

        match rng.gen_range(0..4)
        {
            0 => Orientation::N,
            1 => Orientation::E,
            2 => Orientation::S,
            3 => Orientation::O,
            _ => Orientation::N,
        }
    }
    
}
