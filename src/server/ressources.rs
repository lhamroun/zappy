pub mod ressources{
    use std::collections::HashMap;

    
    #[derive(Debug, Clone)]
    pub struct Ressources{
        pub food: u8,
        pub sibur: u8,
        pub mendiane: u8,
        pub linemate: u8,
        pub deraumere: u8,
        pub phiras: u8,
        pub thystame: u8,
    }

    impl Ressources{
        pub fn new() -> Self
        {
            /*
            let val = 1;
            Ressources
            {
                food: val,
                sibur: val,
                mendiane: val,
                linemate: val,
                deraumere: val,
                phiras: val,
                thystame: val
            }
            // */
            //*
            Ressources
            {
                food: 0,
                sibur: 0,
                mendiane: 0,
                linemate: 0,
                deraumere: 0,
                phiras: 0,
                thystame: 0
            }
            // */
        }

        pub fn new_from_dbg_file(ressources: Vec<u8>) -> Self
        {
            Ressources
            {
                food: ressources[0],
                linemate: ressources[1],
                deraumere: ressources[2],
                sibur: ressources[3],
                mendiane: ressources[4],
                phiras: ressources[5],
                thystame: ressources[6]
            }
        }
    }
}