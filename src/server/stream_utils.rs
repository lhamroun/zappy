pub mod stream_utils
{
    use std::io::{Write, Read};
    use std::collections::HashMap;
    use std::net::{TcpListener, TcpStream};
    use std::thread;
    use std::time::Duration;

    use crate::paket_crafter::paquet_crafter::packet_gfx_fork;
    use crate::utils::utils::copy_until_char;
    use crate::{player, BUF_SIZE, BUF_SIZE_GFX, GFX_SERVER_PORT};
    use crate::gamecontrol::game::GameController;


    /*
    **  Convert String into buffer to send to gfx or client stream
    **/
    fn translate_string_to_buffer(gfx_pck_string: String, size: usize) -> Vec<u8>
    {
        let mut array = Vec::new();
        array.extend(gfx_pck_string.chars());
        array.extend(std::iter::repeat('0').take(size - gfx_pck_string.len()));
        
        let mut result_array = Vec::new();
        for (i, &c) in array.iter().enumerate()
        {
            result_array.push(c as u8);
        }
        //println!("result array --> {:?}", result_array);
        result_array
    }

    pub fn flush(data: &mut [u8])
    {
        for i in & mut *data
        {
            *i = 0;
        }
        //println!("{:?}", data);
    }

    pub fn debug_send_enw_pkt_gfx(game_ctrl: &GameController,  mut stream: &TcpStream)
    {
        let mut i = 1;
        for team in game_ctrl.teams.clone()
        {
            for egg in team.eggs
            {
                let mut pkts: Vec<String> = Vec::new();

                pkts.push(packet_gfx_fork(i, egg.id, egg.coord));
                send_pkt_to_stream(pkts, stream);
                i += 1;
            }
        }
    }

    /*
    **  Send a list of string to a TCP stream
    **  params:
    **      pkts: string vector that contains the packet to send
    **      stream: destination of the TCP packets list
    **/
    pub fn send_pkt_to_stream(pkts: Vec<String>, mut stream: &TcpStream)
    {    
        for pkt in pkts
        {
            //println!("stream port ---> {}", stream.peer_addr().unwrap().port());
            match stream.peer_addr()
            {
                Ok(addr) => {
                    if addr.port() == GFX_SERVER_PORT
                    {
                        println!("sending pkt to gfx --> {}", pkt);
                        let result = translate_string_to_buffer(pkt, BUF_SIZE_GFX);
                        let mut buf: [u8; BUF_SIZE_GFX] = [0; BUF_SIZE_GFX];
                        buf.copy_from_slice(&result[..BUF_SIZE_GFX]);
                        let _ = stream.write(&buf);
                        let _ = stream.flush();
                    }
                    else
                    {
                        println!("sending pkt to cli {} --> {}", stream.peer_addr().unwrap().port(), &pkt[..pkt.len() - 1]);
                        let result = translate_string_to_buffer(pkt, BUF_SIZE);
                        let mut buf: [u8; BUF_SIZE] = [0; BUF_SIZE];
                        buf.copy_from_slice(&result[..BUF_SIZE]);
                        let _ = stream.write(&buf);
                        let _ = stream.flush();
                    }
                },
                Err(msg) => {
                    println!("error : sending pkt to {:?} fail with error code {:?}", stream, msg);
                }
            }

        }
    }


    /*
    **  Tente d'ecouter les nouvelles connexions entrantes apres 
    **  l'eclosion d'un oeuf, pour l'instant il y a 10 tentatives 
    **  avant qu'on dise que la connexion a echoué (et on passe en mode non bloquant
    **  avec 10ms de sleep)
    */
    pub fn get_new_connexion(id: u32, listener: &TcpListener, width: u8, height: u8) -> Option<HashMap<u32, TcpStream>>
    {
        let mut new_connexion: HashMap<u32, TcpStream> = HashMap::new();

        if let Ok(_nb) = listener.set_nonblocking(true)
        {
            let mut i = 0;
            for tcpstream in listener.incoming()
            {
                if i > id + 1 {
                    break ;
                }
                // 88ms for a client to connect
                
                
                match tcpstream
                {
                    Ok(mut stream) =>
                    {
                        // est ce qu'on fait la procedure de handshake ??
                        println!("a new connexion is incomming --> {:?}", stream);
                        // set timeout
                        let _ = stream.set_read_timeout(Some(Duration::new(0, 100000)));

                        /*
                        let _ = stream.write(b"BIENVENUE\n");
                        // register the new client
                        let mut tmp = [0 as u8; BUF_SIZE];
                        if let  Ok(x) = stream.read(&mut tmp)  
                        {
                            println!("receive teamname {}", copy_until_char(&tmp, b'\n'));
                            let _ = stream.write(b"1\n");
                            let map_size = format!("{} {}\n", width, height);
                            let _ = stream.write(map_size.as_bytes());
                        }
                        */
                        new_connexion.insert(id, stream);
                        return Some(new_connexion);
                    },
                    Err(_) =>
                    {
                        continue ;
                    }
                }
                i += 1;
            }
        }
        if new_connexion.is_empty() { return None; }
        Some(new_connexion)
    }



    pub fn first_connection_gfx() -> Option<TcpStream>
    {
        let mut ret = false;
        let mut gfx_stream: Option<TcpStream> = None;
        let gfx_ip = format!("localhost:{}", GFX_SERVER_PORT);

        println!("Waiting GFX server for connection");
        while ret == false
        {   
            gfx_stream = match TcpStream::connect(gfx_ip.clone())
            {
                Ok(stream) =>
                {
                    ret = true;
                    Some(stream)
                },
                Err(e) => 
                {
                    //println!("Failed to connect: {}", e);
                    None
                }
            } 
        }
        gfx_stream
    }

    pub fn get_initial_gfx_packets_from_game_ctrl(game_ctrl: &GameController, t: u16) -> Vec<String>
{
    let mut all_packets : Vec<String> = vec![];
    // msz
    all_packets.push(game_ctrl.packet_gfx_map_size());
    all_packets.push(format!("sgt {}\n", t));
    // bct
    for i in game_ctrl.packet_gfx_ressources_map()
    {
        all_packets.push(i);
    }
    // tna
    for team in game_ctrl.packet_gfx_all_teams()
    {
        all_packets.push(team);
    }


    // enw
    for team in game_ctrl.packet_eggs_all_teams()
    {
        all_packets.push(team);
    }



    // pnw
    for team in game_ctrl.packet_gfx_all_players_of_all_teams()
    {
        for player in team
        {
            all_packets.push(player);
        }
    }
    all_packets
}
}
