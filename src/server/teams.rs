pub mod team
{
    use serde_json::Value;

    use crate::action::action::Action;
    use crate::player::player::{Player, Egg};
    use crate::paket_crafter::paquet_crafter::packet_gfx_player_connexion;
    use crate::ressources::ressources::Ressources;
    
    #[derive(Debug, Clone)]
    pub struct Team
    {
        pub name: String,
        pub connect_nbr: u8,
        pub port_start_index: u16,
        pub players: Vec<Player>,
        pub eggs: Vec<Egg>,
        pub nb_total_players: u32, // count dead and alive players and eggs
    }


    impl Team
    {
        pub fn new(name: &String, connect_nbr: u8) -> Self
        {
            Team
            {
                name: name.clone(),
                connect_nbr: connect_nbr - 1,
                port_start_index: 0,
                players: Vec::new(),
                eggs: Vec::new(),
                nb_total_players: 1
            }
        }

        pub fn update(& mut self)
        {
            self.players.iter_mut().for_each(|p| p.update());
            // remove dead players
            self.players.retain(|p| p.life != 0);
            self.eggs.iter_mut().for_each(|e|
            {
                e.update();
                if e.count == 0
                {
                    // add new player
                    self.players.push(Player::new_from_egg(e.id as u32, e.coord.clone(), e.life, e.port, e.actions.clone()));
                }
                if e.count == 599
                {
                    self.nb_total_players += 1;
                }
            });
            // remove dead eggs
            self.eggs.retain(|egg| egg.count > 0 && egg.life > 0);
        }

        pub fn packet_gfx_add_team(&self) -> String
        {
            format!("tna {}\n", self.name)
        }

        pub fn packet_gfx_add_all_players_team(&self) -> Vec<String>
        {
            let mut vec_gfx_packet_all_players = vec![];
            for player in &self.players
            {
                vec_gfx_packet_all_players.push(format!("{} {}\n", packet_gfx_player_connexion(player), self.name));
            }
            vec_gfx_packet_all_players
        }

        pub fn packet_gfx_new_players_from_id(&self, player_id: u32) -> Option<String>
        {
            for player in &self.players
            {
                if player.id == player_id
                {
                    return Some(format!("{} {}\n", packet_gfx_player_connexion(player), self.name));
                }
            }
            for egg in &self.eggs
            {
                if egg.id == player_id
                {
                    let player = Player::new_from_egg(egg.id, egg.coord.clone(), egg.life, egg.port, egg.actions.clone());
                    return Some(format!("{} {}\n", packet_gfx_player_connexion(&player), self.name));
                }
            }
            None
        }

        pub fn print_players_from_team(&self)
        {
            for player in &self.players
            {
                println!("player #{} :\n\
                port      : {}\n\
                coord     : ({}, {}) -> {:?}\n\
                level/life: {} / {}\n\
                ivt       : food {}, linemate {}, deraumere {}, sibur {}, phiras {}, mendiane {}, thystame {}",
                player.id, 
                player.port,
                player.coord.x,
                player.coord.y,
                player.orientation,
                player.level,
                player.life,
                player.ivt.food,
                player.ivt.linemate,
                player.ivt.deraumere,
                player.ivt.sibur,
                player.ivt.phiras,
                player.ivt.mendiane,
                player.ivt.thystame);
                println!("actions   :");
                for action in &player.actions
                {
                    println!("            (name: {}, count: {}, arg: {:?})",
                        action.action_name,
                        action.count,
                        action.arg);
                }
                println!("");
            }
        }

        pub fn print_eggs_from_team(&self)
        {
            for egg in &self.eggs
            {
                println!("egg #{}    :\n\
                coord     : ({}, {}) --- life / count: {} {}\nport {}\nactions {:?}\n", 
                egg.id, 
                egg.coord.x,
                egg.coord.y,
                egg.life,
                egg.count,
                egg.port,
                egg.actions);
            }
        }

    }

    fn get_eggs_from_dbg_file(tmp_eggs: &Vec<Value>) -> Vec<Egg>
    {
        let mut eggs: Vec<Egg> = Vec::new();
        for tmp_egg in tmp_eggs
        {
            let map_eggs = tmp_egg.as_object().unwrap();


            // get id
            let id = map_eggs.get("id").unwrap().as_u64().unwrap() as u32;
            //println!("egg id : {}", id);

            // get count
            let count = map_eggs.get("count").unwrap().as_u64().unwrap() as u16;
            //println!("egg id : {}", id);

            // get x
            let coord_x = map_eggs.get("coord_x").unwrap().as_u64().unwrap() as u8;
            //println!("egg x : {}", coord_x);

            // get y
            let coord_y = map_eggs.get("coord_y").unwrap().as_u64().unwrap() as u8;
            //println!("egg y : {}", coord_y);

            // get life
            let life = map_eggs.get("life").unwrap().as_u64().unwrap() as u16;
            //println!("egg life : {}", life);

            // get actions
            let tmp_actions = map_eggs.get("actions").unwrap();
            let mut tmp = Vec::new();
            for j in 0..tmp_actions.as_array().unwrap().len()
            {
                tmp.push(tmp_actions.as_array().unwrap()[j].as_str().unwrap());
            }
            //println!("player actions tmp : {:#?}", tmp);
            let mut actions: Vec<Action> = Vec::new();
            for action_name in tmp
            {
                let tmp = Action::new_from_string(action_name.to_string());
                actions.push(tmp);
            }
            //println!("egg actions : {:#?}", actions);

            // create egg from dbg file
            let egg = Egg::new_from_dbg_file(id, count, coord_x, coord_y, life, actions);
            eggs.push(egg);
        }
        eggs
    }

    fn get_players_from_dbg_file(tmp_players: &Vec<Value>) -> Vec<Player>
    {
        let mut players: Vec<Player> = Vec::new();
        for tmp_player in tmp_players
        {
            //println!("fn get players from tmp player :\n{:#?}", tmp_player);    
            let map_players = tmp_player.as_object().unwrap();
            //println!("get_players_from_dbg_file map_players : {:#?}", map_players);


            // get id
            let id = map_players.get("id").unwrap().as_u64().unwrap() as u32;
            //println!("player id : {}", id);

            // get port
            let port = map_players.get("port").unwrap().as_u64().unwrap() as u16;
            //println!("player port : {}", port);

            // get x
            let coord_x = map_players.get("coord_x").unwrap().as_u64().unwrap() as u8;
            //println!("player x : {}", coord_x);

            // get y
            let coord_y = map_players.get("coord_y").unwrap().as_u64().unwrap() as u8;
            //println!("player y : {}", coord_y);

            // get life
            let life = map_players.get("life").unwrap().as_u64().unwrap() as u16;
            //println!("player life : {}", life);

            // get level
            let level = map_players.get("level").unwrap().as_u64().unwrap() as u8;
            //println!("player level : {}", level);

            // get orientation
            let orientation = map_players.get("orientation").unwrap().as_u64().unwrap() as u8;
            //println!("player orientation : {}", orientation);

            // get ivt
            let tmp_ivt = map_players.get("ivt").unwrap();
            let mut tmp = Vec::new();
            for j in 0..tmp_ivt.as_array().unwrap().len()
            {
                tmp.push(tmp_ivt.as_array().unwrap()[j].as_u64().unwrap() as u8);
            }
            //println!("player ivt tmp : {:#?}", tmp);
            let ivt = Ressources::new_from_dbg_file(tmp);
            //println!("player ivt : {:#?}", ivt);

            // get actions
            let tmp_actions = map_players.get("actions").unwrap();
            let mut tmp = Vec::new();
            for j in 0..tmp_actions.as_array().unwrap().len()
            {
                tmp.push(tmp_actions.as_array().unwrap()[j].as_str().unwrap());
            }
            //println!("player actions tmp : {:#?}", tmp);
            let mut actions: Vec<Action> = Vec::new();
            for action_name in tmp
            {
                let tmp = Action::new_from_string(action_name.to_string());
                actions.push(tmp);
            }
            //println!("player actions : {:#?}", actions);

            let player = Player::new_from_dbg_file(id, port, coord_x, coord_y, life, level, orientation, ivt, actions);
            players.push(player);
        }
        players
    }

    pub fn init_teams_from_dbg_file(json_teams: &Value) -> Vec<Team>
    {
        //let mut tmp_players: Vec<Player> = Vec::new();
        let tmp_teams = json_teams.as_array().unwrap();
        let mut teams: Vec<Team> = Vec::new();
       //println!("init_teams_from_dbg_file tmp_teams :\n{:#?}", tmp_teams);

        for tmp_team in tmp_teams
        {
            //println!("le blaspheme du siecle --> tmp_team :{:?}", tmp_team);
            let connect_nbr = tmp_team.get("connect_nbr").unwrap().as_u64().unwrap() as u8;
            //println!("connect nbr : {}", connect_nbr);

            let teamname = tmp_team.get("teamname").unwrap().as_str().unwrap().to_string();
            //println!("teamname : {}", teamname);
            
            let nb_players = tmp_team.get("nb_player").unwrap().as_u64().unwrap() as u32;
            //println!("nbr players : {:?}", nb_players);
            
            let tmp_players = tmp_team.get("players").unwrap().as_array().unwrap();
            //println!("tmp players : {:#?}", tmp_players);
            
            let players = get_players_from_dbg_file(tmp_players);
            //println!("players after conversion : {:#?}", players);

            // il n'y a pas forcément d'eggs mais forcément des players
            let mut eggs: Vec<Egg> = Vec::new();
            let tmp_eggs = tmp_team.get("eggs");
            // get players from Value
            if let Some(eggs_ok) = tmp_eggs
            {
                let tmp = eggs_ok.as_array().unwrap();
                //println!("eggs : {:#?}", tmp);
                eggs = get_eggs_from_dbg_file(tmp);
                // ici on veut créer notre Vec<Egg>
            }
            let mut team: Team = Team {
                            name: teamname,
                            connect_nbr: connect_nbr,
                            port_start_index: 0,
                            players: players,
                            eggs: eggs,
                            nb_total_players: nb_players};
            teams.push(team);

        }
        //println!("generated teams from dbg file:\n{:#?}", teams);
        teams
    }
        
}
//std::process::exit(0);