pub mod utils
{

    #[derive(Debug, Clone, PartialEq)]
    pub enum BroadcastSquare
    {
        Square0,
        Square1,
        Square2,
        Square3,
        Square4,
        Square5,
        Square6,
        Square7,
        Square8,
        SquareError
    }


    use crate::{action::action::ReadyAction, cell::cell::Point, player::player::{Orientation, Player}, teams::team::Team};

    use std::process::Command;
    use std::io::{self, Write};

    pub fn clear_screen() {
        if cfg!(target_os = "windows") {
            // Windows
            Command::new("cmd")
                .args(&["/C", "cls"])
                .status()
                .expect("Failed to clear screen");
        } else {
            // Unix-like (Linux, macOS, etc.)
            print!("\x1B[2J\x1B[1;1H");
            io::stdout().flush().expect("Failed to flush stdout");
        }
    }

     /***********************************************************************************
     * Simple implementation of cpy_from_slice use for translate the buffer receive 
     * in the stream to the teamname
     * 
     * params:
     *      buffer: [u8; 32]
     * 
     * return:
     *       String
    *********************************************************************************/
    pub fn copy_until_char(buffer: &[u8], char: u8) -> String
    {
        let string_dst = buffer
            .iter() // into_iter 
            .take_while(|&x| *x != char)
            .map(|x| *x as char)
            .collect();
        string_dst
    }

    fn equal_with_precision(a: f64, b: f64, precision: f64) -> bool
    {
        (a - b).abs() <= precision
    }

    fn update_source_coord(source: &Point, dest: &Point, width: u8, height: u8) -> (i8,i8) {
        
        let mut offset_index = 4;
        let mut distance = 127;
        let map_offset_array: Vec<(i8, i8)> = vec![ (-(width as i8),    -(height as i8)),
                                                    (0,                 -(height as i8)),
                                                    (width as i8,       -(height as i8)),
                                                    (-(width as i8),    0),
                                                    (0,                 0),
                                                    (width as i8,       0),
                                                    (-(width as i8),    height as i8),
                                                    (0,                 height as i8),
                                                    (width as i8,       height as i8)];

        for (index, map_offset) in map_offset_array.iter().enumerate() {
            let new_source_x = source.x as i8 + map_offset.0;
            let new_source_y = source.y as i8 + map_offset.1;
            //println!("new source for index {} = ({}, {})", index, new_source_x, new_source_y);

            let tmp_distance = (new_source_x - dest.x as i8).abs() + (new_source_y - dest.y as i8).abs();
            //println!("new distance {}\n", tmp_distance);
            if tmp_distance < distance {
                distance = tmp_distance;
                offset_index = index;
            }
        }

        (source.x as i8 + map_offset_array[offset_index].0, source.y as i8 + map_offset_array[offset_index].1)
    }

    pub fn calc_broadcast_value(source: Point, dest: Point, width: u8, height: u8) -> BroadcastSquare
    {
        let mut add_value = 0.;
        let mut hypothenuse: f32 = 0.;
        // let mut adjacent: f32 = 0.;

        if source.x == dest.x && source.y == dest.y
        {
            return BroadcastSquare::Square0;
        }

        let (new_source_x, new_source_y) = update_source_coord(&source, &dest, width, height);
        
        //println!("nouvelles coordonnees source: {:?}", source);

        hypothenuse = ((new_source_x as f32 - dest.x as f32).powi(2) + (new_source_y as f32 - dest.y as f32).powi(2)).sqrt().abs();
        //println!("hypothenuse ---> {}", hypothenuse);

        // translation to get 0, 0 as center
        let new_dest_x = (dest.x as f32 - new_source_x as f32) / hypothenuse;
        let new_dest_y = (-1. * (dest.y as f32) + new_source_y as f32) / hypothenuse;
        //println!("dest x et y  ---> {} et {}", new_dest_x, new_dest_y); 

        if new_dest_x >= 0. && new_dest_y > 0.
        { // (1e cadran)
            //println!("1e cadran");
            add_value += 2. * std::f64::consts::PI / 2.;
        }
        else if new_dest_x < 0. && new_dest_y >= 0.
        { // (2e cadran)
            //println!("2e cadran");
            add_value += 2. * std::f64::consts::PI;
            //add_value += 3. * std::f64::consts::PI / 2.;
        }
        else if new_dest_x <= 0. && new_dest_y < 0.
        { // (3e cadran)
            //println!("3e cadran");
            //add_value += std::f64::consts::PI;
            //add_value += 3. * std::f64::consts::PI / 2.;
            add_value += 0.;
        }
        else if new_dest_x > 0. && new_dest_y <= 0.
        { // (4e cadran)
            //println!("4e cadran");
            //add_value += 3. * std::f64::consts::PI / 2.;
            add_value += std::f64::consts::PI;
        }
        else
        { // error
            println!("trigonometry error");
        }

        let mut rad = (new_dest_y as f64 / new_dest_x as f64).atan();
        //println!("rad before ---> {}", rad);
        rad += add_value;
        //println!("rad + add val ---> {}", rad);
        rad = rad.abs() % (2. * std::f64::consts::PI);
        //println!("rad ---> {}", rad);
        let mut ret = BroadcastSquare::SquareError;
        if (rad >= 0. && rad < std::f64::consts::PI / 4.) || (rad > 7. * std::f64::consts::PI / 4. && rad <= 2. * std::f64::consts::PI)
        {
            ret = BroadcastSquare::Square1;
        }
        else if rad == std::f64::consts::PI / 4.
        {
            ret = BroadcastSquare::Square2;
        }
        else if rad > std::f64::consts::PI / 4. && rad < 3. * std::f64::consts::PI / 4.
        {
            ret = BroadcastSquare::Square3;
        }
        else if rad == (3.0 * std::f64::consts::PI) / 4.
        {
            ret = BroadcastSquare::Square4;
        }
        else if rad > (3.0 * std::f64::consts::PI) / 4. && rad < (5.0 * std::f64::consts::PI) / 4.
        {
            ret = BroadcastSquare::Square5;
        }
        else if rad == (5.0 * std::f64::consts::PI) / 4.
        {
            ret = BroadcastSquare::Square6;
        }
        else if rad > 5. * std::f64::consts::PI / 4. && rad < 7. * std::f64::consts::PI / 4.
        {
            ret = BroadcastSquare::Square7;
        }
        else if rad == 7.0 * std::f64::consts::PI / 4.
        {
            ret = BroadcastSquare::Square8;
        }
        //println!("ret ---> {:?}", ret);

        /*
        println!("\n");
        //println!("PI/4 ---> {}", std::f64::consts::FRAC_PI_4);
        println!("PI/4 ---> {}", std::f64::consts::PI / 4.);
        //println!("PI/2 ---> {}", std::f64::consts::FRAC_PI_2);
        println!("PI/2 ---> {}", std::f64::consts::PI / 2.);
        println!("3PI/4 ---> {}", 3. * std::f64::consts::FRAC_PI_4);
        println!("PI ---> {}", std::f32::consts::PI);
        println!("5PI/4 ---> {}", 5. * std::f32::consts::FRAC_PI_4);
        println!("3PI/2 ---> {}", 3. * std::f32::consts::FRAC_PI_2);
        println!("7*PI/4 ---> {}", 7. * std::f32::consts::FRAC_PI_4);
        println!("2PI ---> {}", 2. * std::f32::consts::PI);
        */
        //println!("\n");
        ret
    }

    pub fn broadcast_square_with_player_distance(square: BroadcastSquare, ref_player: Player, player: Player, map_size: Point) -> BroadcastSquare
    {
        let mid_x = map_size.x / 2;
        let mid_y = map_size.y / 2;
        let distance_x = (ref_player.coord.x as i8 - player.coord.x as i8).abs();
        let distance_y = (ref_player.coord.y as i8 - player.coord.y as i8).abs();
        let direction_x = ref_player.coord.x as i8 - player.coord.x as i8;
        let direction_y = ref_player.coord.y as i8 - player.coord.y as i8;

    
        /*
        println!("mid_x      {}", mid_x);
        println!("mid_y      {}", mid_y);
        println!("distance_x {}", distance_x);
        println!("distance y {}", distance_y);
        // */
        match square
        {
            BroadcastSquare::Square0 => { BroadcastSquare::Square0 },
            BroadcastSquare::Square1 => {
                if distance_x as u8 > mid_x
                    && (player.orientation == Orientation::E
                    ||  player.orientation == Orientation::O)
                {
                    /*
                    let tmp_x = map_size.x + ref_player.coord.x;
                    let tmp_y = ref_player.coord.y;
                    let distance_x = (tmp_x as i8 - player.coord.x as i8).abs();
                    let distance_y = (tmp_y as i8 - player.coord.y as i8).abs();
                    if distance_x == distance_y {
                        if ref_player.coord.y < player.coord.y {
                            return BroadcastSquare::Square2;
                        }
                        else if ref_player.coord.y > player.coord.y {
                            return BroadcastSquare::Square8;
                        }
                    }
                    */


                    return BroadcastSquare::Square5;
                }
                else if distance_y as u8 > mid_y
                    && (player.orientation == Orientation::N
                    ||  player.orientation == Orientation::S)
                {
                    return BroadcastSquare::Square5;
                }
                BroadcastSquare::Square1
            },
            BroadcastSquare::Square2 => {
                if distance_x as u8 > mid_x
                    && distance_y as u8 > mid_y
                {
                    return BroadcastSquare::Square6;
                }
                BroadcastSquare::Square2
            },
            BroadcastSquare::Square3 => {
                if distance_x as u8 > mid_x
                    && (player.orientation == Orientation::N
                    ||  player.orientation == Orientation::S)
                {
                    return BroadcastSquare::Square7;
                }
                else if distance_y as u8 > mid_y
                    && (player.orientation == Orientation::E
                    ||  player.orientation == Orientation::O)
                {
                    return BroadcastSquare::Square7;
                }
                BroadcastSquare::Square3
            },
            BroadcastSquare::Square4 => {
                if distance_x as u8 > mid_x
                    && distance_y as u8 > mid_y
                {
                    return BroadcastSquare::Square8;
                }
                BroadcastSquare::Square4
            },
            BroadcastSquare::Square5 => {
                //*
                if distance_x as u8 > mid_x
                    && (player.orientation == Orientation::E
                    ||  player.orientation == Orientation::O)
                {
                    //*
                    let tmp_x = map_size.x + ref_player.coord.x;
                    let tmp_y = ref_player.coord.y;
                    let distance_x = (tmp_x as i8 - player.coord.x as i8).abs();
                    let distance_y = (tmp_y as i8 - player.coord.y as i8).abs();
                    
                    if distance_x == distance_y {
                        if ref_player.coord.y < player.coord.y {
                            return BroadcastSquare::Square2;
                        }
                        else if ref_player.coord.y > player.coord.y {
                            return BroadcastSquare::Square8;
                        }
                        else if tmp_x > player.coord.x && tmp_y < player.coord.y {
                            return BroadcastSquare::Square3;
                        }
                        else if tmp_x < player.coord.x && tmp_y > player.coord.y {
                            return BroadcastSquare::Square7;
                        }
                    }
                    //*/
                    return BroadcastSquare::Square1;
                }
                //*/
                BroadcastSquare::Square5
                /*
                if distance_x as u8 > mid_x
                    && (player.orientation == Orientation::E
                    ||  player.orientation == Orientation::O)
                    {
                    return BroadcastSquare::Square1;
                }
                else if distance_y as u8 > mid_y
                    && (player.orientation == Orientation::N
                    ||  player.orientation == Orientation::S)
                {
                    return BroadcastSquare::Square1;
                }
                */
            },
            BroadcastSquare::Square6 => {
                if distance_x as u8 > mid_x
                    && distance_y as u8 > mid_y
                {
                    return BroadcastSquare::Square2;
                }
                BroadcastSquare::Square6
            },
            BroadcastSquare::Square7 => {
                if distance_x as u8 > mid_x
                    && (player.orientation == Orientation::N
                    ||  player.orientation == Orientation::S)
                {
                    return BroadcastSquare::Square3;
                }
                else if distance_y as u8 > mid_y
                    && (player.orientation == Orientation::E
                    ||  player.orientation == Orientation::O)
                {
                    return BroadcastSquare::Square3;
                }
                BroadcastSquare::Square7
            },
            BroadcastSquare::Square8 => {
                if distance_x as u8 > mid_x
                    && distance_y as u8 > mid_y
                {
                    return BroadcastSquare::Square4;
                }
                BroadcastSquare::Square8
            },
            _ => {BroadcastSquare::SquareError},
        }
    }

    /*
    **  Aplly an offset to get the real value of the square. Ref orientation is E
    */
    pub fn broadcast_square_with_player_offset(orientation: Orientation, broadcast_square: BroadcastSquare) -> BroadcastSquare
    {
        if broadcast_square == BroadcastSquare::Square0
        {
            return BroadcastSquare::Square0;
        }
        match orientation
        {
            Orientation::E => { return broadcast_square;},
            Orientation::O => {
                match broadcast_square
                {
                    BroadcastSquare::Square1 => {BroadcastSquare::Square5},
                    BroadcastSquare::Square2 => {BroadcastSquare::Square6},
                    BroadcastSquare::Square3 => {BroadcastSquare::Square7},
                    BroadcastSquare::Square4 => {BroadcastSquare::Square8},
                    BroadcastSquare::Square5 => {BroadcastSquare::Square1},
                    BroadcastSquare::Square6 => {BroadcastSquare::Square2},
                    BroadcastSquare::Square7 => {BroadcastSquare::Square3},
                    BroadcastSquare::Square8 => {BroadcastSquare::Square4},
                    _ => BroadcastSquare::Square0
                }
            },
            Orientation::N => {
                match broadcast_square
                {
                    BroadcastSquare::Square1 => {BroadcastSquare::Square7},
                    BroadcastSquare::Square2 => {BroadcastSquare::Square8},
                    BroadcastSquare::Square3 => {BroadcastSquare::Square1},
                    BroadcastSquare::Square4 => {BroadcastSquare::Square2},
                    BroadcastSquare::Square5 => {BroadcastSquare::Square3},
                    BroadcastSquare::Square6 => {BroadcastSquare::Square4},
                    BroadcastSquare::Square7 => {BroadcastSquare::Square5},
                    BroadcastSquare::Square8 => {BroadcastSquare::Square6},
                    _ => BroadcastSquare::Square0
                }
            },
            Orientation::S => {
                match broadcast_square
                {
                    BroadcastSquare::Square1 => {BroadcastSquare::Square3},
                    BroadcastSquare::Square2 => {BroadcastSquare::Square4},
                    BroadcastSquare::Square3 => {BroadcastSquare::Square5},
                    BroadcastSquare::Square4 => {BroadcastSquare::Square6},
                    BroadcastSquare::Square5 => {BroadcastSquare::Square7},
                    BroadcastSquare::Square6 => {BroadcastSquare::Square8},
                    BroadcastSquare::Square7 => {BroadcastSquare::Square1},
                    BroadcastSquare::Square8 => {BroadcastSquare::Square2},
                    _ => BroadcastSquare::Square0
                }
            },
        }
    }

    pub fn remove_duplicate_incantation(teams: &Vec<Team>, ready_action_list: &mut Vec<ReadyAction>, incantated_groups: Vec<Vec<u32>>)
    {
        for mut group in incantated_groups
        {
            let to_remove = group.split_off(1);
            ready_action_list.retain(|action| !to_remove.contains(&action.id));
        }

        /*
        let mut size = ready_action_list.len() as i32;
        for mut i in 0..size
        {
            for mut j in i + 1..size
            {
                println!("i = {}", i);
                println!("j = {}", j);
                let player1 = find_player_from_id(teams.clone(), &ready_action_list[i as usize].id);
                let player2 = find_player_from_id(teams.to_vec(), &ready_action_list[j as usize].id);
                println!("player 1 id = {}", player1.clone().unwrap().id);
                println!("player 2 id = {}", player2.clone().unwrap().id);
                if ready_action_list[i as usize].action.action_name == "incantation"
                    && ready_action_list[j as usize].action.action_name == "incantation"
                    && player1.clone().unwrap().coord == player2.clone().unwrap().coord
                    && player1.clone().unwrap().level == player2.clone().unwrap().level
                {
                    println!("action to remove = {:?}", ready_action_list[j as usize]);
                    ready_action_list.remove(j as usize);
                    size -= 1;
                    i = -1;
                    break;
                }
            }
        }
        */
    }

    pub fn find_ids_in_groups(id: u32, groups: Vec<Vec<u32>>) -> Vec<u32>
    {
        for ids in groups
        {
            if ids.contains(&id)
            {
                return ids.clone();
            }
        }
        Vec::new()
    }

    // not use
    pub fn split_ready_actions(ready_action_list: Vec<ReadyAction>) -> (Vec<ReadyAction>, Vec<ReadyAction>)
    {
        let mut ready_incantation_list: Vec<ReadyAction> = Vec::new();
        let mut ready_other_action_list: Vec<ReadyAction> = Vec::new();

        for ready_action in ready_action_list
        {
            if ready_action.action.action_name == "incantation"
            {
                ready_incantation_list.push(ready_action);
            }
            else
            {
                ready_other_action_list.push(ready_action);
            }
        }
        (ready_incantation_list, ready_other_action_list)
    }

    pub fn remove_prefix(input: String, prefix: String) -> String
    {
        if input.starts_with(&prefix)    { input[prefix.len()..].to_owned() }
        else                            { input }
    }

    pub fn debug_print_dash_frame()
    {
        println!("------------------------------------------------------------------------------");
        println!("------------------------------------------------------------------------------");
        println!("------------------------------------------------------------------------------");
        println!("------------------------------------------------------------------------------");
        println!("------------------------------------------------------------------------------");
        println!("------------------------------------------------------------------------------");
        println!("------------------------------------------------------------------------------");
        println!("------------------------------------------------------------------------------");
        println!("------------------------------------------------------------------------------");
        println!("------------------------------------------------------------------------------");
        println!("------------------------------------------------------------------------------");
        println!("------------------------------------------------------------------------------");
        println!("------------------------------------------------------------------------------");
        println!("------------------------------------------------------------------------------");
        println!("------------------------------------------------------------------------------");
        println!("------------------------------------------------------------------------------");
        println!("------------------------------------------------------------------------------");
        println!("------------------------------------------------------------------------------");
        println!("------------------------------------------------------------------------------");
        println!("------------------------------------------------------------------------------");
        println!("------------------------------------------------------------------------------");
        println!("------------------------------------------------------------------------------");
        println!("------------------------------------------------------------------------------");
        println!("------------------------------------------------------------------------------");
        println!("------------------------------------------------------------------------------");
        println!("------------------------------------------------------------------------------");
        println!("------------------------------------------------------------------------------");
        println!("------------------------------------------------------------------------------");
        println!("------------------------------------------------------------------------------");
        println!("------------------------------------------------------------------------------");
        println!("------------------------------------------------------------------------------");
        println!("------------------------------------------------------------------------------");
        println!("------------------------------------------------------------------------------");
        println!("------------------------------------------------------------------------------");
        println!("------------------------------------------------------------------------------");
        println!("------------------------------------------------------------------------------");
        println!("------------------------------------------------------------------------------");
        println!("------------------------------------------------------------------------------");
        println!("------------------------------------------------------------------------------");
        println!("------------------------------------------------------------------------------");
        println!("------------------------------------------------------------------------------");
        println!("------------------------------------------------------------------------------");
        println!("------------------------------------------------------------------------------");
        println!("------------------------------------------------------------------------------");
        println!("------------------------------------------------------------------------------");
        println!("------------------------------------------------------------------------------");
        println!("------------------------------------------------------------------------------");
    }

    pub fn debug_print_separator()
    {
        println!("___________________________________________________________________________________");
        println!("| # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # |");
        println!("|# # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # #|");
        println!("| # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # # |");
        println!("|---------------------------------------------------------------------------------|");
        println!("\n");
    }

}