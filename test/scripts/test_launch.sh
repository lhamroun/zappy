#!/usr/bin/sh

cargo run --release --bin server -- -x 15 -y 15 -p 1312 -c 1 -n lala toto bibi gugu -t 10 &

sleep 0.5

cargo run --release --bin gfx 1>/dev/null &

sleep 3

cargo run --release --bin client lala 1312 1>/dev/null &
sleep 0.5
cargo run --release --bin client toto 1312 1>/dev/null &
sleep 0.5
cargo run --release --bin client bibi 1312 1>/dev/null &
sleep 0.5
cargo run --release --bin client gugu 1312 1>/dev/null &
