#!/usr/bin/sh

kill `ps -eo pid,command | grep "\/client" | grep -v grep | awk '{print $1}'`
kill `ps -eo pid,command | grep "\/gfx" | grep -v grep | awk '{print $1}'`
kill `ps -eo pid,command | grep "\/server" | grep -v grep | awk '{print $1}'`
